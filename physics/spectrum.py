""" Module to analyze the finished trajectories and make MS

spectrum.py contains constants and methods to interpret output
geometries from trajectories and create mass spectra out of
several of them.
"""

import os                  # A library to manage the file system
import numpy as np         # Library to do basic scientific computing
from matplotlib import pyplot as plt # Library to plot

# Masses for some elements
periodic = {"H": 1.008,
    "Li": 6.941,
    "Be": 9.012,
    "B": 10.811,
    "C": 12.011,
    "N": 14.007,
    "O": 15.999,
    "F": 18.998,
    "Na": 22.990,
    "Mg": 24.305,
    "Al": 26.982,
    "Si": 28.086,
    "P": 30.974,
    "S": 32.065,
    "Cl": 35.453}

# Covalent (or ionic) radii by atomic element (Angstroms) from
# "Inorganic Chemistry" 3rd ed, Housecroft, Appendix 6, pgs 1013-1014
# https://github.com/tmpchem/computational_chemistry/blob/master/scripts/geometry_analysis/bonds.py
cov_rads = {'H' : 0.37, 'C' : 0.77, 'O' : 0.73, 'N' : 0.75, 'F' : 0.71,
'P' : 1.10, 'S' : 1.03, 'Cl': 0.99, 'Br': 1.14, 'I' : 1.33, 'He': 0.30,
'Ne': 0.84, 'Ar': 1.00, 'Li': 1.02, 'Be': 0.27, 'B' : 0.88, 'Na': 1.02,
'Mg': 0.72, 'Al': 1.30, 'Si': 1.18, 'K' : 1.38, 'Ca': 1.00, 'Sc': 0.75,
'Ti': 0.86, 'V' : 0.79, 'Cr': 0.73, 'Mn': 0.67, 'Fe': 0.61, 'Co': 0.64,
'Ni': 0.55, 'Cu': 0.46, 'Zn': 0.60, 'Ga': 1.22, 'Ge': 1.22, 'As': 1.22,
'Se': 1.17, 'Kr': 1.03, 'X' : 0.00}

def distance_r3(atom1, atom2):
    r"""Compute the cartesian distance between atoms

    This function is intended to create the initial
    coordinates file.

    Parameters
    ----------
    atom1 : list of [str, float, float, float]
        The symbol of the atom and XYZ coordinates
    atom2 : list of [str, float, float, float]
        The symbol of the atom and XYZ coordinates

    Returns
    -------
    float
        Cartesian distance between atoms 1 and 2
    """

    d = [(atom1[i] - atom2[i])**2 for i in range(1,4)]
    return sum(d)**0.5

def get_fragment_mass(atoms, periodic=periodic):
    r"""Compute the molecular mass of a given fragment

    This function adds the mass of all atoms in a fragment
    to get the molecular mass

    Parameters
    ----------
    atoms : list of [str, float, float, float]
        A list of the atom symbol and its X, Y and Z coordinates
    periodic : dict
        The periodic table; the keys are the symbols (str) and the
        values (float) are the atomic masses

    Returns
    -------
    tot_mass : float
        Mass of the given fragment
    """

    tot_mass = 0
    for a in atoms:
        tot_mass += periodic[a[0]]
    return tot_mass

def adjacency_matrix(coords, threshold=cov_rads):
    r"""Make an adjacency matrix for a molecule

    This function builds a matrix where bonds are given
    by 1, or 0 where no bond is detected.

    Parameters
    ----------
    coords : list of [str, float, float, float]
        A list of the atom symbol and its X, Y and Z coordinates
    threshold : dic
        A database with covalent/ionic radii

    Returns
    -------
    adj_mat : NumPy array
        Adjacency matrix
    """

    # Get number of atoms
    num_atoms = len(coords)

    # Build the matrix and fill it with zeros
    adj_mat = np.zeros([num_atoms, num_atoms])

    # Loop over all atoms ...
    for i, a1 in enumerate(coords):

        # Loop over the rest of the atoms ...
        for j, a2 in enumerate(coords[i:]):

            # Compute the distance between atoms
            d = distance_r3(a1, a2)

            # Compute the threshold distance
            b = threshold[a1[0]] + threshold[a2[0]]

            # Compare and establish if there's a bond
            if (d <= b*4) and (d > 0):
                adj_mat[i,j] = 1
                adj_mat[j,i] = 1
    return adj_mat

def get_fragments(atoms, adjm, pte=periodic):
    r"""Check if there was fragmentation and get masses

    This function checks if the molecule underwent fragmentation
    and, if so, it extracts the mass of each fragment.

    Parameters
    ----------
    atoms : list of [str, float, float, float]
        A list of the atom symbol and its X, Y and Z coordinates
    adjm : NumPy array
        The system's adjacency matrix
    periodic : dict
        The periodic table; the keys are the symbols (str) and the
        values (float) are the atomic masses

    Returns
    -------
    fragmentation : bool
        If the molecule underwent fragmentation
    fragments : list of list of [str, float, float, float]
        Groups of atoms, which translate into fragments of the molecule
    masses : list of float
        Masses of all fragments
    """

    # Get number of atoms
    num_atoms = len(atoms)

    # Index of each fragment
    groups = [0] * num_atoms

    # Fragment index
    frag = 0

    # Loop over all atoms
    for i in range(num_atoms):
        # If this atom hasn't been assigned a fragment index ...
        if groups[i] == 0:
            # Increment the fragment index
            frag += 1
            # Assign the new fragment index to the atom
            groups[i] = frag
        # Loop over the next atoms ...
        for j in range(i + 1, num_atoms):
            # If the next atoms are connected to this one ...
            if adjm[i,j] == 1:
                # Assign them the same fragment index
                groups[j] = groups[i]

    # Check if the molecule underwent fragmentation
    if len(set(groups)) > 1:
        fragmentation = True
    else:
        fragmentation = False

    # Create a list for the molecular fragments
    fragments = [[] for f in range(frag)]
    # Loop over the fragment indeces
    for i, g in enumerate(groups):
        # Add the atoms of a fragment to a list
        fragments[g - 1].append(atoms[i])

    # Compute the masses of each fragment
    masses = [get_fragment_mass(f, pte) for f in fragments]

    return fragmentation, fragments, masses

def save_fragments(traj_spect, spectrum):
    r"""Create a CSV file with the fragment's masses

    This function saves each trajectory's fragments masses
    in a CSV file for later analysis

    Parameters
    ----------
    traj_spect : dict
        Name and fragment masses of a given trajectory
    spectrum : list of [float, int]
        The masses and number of appearences in each trajectory

    Returns
    -------
    None
        A CSV file with the fragment's masses
    """

    csv = []
    
    # Loop over all trajectory's results
    for key, value in traj_spect.items():
        # The name of the trajectory
        line = f"{key}"
        # Loop over all masses of that trajectory's resulting structure
        for peak in spectrum:
            line += ", "
            # If there's a fragment with said mass, add it
            if peak[0] in value:
                line += f"{peak[0]:.2f}"

        # Add the line to the CSV file
        csv.append(line + "\n")

    # Save CSV file
    with open("fragments.csv", "w") as ff:
        ff.writelines(csv)

def make_spectrum(sim_name, molecule, energy, spctr, g_trajs, label_thrs=0.1):
    r"""Create the mass spectrum

    This function takes the accumulated masses and creates a
    peak chart out of them in PNG format.

    Parameters
    ----------
    sim_name : str
        The name of the simulation
    molecule : str
        The human readable name of the molecule
    energy : float
        Average energy of the simulation
    spctr : list of [float, int]
        The masses and number of appearences in each trajectory
    g_trajs : int
        Number of trajectories which did yield a final structure
    label_thrs : float
        The percentage (between 0 and 1) at which a peak will
        start getting a label in the chart

    Returns
    -------
    None
        A PNG image
    """

    # Separate the X (masses) and Y (how many times it appeared) coords
    X, Y = zip(*spctr)

    # Get the maximum value of the mass count
    rel = max(Y)

    # Rescale all Y values
    Y = [y/rel * 100 for y in Y]

    # Make a base Y coordinate for all masses
    Y0 = [0]*len(Y)

    # Establish the dimensions of the plot
    plt.figure(figsize=(16, 9))

    # Plot for each X value, a line from Y0 to Y
    plt.vlines(X, Y0, Y, color="red", linestyles="solid")

    # Add a label over each peak
    for i, v in enumerate(Y):
        if v >= (label_thrs * 100):
            plt.annotate(f"{X[i]:.2f}",
                            (X[i] - 0.5, v + 1.5),
                            fontsize=6,
                            rotation=90
                        )
    #Add a title
    plt.title((f"Theoretical Mass Spectrum for {molecule}\n"
        rf"{g_trajs} trajectories at {energy:.4f} $kcal / mol$"))
    # Add the label for the X axis
    plt.xlabel(r"$m / z$", fontsize=14)
    # Add the label for the Y axis
    plt.ylabel("Relative abundance (%)", fontsize=14)
    # Set the range for the Y axis
    plt.ylim([0,115])
    # Set the range for the X axis
    plt.xlim([-5, max(X) + 5])
    # Save the image
    plt.savefig(f"{sim_name}_ms.png", dpi=300)
    # Clear the plotting area
    plt.cla()