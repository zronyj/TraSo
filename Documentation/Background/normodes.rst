Normal Modes
++++++++++++

The normal modes of a molecule are its vibrational eigenstates, each of them corresponding to a particular energy well, modeled as a quadratic function [JensenJ]_. To exemplify this analysis, a :math:`CO_2` molecule will be considered. In this case, only 3 atoms should be taken into account.

.. image:: ../imgs/CO2.png
	:width: 350
	:align: center


Displacement Vectors
====================

In a molecule, all atoms can have translational motion in all 3 axis, but each of them are bound by the electronic structure surrounding the nuclei. Discarding the simultaneous motions where all the atoms move to the same side, or those motions where the molecule seems to be spinning, each atom is considered as a harmonic oscillator which will have to be solved with respect to the other atoms. In the case of the :math:`CO_{2}` molecule only 4 motions are left after removing the translations and rotations:

.. _co2nm1:
.. figure:: ../imgs/nm_1.png
	:width: 300px
	:align: center

	First normal mode of the :math:`CO_{2}` molecule: symmetric stretch.

.. _co2nm2:
.. figure:: ../imgs/nm_2.png
	:width: 300px
	:align: center

	Second normal mode of the :math:`CO_{2}` molecule: asymmetric stretch.

.. _co2nm3:
.. figure:: ../imgs/nm_3.png
	:width: 300px
	:align: center

	Third normal mode of the :math:`CO_{2}` molecule: in-plane bend.

.. _co2nm4:
.. figure:: ../imgs/nm_4.png
	:width: 300px
	:align: center

	Fourth normal mode of the :math:`CO_{2}` molecule: out-of-plane bend.

To represent these motions in a mathematical way, each atomic displacement is considered as a 3-dimensional vector, and all vectors in a normal mode :math:`i` are subsequently chained together in a larger vector :math:`\vec{o}_{i}` with dimension :math:`3 \cdot N`.

.. math::
	:label: nmgen

	\vec{o}_{n} = \left[ x_{1}, y_{1}, z_{1}, x_{2}, y_{2}, z_{2}, \ldots , x_{N}, y_{N}, z_{N} \right]

Translating this to the :math:`CO_{2}` molecule, its normal modes can be encoded in the following way:

.. math::

	\vec{o}_{n} = \left[ x_{1}, y_{1}, z_{1}, x_{2}, y_{2}, z_{2}, x_{3}, y_{3}, z_{3} \right]

Where each vector represents the X, Y and Z components of the displacement of each atom of the :math:`CO_{2}` molecule. Therefore, when encoding the motions described as arrows in Figures :ref:`1 <co2nm1>` to :ref:`4 <co2nm4>`, using :eq:`nmgen`, the normal modes of :math:`CO_{2}` will look like this.

.. math::

	 \vec{o}_{1} = \left[ -1, 0, 0, 0, 0, 0, 1, 0, 0 \right]

.. math::

	\vec{o}_{2} = \left[ -1, 0, 0, 1, 0, 0, -1, 0, 0 \right]

.. math::

	\vec{o}_{3} = \left[ 0, 1, 0, 0, -1, 0, 0, 1, 0 \right]

.. math::

	\vec{o}_{4} = \left[ 0, 0, 1, 0, 0, -1, 0, 0, 1 \right]

This way of representing the normal modes as lists or arrays of components is, actually, how most electronic structure software packages handle the normal modes of molecules [GaussianVib]_. However, those software packages also normalize those vectors and mass-weight them as a method to make the eigenvalue problem easier to solve [Neese]_.

.. math::
	:label: mwnm

	\vec{o}_{n} = \left[ x_{1} \sqrt{m_{1}}, y_{1} \sqrt{m_{1}}, z_{1} \sqrt{m_{1}}, x_{2} \sqrt{m_{2}}, y_{2} \sqrt{m_{2}}, z_{2} \sqrt{m_{2}}, \ldots , x_{N} \sqrt{m_{N}}, y_{N} \sqrt{m_{N}}, z_{N} \sqrt{m_{N}} \right]

Normal modes are, therefore, represented as mass-weighted displacement vectors, having all 3 dimensions of displacement of each atom as elements [Neese]_. These vectors are the result of considering each pair of atoms as harmonic oscillators, and solving Hooke's equations for them.

Pairs of Atoms as Harmonic Oscillators
======================================

Now, given that all these atoms move according to Hooke's Law, the problem is solved as a harmonic oscillator. As such, the position of atom :math:`i`, expressed as :math:`\vec{r}_{i} = \left[ x_{i}, y_{i}, z_{i} \right]`, will be considered through its potential energy :math:`V` from Hooke's Law [JensenJ]_ [JensenF]_. To illustrate this last concept, only the first component :math:`x` of the position :math:`\vec{r}_{i}` of atom :math:`i` will be considered. Then, by defining :math:`x_{0}` as the the equilibrium position for said atom, Hooke's law can be expressed as:

.. math::
	:label: hookee

	V = \frac{1}{2} k x^{2} = \frac{1}{2} k_{x} \left(x - x_{0}\right)^{2}

.. math::
	:label: potentialforce

	F_{Hooke} = - \dfrac{d V}{d x} = - k_{x} x

By taking the first derivative of :math:`V` with respect to the position, the force :math:`F` can be found as seen in :eq:`potentialforce`. However, the second derivative will yield a more interesting result:

.. math::
	:label: hookek

	\dfrac{d^{2} V}{d x^{2}} = k_{x}

This reveals that the spring constant :math:`k` is related to the potential energy :math:`V` by its second derivative. The constant :math:`k` for each pair of atoms :math:`i` and :math:`j` can thus be computed using a matrix of second derivatives in the following way:

.. math::
	:label: hessian

	H = \begin{bmatrix}
	\dfrac{\partial^{2} V}{\partial x^{2}_{1}} & \dfrac{\partial^{2} V}{\partial x_{1} \partial y_{1}} & \dfrac{\partial^{2} V}{\partial x_{1} \partial z_{1}} & \ldots &
	\dfrac{\partial^{2} V}{\partial x_{1} \partial x_{N}} & \dfrac{\partial^{2} V}{\partial x_{1} \partial y_{N}} & \dfrac{\partial^{2} V}{\partial x_{1} \partial z_{N}}\\
	\dfrac{\partial^{2} V}{\partial y_{1} \partial x_{1}} & \dfrac{\partial^{2} V}{\partial y_{1}^{2}} & \dfrac{\partial^{2} V}{\partial y_{1} \partial z_{1}} & \ldots &
	\dfrac{\partial^{2} V}{\partial y_{1} \partial x_{N}} & \dfrac{\partial^{2} V}{\partial y_{1} \partial y_{N}} & \dfrac{\partial^{2} V}{\partial y_{1} \partial z_{N}}\\
	\vdots & \vdots & \vdots &
	\ddots &
	\vdots & \vdots & \vdots\\
	\dfrac{\partial^{2} V}{\partial z_{N} \partial x_{1}} & \dfrac{\partial^{2} V}{\partial z_{N} \partial y_{1}} & \dfrac{\partial^{2} V}{\partial z_{N} \partial z_{1}} & \ldots &
	\dfrac{\partial^{2} V}{\partial z_{N} \partial x_{N}} & \dfrac{\partial^{2} V}{\partial z_{N} \partial y_{N}} & \dfrac{\partial^{2} V}{\partial z_{N}^{2}}
	\end{bmatrix}

The matrix will have a size of and :math:`3N` by and :math:`3N`. In cases of a large molecule, this matrix should only be handled by computers. However, following the example of the :math:`CO_{2}` molecule, and :math:`H` fits in a page, and for illustrative purposes, is is shown as the next equation.

.. math::
	
	H_{CO_2} = \begin{bmatrix}
	\dfrac{\partial^{2} V}{\partial x^{2}_{1}} & \dfrac{\partial^{2} V}{\partial x_{1} \partial y_{1}} & \dfrac{\partial^{2} V}{\partial x_{1} \partial z_{1}} &
	\dfrac{\partial^{2} V}{\partial x_{1} \partial x_{2}} & \dfrac{\partial^{2} V}{\partial x_{1} \partial y_{2}} & \dfrac{\partial^{2} V}{\partial x_{1} \partial z_{2}} &
	\dfrac{\partial^{2} V}{\partial x_{1} \partial x_{3}} & \dfrac{\partial^{2} V}{\partial x_{1} \partial y_{3}} & \dfrac{\partial^{2} V}{\partial x_{1} \partial z_{3}}\\
	\dfrac{\partial^{2} V}{\partial y_{1} \partial x_{1}} & \dfrac{\partial^{2} V}{\partial y_{1}^{2}} & \dfrac{\partial^{2} V}{\partial y_{1} \partial z_{1}} &
	\dfrac{\partial^{2} V}{\partial y_{1} \partial x_{2}} & \dfrac{\partial^{2} V}{\partial y_{1} \partial y_{2}} & \dfrac{\partial^{2} V}{\partial y_{1} \partial z_{2}} &
	\dfrac{\partial^{2} V}{\partial y_{1} \partial x_{3}} & \dfrac{\partial^{2} V}{\partial y_{1} \partial y_{3}} & \dfrac{\partial^{2} V}{\partial y_{1} \partial z_{3}}\\
	\dfrac{\partial^{2} V}{\partial z_{1} \partial x_{1}} & \dfrac{\partial^{2} V}{\partial z_{1} \partial y_{1}} & \dfrac{\partial^{2} V}{\partial z_{1}^{2}} &
	\dfrac{\partial^{2} V}{\partial z_{1} \partial x_{2}} & \dfrac{\partial^{2} V}{\partial z_{1} \partial y_{2}} & \dfrac{\partial^{2} V}{\partial z_{1} \partial z_{2}} &
	\dfrac{\partial^{2} V}{\partial z_{1} \partial x_{3}} & \dfrac{\partial^{2} V}{\partial z_{1} \partial y_{3}} & \dfrac{\partial^{2} V}{\partial z_{1} \partial z_{3}}\\
	\dfrac{\partial^{2} V}{\partial x_{2} \partial x_{1}} & \dfrac{\partial^{2} V}{\partial x_{2} \partial y_{1}} & \dfrac{\partial^{2} V}{\partial x_{2} \partial z_{1}} &
	\dfrac{\partial^{2} V}{\partial x_{2}^{2}} & \dfrac{\partial^{2} V}{\partial x_{2} \partial y_{2}} & \dfrac{\partial^{2} V}{\partial x_{2} \partial z_{2}} &
	\dfrac{\partial^{2} V}{\partial x_{2} \partial x_{3}} & \dfrac{\partial^{2} V}{\partial x_{2} \partial y_{3}} & \dfrac{\partial^{2} V}{\partial x_{2} \partial z_{3}}\\
	\dfrac{\partial^{2} V}{\partial y_{2} \partial x_{1}} & \dfrac{\partial^{2} V}{\partial y_{2} \partial y_{1}} & \dfrac{\partial^{2} V}{\partial y_{2} \partial z_{1}} &
	\dfrac{\partial^{2} V}{\partial y_{2} \partial x_{2}} & \dfrac{\partial^{2} V}{\partial y_{2}^{2}} & \dfrac{\partial^{2} V}{\partial y_{2} \partial z_{2}} &
	\dfrac{\partial^{2} V}{\partial y_{2} \partial x_{3}} & \dfrac{\partial^{2} V}{\partial y_{2} \partial y_{3}} & \dfrac{\partial^{2} V}{\partial y_{2} \partial z_{3}}\\
	\dfrac{\partial^{2} V}{\partial z_{2} \partial x_{1}} & \dfrac{\partial^{2} V}{\partial z_{2} \partial y_{1}} & \dfrac{\partial^{2} V}{\partial z_{2}^{2}} &
	\dfrac{\partial^{2} V}{\partial z_{2} \partial x_{2}} & \dfrac{\partial^{2} V}{\partial z_{2} \partial y_{2}} & \dfrac{\partial^{2} V}{\partial z_{2}^{2}} &
	\dfrac{\partial^{2} V}{\partial z_{2} \partial x_{3}} & \dfrac{\partial^{2} V}{\partial z_{2} \partial y_{3}} & \dfrac{\partial^{2} V}{\partial z_{2} \partial z_{3}}\\
	\dfrac{\partial^{2} V}{\partial x_{3} \partial x_{1}} & \dfrac{\partial^{2} V}{\partial x_{3} \partial y_{1}} & \dfrac{\partial^{2} V}{\partial x_{3} \partial z_{1}} &
	\dfrac{\partial^{2} V}{\partial x_{3} \partial x_{2}} & \dfrac{\partial^{2} V}{\partial x_{3} \partial y_{2}} & \dfrac{\partial^{2} V}{\partial x_{3} \partial z_{2}} &
	\dfrac{\partial^{2} V}{\partial x_{3}^{2}} & \dfrac{\partial^{2} V}{\partial x_{3} \partial y_{3}} & \dfrac{\partial^{2} V}{\partial x_{3} \partial z_{3}}\\
	\dfrac{\partial^{2} V}{\partial y_{3} \partial x_{1}} & \dfrac{\partial^{2} V}{\partial y_{3} \partial y_{1}} & \dfrac{\partial^{2} V}{\partial y_{3} \partial z_{1}} &
	\dfrac{\partial^{2} V}{\partial y_{3} \partial x_{2}} & \dfrac{\partial^{2} V}{\partial y_{3} \partial y_{2}} & \dfrac{\partial^{2} V}{\partial y_{3} \partial z_{2}} &
	\dfrac{\partial^{2} V}{\partial y_{3} \partial x_{3}} & \dfrac{\partial^{2} V}{\partial y_{3}^{2}} & \dfrac{\partial^{2} V}{\partial y_{3} \partial z_{3}}\\
	\dfrac{\partial^{2} V}{\partial z_{3} \partial x_{1}} & \dfrac{\partial^{2} V}{\partial z_{3} \partial y_{1}} & \dfrac{\partial^{2} V}{\partial z_{3} \partial z_{2}} &
	\dfrac{\partial^{2} V}{\partial z_{3} \partial x_{2}} & \dfrac{\partial^{2} V}{\partial z_{3} \partial y_{2}} & \dfrac{\partial^{2} V}{\partial z_{3} \partial z_{2}} &
	\dfrac{\partial^{2} V}{\partial z_{3} \partial x_{3}} & \dfrac{\partial^{2} V}{\partial z_{3} \partial y_{3}} & \dfrac{\partial^{2} V}{\partial z_{3}^{2}}
	\end{bmatrix}


In general, :eq:`hookek` shows how every :math:`k_{ij}` is found for each pair of atoms :math:`i` and :math:`j`. This matrix :math:`H` is known as the Hessian. When it is solved as an eigenvalue problem, the normal modes :math:`\vec{o}_{j}` and frequencies :math:`\nu_{j}` are the results [GaussianVib]_. It should be pointed out, however, that this is not the matrix whose eigenvalues are found by electronic structure software packages [Neese]_. Since the idea is to find the frequencies :math:`\nu_{j}`, most packages exploit the fact that

.. math::
	:label: omega1

	\omega = 2 \pi \nu

.. math::
	:label: omega2

	\omega = \sqrt{\frac{k}{m}}

By taking advantage of :eq:`omega2`, and considering equation :eq:`hookek` to use a more compact notation, the actual matrix to be solved, in order to get the normal modes, is the following:

.. math::
	
	H = \begin{bmatrix}
	\frac{k_{x_{1} x_{1}}}{ \sqrt{ m_{1} m_{1} } } & \frac{k_{x_{1} y_{1}}}{ \sqrt{ m_{1} m_{1} } } & \frac{k_{x_{1} z_{1}}}{ \sqrt{ m_{1} m_{1} } } & \ldots &
	\frac{k_{x_{1} x_{N}}}{ \sqrt{ m_{1} m_{N} } } & \frac{k_{x_{1} y_{N}}}{ \sqrt{ m_{1} m_{N} } } & \frac{k_{x_{1} z_{N}}}{ \sqrt{ m_{1} m_{N} } }\\
	\frac{k_{y_{1} x_{1}}}{ \sqrt{ m_{1} m_{1} } } & \frac{k_{y_{1} y_{1}}}{ \sqrt{ m_{1} m_{1} } } & \frac{k_{y_{1} z_{1}}}{ \sqrt{ m_{1} m_{1} } } & \ldots &
	\frac{k_{y_{1} x_{N}}}{ \sqrt{ m_{1} m_{N} } } & \frac{k_{y_{1} y_{N}}}{ \sqrt{ m_{1} m_{N} } } & \frac{k_{y_{1} z_{N}}}{ \sqrt{ m_{1} m_{N} } }\\
	\vdots & \vdots & \vdots &
	\ddots &
	\vdots & \vdots & \vdots\\
	\frac{k_{z_{N} x_{1}}}{ \sqrt{ m_{N} m_{1} } } & \frac{k_{z_{N} y_{1}}}{ \sqrt{ m_{N} m_{1} } } & \frac{k_{z_{N} z_{1}}}{ \sqrt{ m_{N} m_{1} } } & \ldots &
	\frac{k_{z_{N} x_{N}}}{ \sqrt{ m_{N} m_{N} } } & \frac{k_{z_{N} y_{N}}}{ \sqrt{ m_{N} m_{N} } } & \frac{k_{z_{N} z_{N}}}{ \sqrt{ m_{N} m_{N} } }
	\end{bmatrix}

The eigenvalues will then become the angular velocity :math:`\omega_{i}` of each normal mode. But, of course, this also leads to the normal modes to be mass-weighted.

.. math::

	\tilde{o}_{i} = \sqrt{m_{i}} \vec{o}_{i}

When solving the eigenvalue problem of the Hessian in this way, the equation is the following:

.. math::
	:label: hessianeigenval

	H \tilde{o}_{j} = \omega_{j}^{2} \tilde{o}_{j}

Where :math:`\tilde{o}_{j}` is the mass-weighted raw normal mode, and :math:`\omega_{j}` its angular velocity. These normal modes are, however, linearly dependent on the translational and rotational motions of the molecule. To remove them and only consider the vibrations, another procedure must be followed.

Removing the Molecular Translations and Rotations
=================================================

Unfortunately, solving :eq:`hessianeigenval` will lead to several eigenvectors which are linearly dependent on the molecule's translation and rotation. To remove those terms from the vibrational normal modes, the molecule's translations and rotations have to be computed separately, and then removed from the Hessian through orthogonalization [GaussianVib]_.

Before starting, the inverse square root mass matrix :math:`M` will be defined. In that case, :math:`m_{i}` is the mass of atom :math:`i`, and its square root inverse is repeated 3 times over the diagonal, to mass-weight each coordinate (e.g. X, Y, and Z) of each atom.

.. math::
	
	M = \begin{bmatrix}
		m_{1}^{-\frac{1}{2}} & 0 & 0 & 0 & 0 & 0 & \ldots & 0 & 0 & 0\\
		0 & m_{1}^{-\frac{1}{2}} & 0 & 0 & 0 & 0 & \ldots & 0 & 0 & 0\\
		0 & 0 & m_{1}^{-\frac{1}{2}} & 0 & 0 & 0 & \ldots & 0 & 0 & 0\\
		0 & 0 & 0 & m_{2}^{-\frac{1}{2}} & 0 & 0 & \ldots & 0 & 0 & 0\\
		0 & 0 & 0 & 0 & m_{2}^{-\frac{1}{2}} & 0 & \ldots & 0 & 0 & 0\\
		0 & 0 & 0 & 0 & 0 & m_{2}^{-\frac{1}{2}} & \ldots & 0 & 0 & 0\\
		\vdots & \vdots & \vdots & \vdots & \vdots & \vdots & \ddots & \vdots & \vdots & \vdots\\
		0 & 0 & 0 & 0 & 0 & 0 & \ldots & m_{N}^{-\frac{1}{2}} & 0 & 0\\
		0 & 0 & 0 & 0 & 0 & 0 & \ldots & 0 & m_{N}^{-\frac{1}{2}} & 0\\
		0 & 0 & 0 & 0 & 0 & 0 & \ldots & 0 & 0 & m_{N}^{-\frac{1}{2}}
	\end{bmatrix}

To compute the translation of the whole molecule in all 3 dimensions, a simple basis :math:`B` can be used on all atoms:

.. math::
	:label: euclidbasis

	B = \left\{
	\begin{bmatrix}
		1\\
		0\\
		0
	\end{bmatrix},
	\begin{bmatrix}
		0\\
		1\\
		0
	\end{bmatrix},
	\begin{bmatrix}
		0\\
		0\\
		1
	\end{bmatrix}
	\right\}

This will lead to 3 vectors which, after being multiplied by the square root of the mass for each atom, will have the following shape:

.. math::
	
	\tilde{o}_{x} = \left[ \sqrt{m_{1}}, 0, 0, \sqrt{m_{2}}, 0, 0, \ldots, \sqrt{m_{N}}, 0, 0 \right]

.. math::
	
	\tilde{o}_{y} = \left[ 0, \sqrt{m_{1}}, 0, 0, \sqrt{m_{2}}, 0, \ldots, 0, \sqrt{m_{N}}, 0 \right]

.. math::

	\tilde{o}_{z} = \left[ 0, 0, \sqrt{m_{1}}, 0, 0, \sqrt{m_{2}}, \ldots, 0, 0, \sqrt{m_{N}} \right]

The rotation vectors require some additional calculations. The first part is to compute the molecule's center of mass :math:`\vec{r}_{com}`.

.. math::
	:label: com

	\vec{r}_{com} = \frac{1}{ \sum_{i=1}^{N} m_{i} } \sum_{i=j}^{N} m_{j} \vec{r}_{j}

Where :math:`\vec{r}_{i}` is the vector with the 3-dimensional coordinates of atom :math:`i`.

Afterwards, the whole molecule is moved to the center of mass

.. math::
	:label: movecom

	\vec{r}_{i} \left[ com \right] = \left. \vec{r}_{i} - \vec{r}_{com} \right| i = 1, 2, \ldots, n

and its inertia tensor :math:`I` computed.

.. math::
	:label: inertia

	I = \begin{bmatrix}
		I_{xx} & I_{xy} & I_{xz}\\
		I_{yx} & I_{yy} & I_{yz}\\
		I_{zx} & I_{zy} & I_{zz}
	\end{bmatrix} =
	\begin{bmatrix}
		\sum_{i} m_{i} \left( y^{2}_{i} + z^{2}_{i} \right) & - \sum_{i} m_{i} \left( x_{i} y_{i} \right) & - \sum_{i} m_{i} \left( x_{i} z_{i} \right)\\
		- \sum_{i} m_{i} \left( y_{i} x_{i} \right) & \sum_{i} m_{i} \left( x^{2}_{i} + z^{2}_{i} \right) & - \sum_{i} m_{i} \left( y_{i} z_{i} \right)\\
		- \sum_{i} m_{i} \left( z_{i} x_{i} \right) & - \sum_{i} m_{i} \left( z_{i} y_{i} \right) & \sum_{i} m_{i} \left( x^{2}_{i} + x^{2}_{i} \right)
	\end{bmatrix}


The matrix described in :eq:`inertia` is diagonalized through a matrix :math:`X`; the latter will be used to compute the rotational eigenvectors of :math:`H`. But before finding them, it is worth considering the following to avoid excessive calculations:

.. math::
	
	\left( P_{x} \right)_{i} = \vec{r}_{i} \left[ com \right] \cdot X_{1,..}

This means that :math:`\left( P_{x} \right)_{i}` is the dot product of the 3 coordinates of atom :math:`\vec{r}_{i}` and the first row of the matrix :math:`X`. Having this in mind, the rotational eigenvectors are computed as follows:

.. math::
	
	\tilde{o}_{rx} = \left( \left( P_{y} \right)_{i} X_{.., 3} - \left( P_{z} \right)_{i} X_{.., 2} \right) \sqrt{m_{i}}

.. math::

	\tilde{o}_{ry} = \left( \left( P_{z} \right)_{i} X_{.., 1} - \left( P_{x} \right)_{i} X_{.., 3} \right) \sqrt{m_{i}}

.. math::

	\tilde{o}_{rz} = \left( \left( P_{x} \right)_{i} X_{.., 2} - \left( P_{y} \right)_{i} X_{.., 1} \right) \sqrt{m_{i}}

Where :math:`\tilde{o}_{rx}` is the molecular rotational normal mode around the X axis, :math:`\tilde{o}_{ry}` is the rotational normal mode around the Y axis, and :math:`\tilde{o}_{rz}` is the rotational normal mode around the Z axis.

Now, both the translational and rotational vectors have to be normalized. However, to do so, the mass-weighting is momentarily removed:

.. math::
	:label: nmnormalization

	\left| M \tilde{o}_{j} \right|^{2} = \left| \vec{o}_{j} \right|^{2} = 1

If the left-hand-side of equation :eq:`nmnormalization` yields a result close to zero, then most quantum chemistry packages set all the values of the vector to :math:`0`. This is done because the vector is likely not to be a translational or rotational normal mode, because some symmetry in the molecule.

Finally, all normalized translational and rotational normal modes undergo a Gram-Schmidt orthonormalization procedure to generate the other normal modes :math:`\vec{o}_{i \rightarrow n}`. The result of considering all :math:`\tilde{o}_{j}` as columns, is the matrix :math:`D`, which transforms mass-weighted coordinates :math:`\tilde{o}_{j}` into internal coordinates :math:`\tilde{s}`, where rotation and translation have been separated out.

.. math::
	:label: carttoint

	\tilde{s} = D \tilde{o}

By using the matrix :math:`D`, then the Hessian can be taken from the mass-weighted cartesian space, into internal coordinate space. This is done in the following way:

.. math::
	:label: hessianint
	
	H_{int} = D^{\dagger} H D

Diagonalizing :math:`H_{int}` by means of the matrix :math:`\Phi` results in the eigenvalue matrix :math:`\Omega`, which contains the angular velocities :math:`\omega_{i}^{2}` of each atom in each dimension.

.. math::
	:label: diaghessint

	\Phi^{\dagger} H_{int} \Phi = \Omega

The angular velocities :math:`\omega_{i}` can be converted to frequencies :math:`\nu_{i}`, which are already the final eigenvalues of the Hessian; the vibrational frequencies of the molecule.

.. math::

	\omega_{i}^{2} = 4 \pi^{2} \nu_{i}^{2}

Nevertheless, the column vectors of :math:`\Phi` are the normal modes in internal coordinates; a final step is still needed to obtain the mass-weighted normal modes in Cartesian coordinates. To compute them, :eq:`hessianint` is inserted into :eq:`diaghessint`.

.. math::
	
	\Phi^{\dagger} D^{\dagger} H D \Phi = \Omega

Now :math:`O` becomes a matrix whose columns are the mass-weighted normal modes in Cartesian coordinates.

.. math::
	:label: mwcarcoo

	O = D \Phi

However, some software packages still compute just the normal modes in Cartesian coordinates only. Therefore, the last step to obtain the matrix whose column space are the Hessian's normal modes is to apply the matrix :math:`M` to said product.

.. math::
	
	\Xi = M D \Phi = M O

Finally, the matrix :math:`\Xi` are the normal modes in Cartesian coordinates. These are normalized by most software packages like Gaussian and Orca [GaussianVib]_ [Neese]_, though the latter works with the mass-weighted normal modes :math:`O`, as seen in equation :eq:`mwcarcoo`.

Now that the normal modes have been computed, the target energy can be given to them by, again, considering each normal mode as a displacement of a harmonic oscillator and sampling the position and momentum in phase space.

References
==========

.. [JensenJ] Jensen, J. H. Molecular Modeling Basics; CRC Press/Taylor & Francis Group: Boca Raton, 2010; p 189.

.. [GaussianVib] Ochterski, J. W. Vibrational Analysis in Gaussian. 2021; https://gaussian.com/vib/.

.. [Neese] Neese, F. Wiley Interdisciplinary Reviews: Computational Molecular Science 2012, 2, 73–78.

.. [JensenF] Jensen, F. Introduction to Computational Chemistry, 2nd ed.; John Wiley & Sons Ltd: West Sussex, 2013; pp –624.