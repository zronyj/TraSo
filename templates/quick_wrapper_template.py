#!/usr/bin/env python3

import os
import sys
import subprocess

# QUICK input first line (configuration)
quick_conf_line = "{lev} {theo} BASIS={bset} CUTOFF=1.0d-10 DENSERMS=1.0d-6 SCF={scf} GRADIENT EXTCHARGES DIPOLE\n\n"

# =============================================================================
# File names and paths
# =============================================================================

# Get the name of NAMD's output, as this script's input
input_name = sys.argv[1]

# Get the file's path
base_directory = os.path.dirname(input_name)

# Create path for Quick's input and output
quick_input = os.path.join(base_directory, "qmmm.in")
quick_output = os.path.join(base_directory, "qmmm.out")

# Create path for NAMD's future input
namd_input = os.path.join(base_directory, f"{{input_name}}.result")

# =============================================================================
# Extract data from NAMD's output
# =============================================================================

# Open NAMD's output and extract the data
with open(input_name, "r") as namd_out:
	
	# Split the line by spaces
	temp = namd_out.readline().split()

	# Get the number of atoms
	num_atoms = int(temp[0])

	# Get the number of point charges
	numPntChr = int(temp[1])

	# Create a list to store QUICK's input lines
	quick_lines = [quick_conf_line]

	# Create a list to store QUICK's point charges
	quick_charges = ["\n"]

	# For every *other* line in NAMD's outout ...
	for n_index, n_line in enumerate(namd_out):
		temp2 = n_line.split()

		# Get the coordinates
		posx, posy, posz = temp2[:3]

		if n_index < num_atoms:

			# Get the element
			element = temp2[3]

			# Add coordinates for the QUICK input
			quick_lines.append(f" {{element}} {{float(posx):>16.8f}} {{float(posy):>16.8f}} {{float(posz):>16.8f}}\n")

		else:
			# Get the atomic charge
			charge = temp2[3]

			# Add charges to the QUICK input
			quick_charges.append(f" {{charge}} {{posx:>16.8f}} {{posy:>16.8f}} {{posz:>16.8f}}\n")

	quick_lines += quick_charges

# =============================================================================
# Create QUICK's input
# =============================================================================
with open(quick_input, 'w') as quick_in:
	quick_in.writelines(quick_lines)

# =============================================================================
# Run QUICK
# =============================================================================
os.chdir(base_directory)

# Open a file to store any standard output from Quick
with open(f"quick.log", "w") as q_rt:
	log = subprocess.run(f"quick.cuda {{quick_input}}", shell=True, stdout=q_rt)

# =============================================================================
# Extract data from QUICK's output
# =============================================================================
with open(quick_output, 'r') as f:
	quick_out = f.readlines()

# Iterate over QUICK's input
for q_index, q_line in enumerate(quick_out):

	# Extract the atom number according to QUICK
	if "TOTAL ATOM NUMBER" in q_line:
		temp3 = q_line.split()
		qm_num_atoms = int(temp3[4])

		# Sanity check
		if qm_num_atoms != num_atoms:
			raise ValueError("The number of atoms from QUICK is different from\
				the number of atoms from NAMD.")

	# Extract the energy
	if "TOTAL ENERGY" in q_line:
		temp4 = q_line.split()
		energy = float(temp4[3])

	# Locate the gradient
	if "ANALYTICAL GRADIENT" in q_line:
		grad_line = q_index + 4

	# Locate the charges
	if "ATOMIC CHARGES" in q_line:
		dipole_line = q_index + 2


# Reading the gradients only
qm_grads = {'X':[0]*num_atoms, 'Y':[0]*num_atoms, 'Z':[0]*num_atoms}
for index_grad in range(grad_line, grad_line + num_atoms * 3):
	temp5 = quick_out[index_grad].split()
	grad_row = int(temp5[0][:-1]) - 1
	grad_col = temp5[0][-1]
	qm_grads[grad_col][grad_row] = float(temp5[2]) * -1185.82151

# Reading the charges only
qm_charges = [0]*num_atoms
for index_charge in range(num_atoms):
	temp6 = quick_out[dipole_line + index_charge].split()
	qm_charges[index_charge] = float(temp6[1])

# =============================================================================
# Create NAMS's input
# =============================================================================
namd_in_lines = [f"{energy * 627.509469}\n"]
for item in range(num_atoms):
	namd_in_lines.append(f"{{qm_grads['X'][item]}} {{qm_grads['Y'][item]}} {{qm_grads['Z'][item]}} {{qm_charges[item]}}\n")

with open(namd_input, 'w') as namd_in:
	namd_in.writelines(namd_in_lines)