#!/usr/bin/env python3

import os
import sys
import subprocess

# Orca input first line (configuration)
o_conf_line = """! {theo} {diff} {bset} EnGrad
%maxcore {ram}
%pal
nprocs {cpu}
end
%scf
maxiter {scf}
TolE 3e-5
ConvCheckMode 1
end
*xyz {chrg} {mltpl}
"""

# =============================================================================
# File names and paths
# =============================================================================

# Get the name of NAMD's output, as this script's input
input_name = sys.argv[1]

# Get the file's path
base_directory = os.path.dirname(input_name)

# Create path for Orca's input and output
o_input = os.path.join(base_directory, "qmmm.inp")
o_output = os.path.join(base_directory, "qmmm.out")

# Create path for NAMD's future input
namd_input = os.path.join(base_directory, f"{{input_name}}.result")

# =============================================================================
# Extract data from NAMD's output
# =============================================================================

# Open NAMD's output and extract the data
with open(input_name, "r") as namd_out:
    data = namd_out.readlines()

# Split the line by spaces
temp = data[0].split()

# Get the number of atoms
num_atoms = int(temp[0])

# Get the number of point charges
numPntChr = int(temp[1])

# Create a list to store the atomic coordinates
xyz_lines = []

# Create a list to store the point charges
xyz_charges = []

# For every *other* line in NAMD's outout ...
for n_index, n_line in enumerate(data[1:]):
    temp2 = n_line.split()

    # Get the coordinates
    posx, posy, posz = temp2[:3]

    if n_index < num_atoms:

        # Get the element
        element = temp2[3]

        # Add coordinates for the Orca input
        xyz_lines.append(f" {{element}} {{float(posx):>16.8f}} {{float(posy):>16.8f}} {{float(posz):>16.8f}}\n")

    else:
        # Get the atomic charge
        charge = temp2[3]

        # Add charges to the Orca input
        xyz_charges.append(f"{{charge}} {{posx:>16.8f}} {{posy:>16.8f}} {{posz:>16.8f}}\n")

o_conf_line += "".join(xyz_lines) + "*"

xyz_lines = [f"{{len(xyz_lines)}}\n", "XYZ coordinates for Orca\n"] + xyz_lines

with open("qmmm.xyz", "w") as coords:
    coords.writelines(xyz_lines)

with open("qmmm.q", "w") as charges:
    charges.writelines(xyz_charges)

# =============================================================================
# Create Orca's input
# =============================================================================
with open(o_input, 'w') as o_in:
    o_in.writelines(o_conf_line)

# =============================================================================
# Run Orca
# =============================================================================
os.chdir(base_directory)

# Open a file to store any standard output from Orca
with open(o_output, "w") as o_rt:
    log = subprocess.run(f"{path} {{o_input}}", shell=True, stdout=o_rt)

# =============================================================================
# Extract data from Orca's output
# =============================================================================
with open(o_output, 'r') as f:
    o_out = f.readlines()

grad_line = -1
char_line = -1

# Iterate over Orca's input
for q_index, q_line in enumerate(o_out):

    # Extract the atom number according to Orca
    if "Number of atoms" in q_line:
        temp3 = q_line.split()
        qm_num_atoms = int(temp3[4])

        # Sanity check
        if qm_num_atoms != num_atoms:
            raise ValueError("The number of atoms from Orca is different from\
                the number of atoms from NAMD.")

    # Extract the energy
    if "FINAL SINGLE POINT ENERGY" in q_line:
        temp4 = q_line.split()
        energy = float(temp4[4])

    # Locate the gradient
    if "CARTESIAN GRADIENT" in q_line:
        grad_line = q_index + 3

    # Locate the charges
    if "MULLIKEN ATOMIC CHARGES" in q_line:
        char_line = q_index + 2

f = open("Error.txt", "w")
# Reading the charges only
qm_charges = [0]*num_atoms
for index_charge in range(char_line, char_line + num_atoms):
    f.write(o_out[index_charge])
    temp7 = o_out[index_charge].split()
    qm_charges[int(temp7[0])] = float(temp7[3])
f.close()

# Reading the gradients only
qm_grads = {{'X':[0]*num_atoms, 'Y':[0]*num_atoms, 'Z':[0]*num_atoms}}
for index_grad in range(grad_line, grad_line + num_atoms):
    temp5 = o_out[index_grad].split()
    temp6 = [float(t5) for t5 in temp5[3:]]
    qm_grads['X'][index_grad - grad_line] = float(temp6[0]) * -1185.82151
    qm_grads['Y'][index_grad - grad_line] = float(temp6[1]) * -1185.82151
    qm_grads['Z'][index_grad - grad_line] = float(temp6[2]) * -1185.82151

# =============================================================================
# Create NAMS's input
# =============================================================================
namd_in_lines = [f"{{energy * 627.509469}}\n"]
for item in range(num_atoms):
    namd_in_lines.append(f"{{qm_grads['X'][item]}} {{qm_grads['Y'][item]}} {{qm_grads['Z'][item]}} {{qm_charges[item]}}\n")

with open(namd_input, 'w') as namd_in:
    namd_in.writelines(namd_in_lines)
