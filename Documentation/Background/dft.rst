Quantum Mechanics -QM-
++++++++++++++++++++++

Computing the normal modes (the Hessian), the gradient, the electronic density and the energy of each step of the MD can be done using several electronic structure calculation methods [Bauer]_. An attractive option given its accuracy and speed is Density Functional Theory -DFT-. This method, as the Hartree-Fock method, is based on the time independent Schrödinger :eq:tischrodinger and the variational principle, leading to a Self Consistent Field -SCF-. The main difference is that DFT uses the electronic density :math:`\rho` of a molecule instead of its wave function :math:`\psi` [JensenF]_.

.. math::
	:label: tischrodinger

	\left[ - \frac{\hbar}{2 m_{e}} \nabla^{2}_{i} + V \right] \psi = E_{i} \psi_{i}

In DFT, the system’s total energy is expressed in terms of a functional [1]_ over the electronic density (see :eq:dftenergy). The equations postulated by Walter Kohn and Jeu Lu Sham made it possible to perform density-based electronic structure calculations using computers [JensenF]_ [Cramer]_. The idea behind these equations lies in reducing the many-body problem of electrons interacting with each other, to many problems of a single non-interacting electron (i.e. solving the time independent Schrödinger :eq:tischrodinger several times for a single electron). This can be easily done by means of a SCF [Kohn]_.

.. math::
	:label: dftenergy

	E_{DFT} = T_{e} \left( \rho \right) + V_{ne} \left( \rho \right) + J \left( \rho \right) + E_{xc} \left( \rho \right)

Where :math:`T_{e}` is the kinetic energy of the electrons, :math:`V_{ne}` is the nuclei-electron potential energy, :math:`J` is the electrostatic interaction energy, and :math:`E_{xc}` is the exchange and correlation energy [JensenJ]_. The latter arises from the anti-symmetry of the wave function, introduced by the Slater determinant. All terms depend on the electronic density :math:`\rho`, which in turn will also be computed at the same time as the energy [JensenF]_.

.. math::
	:label: rhodens

	\rho \left( \vec{q}_{1} \right) = N \int d \vec{q}_{2} \ldots \vec{q}_{N} \left| \psi \left( \vec{q}_{1}, \vec{q}_{2}, \ldots \vec{q}_{N} \right) \right|^{2}

The electronic density is computed iteratively by an SCF until a self-consistent solution [2]_ is found. Each iteration requires the calculation of the density (see :eq:rhodens) to find the effective potential which, in turn, is used to solve the Kohn-Sham equations for the next cycle [JensenJ]_.

It should be noted that the Kohn-Sham equations contain a term referring to the exchange and correlation potential [JensenJ]_. This term is, to date, not known analytically and has been estimated in many ways using ab-initio and/or empirical data. This has led to the existence of many functionals to compute the electronic structure of molecules and materials [Goerigk]_. One of the most recognized functionals in the field of molecules is the B3LYP functional. This functional consists of an exact exchange term from Hartree-Fock energies, a term for electron-electron and nucleus-electron energies computed using the Local Density Approximation -LDA-, and several corrections using the Generalized Gradient Approximation -GGA- [Tirado-Rives]_.

.. [1] A functional is a function which takes functions as arguments.
.. [2] A self-consistent solution means that a certain quantity (observable) does not change significantly upon continuous iterations of the algorithm. The usual quantity referred to, is the molecule’s energy.

Local Density Approximation -LDA-
=================================

The LDA model stands over the hypothesis of a *uniform electron gas* surrounding the molecule's atomic nuclei. The nuclei are, therefore, treated as a uniformly spread positive background [3]_. The exchange and correlation energy is assumed to be separable, given that the density is a slow varying function of position (see equation :eq:`sepxc`). Out of these parts, the exchange is known exactly, while very good approximations have been found for the correlation [Cramer]_ [JensenF]_.

.. math::
	:label:  sepxc

	E_{xc} \left( \rho \right) = E_{x} \left( \rho \right) + E_{c} \left( \rho \right)

This model yields good results for equilibrium geometries, harmonic frequencies or charge moments. It is also the one yielding fastest results among all functionals given its simplicity [Goerigk]_. Nevertheless, it performs poorly when computing energies (e.g. atomization, bonding). Therefore, when dealing with chemical reactions, it should be avoided. Some examples of LDA functionals are Vosko, Wilk and Nusair -VWN- and Perdew and Wang -PW-[Cramer]_ [JensenF]_.

.. [3] Think of a perfect crystal lattice of a metal with valence electrons.

Generalized Gradient Approximation -GGA-
========================================

In the case of the GGA model, the energy is made dependent of the density :math:`\rho` and also its gradient :math:`\nabla \rho`. This allows the proper consideration of a non-uniform electron gas. The model also requires the function to integrate the Fermi and Coulomb holes to their respective values :math:`-1` and :math:`0`. The exchange part is usually based on the LDA approach with an additional term depending on the density and its gradient. The correlation part, on the other hand, are more complex and not directly correlated to a physical reasoning [JensenJ]_.

GGA functionals are more accurate for energy calculations than LDA functionals. However, they still have trouble describing all systems or chemical reactions. In the case of GGA functionals, the exchange and correlation parts are found purposefully separated; some common exchange GGA functionals are the Becke -B88- and OPTimized eXchange -OPTX-, while some common correlation functionals are the Perdew -P86-, Perdew and Wang -PW91- and Lee, Yang and Par -LYP-. Combinations of these functionals lead to the more common BP86, BLYP, OLYP or BPW91. Finally, several related exchange–correlation functionals have been proposed to refine the former. Among them are the Perdew–Wang 1986 -PW86-, Perdew–Wang 1991 -PW91- and Perdew–Burke–Ernzerhof -PBE- functionals. The latter being the best refinement among the 3 of them [Cramer]_ [JensenF]_.

Beyond GGA functionals
======================

For a more accurate description of the energy and the electron density, some GGA functionals additionally use the second derivative (the Laplacian) of the density, and/or the kinetic energy density. Another way to refine the calculations is to include orbital information into the model. The mathematical expression of these functionals is much more complicated than previous ones. Because they rely on the GGA model, these functionals are named meta-GGA [Cramer]_.

Another approach to refine the GGA model is to consider an exact connection between the exchange-correlation energy and the corresponding hole potential. This means that the exchange part will be a linear combination of the LDA energy and the exact energy. The latter coming from the Hartree-Fock wave function. Another approach is to consider a linear combination of LDA and a gradient correction term. These models are called hybrid methods. One of the best known functionals in this category is the Becke 3 parameter exchange functional -B3- combined with the LYP correlation functional to build the B3LYP functional [JensenJ]_.

Dispersion Effects
==================

DFT, by its nature, only accounts for the electronic structure of a molecule. However, for intermolecular interactions [4]_, another term can be included in the total energy functional [Grimme1]_. One popular approach to deal with these interactions is the dispersion correction developed by Stefan Grimme. The latter, also known as the Grimme correction, is widely used due to its simplicity and effectiveness in improving the accuracy of DFT calculations for systems where van der Waals interactions play a significant role [Grimme2]_. Additionally, the mathematical shape of this correction is relatively simple, which translates into a low computational cost.

Grimme's dispersion correction is an empirical correction, based on Becke's GGA approach, that adds an additional term to the DFT total energy functional. This term accounts for the attractive dispersion forces and is typically represented by a sum of atom-pairwise :math:`C_{6} \cdot R^{-6}` terms, where :math:`C_{6}` is a dispersion coefficient and :math:`R` is the distance between interacting atoms [Grimme1]_ [Grimme3]_.

This correction is especially relevant for systems with non-covalently bonded molecules, layered structures, or weakly interacting surfaces. It aids in the modeling of the long-range attraction between atoms that is not adequately described by traditional DFT functionals [Grimme4]_ [Grimme2]_. By including the dispersion correction, DFT calculations can better reproduce experimental results for systems with van der Waals interactions [Grimme4]_. Particularly, in the case of simulating the mass spectrum of a molecule, the intermolecular interactions of the fragments can be accounted for by using this dispersion term [Grimme5]_ [Bauer]_ [Koopman1]_.

.. [4] Intermolecular interactions are usually known as Van der Walls forces. The latter can be further decomposed in the Debye, Keesom and London forces. The firs two refer to polar interactions, while London dispersion forces arise from non-zero instantaneous dipole moments and electron correlation [Grimme3]_ [Leite]_ [Grimme2]_.

References
==========

.. [JensenJ] Jensen, J. H. Molecular Modeling Basics; CRC Press/Taylor & Francis Group: Boca Raton, 2010; p 189.

.. [JensenF] Jensen, F. Introduction to Computational Chemistry, 2nd ed.; John Wiley & Sons Ltd: West Sussex, 2013; pp –624.

.. [Bauer] Bauer, C. A.; Grimme, S. Journal of Physical Chemistry A 2014, 118, 11479–11484.

.. [Cramer] Cramer, C. J. Essentials of Computational Chemistry: Theories and Models, 2nd ed.; John Wiley & Sons Ltd: West Sussex, 2004.

.. [Kohn] Kohn, W.; Sham, L. J. Physical Review 1965, 140, A1133.

.. [Goerigk] Goerigk, L.; Hansen, A.; Bauer, C.; Ehrlich, S.; Najibi, A.; Grimme, S. Physical Chemistry Chemical Physics 2017, 19, 32184–32215.

.. [Tirado-Rives] Tirado-Rives, J.; Jorgensen, W. L. Journal of Chemical Theory and Computation 2008, 4, 297–306.

.. [Grimme1] Grimme, S. Journal of Computational Chemistry 2006, 27, 1787–1799.

.. [Grimme2] Grimme, S.; Hansen, A.; Brandenburg, J. G.; Bannwarth, C. Chemical Reviews 2016, 116, 5105–5154.

.. [Grimme3] Grimme, S. Wiley Interdisciplinary Reviews: Computational Molecular Science 2011, 1, 211–228.

.. [Grimme4] Grimme, S.; Antony, J.; Ehrlich, S.; Krieg, H. Journal of Chemical Physics 2010, 132.

.. [Grimme5] Grimme, S. Angewandte Chemie International Edition 2013, 52, 6306–6312.

.. [Leite] Leite, F. L.; Bueno, C. C.; Da Róz, A. L.; Ziemath, E. C.; Oliveira, O. N. International Journal of Molecular Sciences 2012, 13, 12773.

.. [Koopman1] Koopman, J.; Grimme, S. ACS Omega 2019, 4, 15120–15133.