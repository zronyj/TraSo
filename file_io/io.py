""" Module to handle all the input reading and output creation

The paths for all other programs and the paramters to run TraSo
are provided in JSON files, which have to be read and parsed.
On the other hand, the outputs have to be written in a particular
format, which is basically created and established here.
"""

import os
import re
import json

# Program header for the beginning of the output
__head__ = """ ----------------------------------------------------------------------------- 
                                                                               
                                                  ****                         
                                           ******                              
                                    **********                                 
                             *************                                     
                            *****************************                      
                                                 **************                
                                                ****************               
                                             *******************               
                                        **********************                 
                                  *************************                    
                            ****************************                       
                     *******************************                           
             ***********************************                               
                                                                               
                                                                               
                  ********                    ****                             
                  *  **  *                   **  **                            
                     **      ****    ****     **      ****                     
                     **     **   *  *   **      **   *   **                    
                     **     **      *   **   **  **  *   **                    
                     **     **       *** **   ****    ****                     
                                                                               
 ----------------------------------------------------------------------------- 
                                                                               
                             TraSo v {v:.1f} ({y:4}.{m:02})                             
                                                                               
                               by {author:^14}                               
                        ({email:^29})                        
                                                                               
 ----------------------------------------------------------------------------- 

    Trajectory Software is intended for unimolecular decomposition reactions
    by sampling a molecule's normal modes to a target energy. The main idea
    is to theoretically generate CID mass spectra.

 ----------------------------------------------------------------------------- 
 
    This software package has come to existence thanks to the groundbreaking
    work of Hase[1], Grimme[2], Spezia[3] and Martin-Somer[4]. All
    acknowledgements are to them for their great contribution to this
    branch of Computational Chemistry. Thank you!

    [1] Hase, W. L.; Buckowski, D. G. Chemical Physics Letters 1980, 74,
        284–287.
    [2] Grimme, S. Angewandte Chemie International Edition 2013, 52,
        6306–6312.
    [3] Spezia, R.; Salpin, J. Y.; Gaigeot, M. P.; Hase, W. L.; Song, K.
        Journal of Physical Chemistry A 2009, 113, 13853–13862.
    [4] Martin-Somer, A.; Martens, J.; Grzetic, J.; Hase, W. L.;
        Oomens, J.; Spezia, R. Journal of Physical Chemistry A 2018, 122,
        2612–2625.

 ----------------------------------------------------------------------------- 
                                                                               
    TraSo - Molecular Trajectory Software                                     
    Copyright (C) {y:4}  {author:^14}                                        
                                                                              
    This program is free software: you can redistribute it and/or modify      
    it under the terms of the GNU General Public License as published by      
    the Free Software Foundation, either version 3 of the License, or         
    (at your option) any later version.                                       
                                                                              
    This program is distributed in the hope that it will be useful,           
    but WITHOUT ANY WARRANTY; without even the implied warranty of            
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             
    GNU General Public License for more details.                              
                                                                              
    You should have received a copy of the GNU General Public License         
    along with this program.  If not, see <https://www.gnu.org/licenses/>.    
                                                                               
 ----------------------------------------------------------------------------- 
"""

__desc__ = (" Trajectory Software is intended for unimolecular "
    "decomposition reactions by sampling a molecule's "
    "normal modes to a target energy. The main idea is "
    "to theoretically generate CID mass spectra.")

def switcher(o, n, a, s):
    r"""Check that the requested calculation makes sense

    This function checks that the user doesn't enter a list of
    calculations implying a broken sequence (e.g. you can't run
    BOMD after a frequencies calculation without the sampling).

    Parameters
    ----------
    o : bool
        Flag for optimization and frequencies
    n : bool
        Flag for the normal mode sampling
    a : bool
        Flag for the Born-Oppenheimer MD / Ab Initio MD
    s : bool
        Flag for creating the mass spectrum

    Raises
    ------
    ValueError
        If the flags imply a broken sequence

    Returns
    -------
    switches : list of bool
        List of activation flags for the stages of TraSo
    """

    # Activation flags as a list
    switches = [o, n, a, s]

    # Converting the flags to binary
    str_bin = '0b' + ''.join(['1' if x else '0' for x in switches])

    # Excluding broken sequence cases
    if int(str_bin, 2) in [5, 9, 10, 11, 13]:
        raise ValueError(" You can't run this calculation skipping a step!")

    # If all flags are off, turn them on
    if sum(switches) == 0:
        return [True, True, True, True]
    else:
        return switches

def take_paths(base_dir):
    r"""Load the paths for all the other executables

    Parameters
    ----------
    base_dir : str
        The path of the TraSo executable

    Raises
    ------
    LookupError
        If there is no Path keyword for a given software package.
        If there is no QM or MD keyword for a given software package.
        If there is no Frequencies keyword for a given QM software package.
    FileNotFoundError
        If the path given for a software package doesn't exist
    ValueError
        If there is no software package to do QM calculations.
        If there is no software package to do MD calculations.
        If there is no software package to do Frequencies calculations.

    Returns
    -------
    qm_options : dict
        Data of all the programs which can run quantum
        mechanical calculations.
    md_options : dict
        Data of all the programs which can run molecular
        dynamics calculations.
    """

    # Build the complete path to the configuration file
    json_path = os.path.join(base_dir, 'binpaths.json')

    # Open the file and read the data
    with open(json_path, 'r') as f:
        data = f.read()

    # Parse the JSON file to a dictionary
    all_options = json.loads(re.sub("//.*","",data,flags=re.MULTILINE))

    # Define new dictionaries
    qm_options = {}
    md_options = {}

    freqs = False

    # Iterate over all the conf options
    for key, value in all_options.items():
        
        # Check if the path for the software package was specified
        if not("Path" in value.keys()):
            raise LookupError(f"There is no path set for {key}!")

        # Check whether the program actually exists in the path
        if not(os.path.exists(value["Path"])):
            raise FileNotFoundError((f"The path provided for {key} doesn't "
                            "seem to exist!"))

        # Check whether there's a TraSo module for the program
        if not(os.path.exists(
                    os.path.join(base_dir, "file_io", f"{key.lower()}.py")
                    )
                ):
            raise FileNotFoundError((f"There is no module to handle {key}. "
                            "Please make sure that the module exists."))

        # Check whether the software package can do QM or MD
        if not ("QM" in value.keys()) or not ("MD" in value.keys()):
            raise LookupError((f"The entry for {key} doesn't specify how it "
                "handles QM and MD calculations!"))

        # Check if the QM package has the Frequencies calculation flag
        if value["QM"] and not("Frequencies" in value.keys()):
            raise LookupError((f"The entry for {key} doesn't specify if it "
                "can perform frequencies calculations!"))

        # Check if package can actually perform frequencies calculations
        if value["QM"] and value["Frequencies"]:
            freqs = True

        if value["QM"]:                     # If this soft can do QM ...
            qm_options[key] = value         # ... put it as a QM option
        if value["MD"]:                     # If this soft can do MD ...
            md_options[key] = value         # ... put it as a MD option

    # Is there at least 1 program which can do the QM part?
    if len(qm_options) == 0:
        raise ValueError(("No program to do the quantum mechanics part "
                    "was included!"))

    # Is there at least 1 program which can do the MD part?
    if len(md_options) == 0:
        raise ValueError(("No program to do the molecular dynamics part "
                    "was included!"))

    # Is there at least one package that can do frequencies calculations?
    if not(freqs):
        raise ValueError(("No program to do the frequencies calculation "
                    "was included!"))

    return qm_options, md_options

def load_input(input_path, file_name):
    r"""Load the input data, and parse it for a TraSo calculation

    Parameters
    ----------
    input_path : str
        The string with the path to the JSON file
    file_name : str
        Name of the JSON file with the input data

    Raises
    ------
    FileNotFoundError
        If the input file or the XYZ file don't exist
    ValueError
        If the input file is not a JSON file

    Returns
    -------
    parameters : dict
        Input parameters for a TraSo calculation
    """

    # Does the file exist?
    full_path = os.path.join(input_path, file_name)

    if not(os.path.exists(full_path)):
        raise FileNotFoundError((f"The path provided for {file_name} "
                            "doesn't seem to exist!"))

    # Check that the provided file has a "json" extension
    if file_name[-4:] != "json":
        raise ValueError("The provided file is not a JSON file!")

    # Read the file
    with open(full_path, "r") as inp:
        par = inp.read()

    # Parse the content of the file
    parameters = take_input(par)

    # Check if the coordinates exist
    if not(os.path.exists(os.path.join(input_path,
        parameters['General']['Coordinates']))):
        FileNotFoundError((f"The file {parameters['General']['Coordinates']}"
            " doesn't seem to exist!"))

    return parameters


def take_input(input_data):
    r"""Parse the input parameters for a TraSo calculation

    Parameters
    ----------
    input_data : str
        The string with all the data encoded in JSON format

    Raises
    ------
    LookupError
        If there is a missing field in the input

    Returns
    -------
    all_params : dict
        Input parameters for a TraSo calculation
    """

    # Reference to all fields required for TraSo to work
    input_sample = {
        "General": ["Molecule name", "Coordinates"],
        "Sampling": ["Method", "Energy", "Trajectories", "Parallel propagations"],
        "QM": ["Software", "Functional", "Basis set", "Diffusion",
                "Charge", "Multiplicity", "SCF"],
        "MD": ["Steps", "Time step"]
    }

    # Parse the JSON file to a dictionary
    all_params = json.loads(re.sub("//.*","",input_data,flags=re.MULTILINE))

    # Check if the input fields exist
    for k, v in input_sample.items():
        if not(k in all_params.keys()):
            raise LookupError(f"No field '{k}' was found in your input!")
        for k2 in v:
            if not(k2 in all_params[k].keys()):
                raise LookupError(f"No field '{k2}' was found in '{k}'.")

    return all_params
