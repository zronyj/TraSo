<!DOCTYPE html>
<html class="writer-html5" lang="en" >
<head>
  <meta charset="utf-8" /><meta name="generator" content="Docutils 0.18.1: http://docutils.sourceforge.net/" />

  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>Molecular Dynamics -MD- &mdash; TraSo 0.2 documentation</title>
      <link rel="stylesheet" href="../_static/pygments.css" type="text/css" />
      <link rel="stylesheet" href="../_static/css/theme.css" type="text/css" />
      <link rel="stylesheet" href="../_static/css/math.css" type="text/css" />
  <!--[if lt IE 9]>
    <script src="../_static/js/html5shiv.min.js"></script>
  <![endif]-->
  
        <script data-url_root="../" id="documentation_options" src="../_static/documentation_options.js"></script>
        <script src="../_static/jquery.js"></script>
        <script src="../_static/underscore.js"></script>
        <script src="../_static/_sphinx_javascript_frameworks_compat.js"></script>
        <script src="../_static/doctools.js"></script>
        <script async="async" src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js"></script>
    <script src="../_static/js/theme.js"></script>
    <link rel="index" title="Index" href="../genindex.html" />
    <link rel="search" title="Search" href="../search.html" />
    <link rel="next" title="Quantum Chemistry - Density Functional Theory -DFT-" href="dft.html" />
    <link rel="prev" title="Initial Conditions for Trajectories" href="initconds.html" /> 
</head>

<body class="wy-body-for-nav"> 
  <div class="wy-grid-for-nav">
    <nav data-toggle="wy-nav-shift" class="wy-nav-side">
      <div class="wy-side-scroll">
        <div class="wy-side-nav-search" >

          
          
          <a href="../index.html" class="icon icon-home">
            TraSo
          </a>
<div role="search">
  <form id="rtd-search-form" class="wy-form" action="../search.html" method="get">
    <input type="text" name="q" placeholder="Search docs" aria-label="Search docs" />
    <input type="hidden" name="check_keywords" value="yes" />
    <input type="hidden" name="area" value="default" />
  </form>
</div>
        </div><div class="wy-menu wy-menu-vertical" data-spy="affix" role="navigation" aria-label="Navigation menu">
              <p class="caption" role="heading"><span class="caption-text">How to install:</span></p>
<ul>
<li class="toctree-l1"><a class="reference internal" href="../Install/get.html">Getting TraSo</a></li>
<li class="toctree-l1"><a class="reference internal" href="../Install/config.html">Setting it up</a></li>
</ul>
<p class="caption" role="heading"><span class="caption-text">Running simulations:</span></p>
<ul>
<li class="toctree-l1"><a class="reference internal" href="../Running/input.html">The Input File</a></li>
<li class="toctree-l1"><a class="reference internal" href="../Running/cli.html">Command Line Interface</a></li>
</ul>
<p class="caption" role="heading"><span class="caption-text">Background theory:</span></p>
<ul class="current">
<li class="toctree-l1"><a class="reference internal" href="normodes.html">Normal Modes</a></li>
<li class="toctree-l1"><a class="reference internal" href="initconds.html">Initial Conditions for Trajectories</a></li>
<li class="toctree-l1 current"><a class="current reference internal" href="#">Molecular Dynamics -MD-</a><ul>
<li class="toctree-l2"><a class="reference internal" href="#verlets-equations">Verlet’s Equations</a></li>
<li class="toctree-l2"><a class="reference internal" href="#velocity-verlet-algorithm">Velocity Verlet Algorithm</a></li>
<li class="toctree-l2"><a class="reference internal" href="#leapfrog-verlet-algorithm">Leapfrog Verlet Algorithm</a></li>
<li class="toctree-l2"><a class="reference internal" href="#obtaining-the-forces">Obtaining the Forces</a></li>
<li class="toctree-l2"><a class="reference internal" href="#references">References</a></li>
</ul>
</li>
<li class="toctree-l1"><a class="reference internal" href="dft.html">Quantum Chemistry - Density Functional Theory -DFT-</a></li>
</ul>
<p class="caption" role="heading"><span class="caption-text">Adding modules:</span></p>
<ul>
<li class="toctree-l1"><a class="reference internal" href="../Modules/qm.html">Adding a Quantum Mechanics Package Module</a></li>
<li class="toctree-l1"><a class="reference internal" href="../Modules/sampling.html">Adding another Sampling Module</a></li>
</ul>
<p class="caption" role="heading"><span class="caption-text">Code description:</span></p>
<ul>
<li class="toctree-l1"><a class="reference internal" href="../modules.html">TraSo</a></li>
</ul>

        </div>
      </div>
    </nav>

    <section data-toggle="wy-nav-shift" class="wy-nav-content-wrap"><nav class="wy-nav-top" aria-label="Mobile navigation menu" >
          <i data-toggle="wy-nav-top" class="fa fa-bars"></i>
          <a href="../index.html">TraSo</a>
      </nav>

      <div class="wy-nav-content">
        <div class="rst-content">
          <div role="navigation" aria-label="Page navigation">
  <ul class="wy-breadcrumbs">
      <li><a href="../index.html" class="icon icon-home" aria-label="Home"></a></li>
      <li class="breadcrumb-item active">Molecular Dynamics -MD-</li>
      <li class="wy-breadcrumbs-aside">
            <a href="../_sources/Background/verlet.rst.txt" rel="nofollow"> View page source</a>
      </li>
  </ul>
  <hr/>
</div>
          <div role="main" class="document" itemscope="itemscope" itemtype="http://schema.org/Article">
           <div itemprop="articleBody">
             
  <section id="molecular-dynamics-md">
<h1>Molecular Dynamics -MD-<a class="headerlink" href="#molecular-dynamics-md" title="Permalink to this heading"></a></h1>
<p>To simulate the molecular fragmentation after its excitation, a time dependency must be considered: propagation <a class="reference internal" href="#koopman2" id="id1"><span>[Koopman2]</span></a>. Contrary to conventional MD simulations where pressure or temperature are sought to remain constant, this case requires the energy to remain constant <a class="reference internal" href="#carra" id="id2"><span>[Carra]</span></a>. This means that the propagation of the molecule should happen without any solvent nor any artifact to constrain the molecule’s volume or its atoms velocity. The atomic velocity will be used as an indicator of the system’s energy through its relationship with the temperature <span class="math notranslate nohighlight">\(T\)</span>. Actually, another sampling method for the simulation of mass spectra involves setting a temperature, not atomic velocities <a class="reference internal" href="#carra" id="id3"><span>[Carra]</span></a>. Said relationship can be observed in <a class="reference internal" href="#equation-boltzvel">Equation (50)</a> as the average atomic velocity being proportional to the number of atoms <span class="math notranslate nohighlight">\(n\)</span> and the temperature, held equal by the constant <span class="math notranslate nohighlight">\(k_{B}\)</span>; the Boltzmann constant <a class="reference internal" href="#nelson" id="id4"><span>[Nelson]</span></a>.</p>
<div class="math notranslate nohighlight" id="equation-boltzvel">
<span class="eqno">(50)<a class="headerlink" href="#equation-boltzvel" title="Permalink to this equation"></a></span>\[\frac{1}{n} \left( \sum_{i=1}^{n} \frac{1}{2} m_{i} v_{i}^{2} \right) = \frac{3}{2} n k_{B} T\]</div>
<p>Each atom’s propagation during the MD simulation follows the Verlet equations in some way. The most basic algorithm for this is the simple Verlet algorithm.</p>
<section id="verlets-equations">
<h2>Verlet’s Equations<a class="headerlink" href="#verlets-equations" title="Permalink to this heading"></a></h2>
<p>Beginning with the idea of finding the position of a particle through a Taylor expansion, and truncating it at the third term, the equation of position takes the following shape:</p>
<div class="math notranslate nohighlight">
\[x \left( t + \Delta t \right) = x \left( t \right) + \Delta t \dfrac{d}{dt} x \left( t \right) +
\frac{1}{2} \Delta t^{2} \dfrac{d^{2}}{d t^{2}} x \left( t \right)\]</div>
<div class="math notranslate nohighlight" id="equation-verlet1a">
<span class="eqno">(51)<a class="headerlink" href="#equation-verlet1a" title="Permalink to this equation"></a></span>\[x_{s + 1} = x_{s} + v_{s} \Delta t + \frac{1}{2} a_{s} \Delta t^{2}\]</div>
<p>Considering that the fourth term of the expansion is not usually computed, a better approximation can be obtained by considering the previous step.</p>
<div class="math notranslate nohighlight" id="equation-verlet1b">
<span class="eqno">(52)<a class="headerlink" href="#equation-verlet1b" title="Permalink to this equation"></a></span>\[x_{s - 1} = x_{s} -     v_{s} \Delta t + \frac{1}{2} a_{s} \Delta t^{2}\]</div>
<p>By adding equations <a class="reference internal" href="#equation-verlet1a">Equation (51)</a> and <a class="reference internal" href="#equation-verlet1b">Equation (52)</a>, and solve for <span class="math notranslate nohighlight">\(x_{s + 1}\)</span>, the first Verlet equation arises:</p>
<div class="math notranslate nohighlight" id="equation-verlet1">
<span class="eqno">(53)<a class="headerlink" href="#equation-verlet1" title="Permalink to this equation"></a></span>\[x_{s + 1} = 2 x_{s} - x_{s - 1} + a_{s} \Delta t^{2}\]</div>
<p>Now, to compute the energy of the particle, the velocity is required. To compute the velocity, equation <a class="reference internal" href="#equation-verlet1b">Equation (52)</a> is subtracted from <a class="reference internal" href="#equation-verlet1a">Equation (51)</a> and solved for <span class="math notranslate nohighlight">\(v_{s}\)</span>.</p>
<div class="math notranslate nohighlight" id="equation-verlet2a">
<span class="eqno">(54)<a class="headerlink" href="#equation-verlet2a" title="Permalink to this equation"></a></span>\[v_{s} = \frac{x_{s + 1} - x_{s - 1}}{ 2 \Delta t }\]</div>
<p>Given that the denominator considers an increment of two time steps, and the numerator does so as well by advancing from <span class="math notranslate nohighlight">\(s - 1\)</span> to <span class="math notranslate nohighlight">\(s + 1\)</span>, a single step forward would require the initial position to be <span class="math notranslate nohighlight">\(x_{s}\)</span>. In that case, equation <a class="reference internal" href="#equation-verlet2a">Equation (54)</a> becomes:</p>
<div class="math notranslate nohighlight" id="equation-verlet2">
<span class="eqno">(55)<a class="headerlink" href="#equation-verlet2" title="Permalink to this equation"></a></span>\[v_{s + 1} = \frac{x_{s + 1} - x_{s}}{ \Delta t }\]</div>
<p>This is Verlet’s second equation of motion. However, equation <a class="reference internal" href="#equation-verlet2">Equation (55)</a> requires the result from <a class="reference internal" href="#equation-verlet1">Equation (53)</a>. And <a class="reference internal" href="#equation-verlet1">Equation (53)</a> needs the acceleration <span class="math notranslate nohighlight">\(a_{s}\)</span> for each step. The latter is computed as the product of the inverse of the mass <span class="math notranslate nohighlight">\(m\)</span> of the particle and the particle’s force <span class="math notranslate nohighlight">\(F_{s}\)</span>, which in turn is computed as the positional derivative of the potential energy given by the Force Field, or the electronic structure <a class="footnote-reference brackets" href="#id8" id="id5" role="doc-noteref"><span class="fn-bracket">[</span>1<span class="fn-bracket">]</span></a>. The latter leading to the <em>Ab-Initio</em> Molecular Dynamics -AIMD- method <a class="reference internal" href="#cuendet" id="id6"><span>[Cuendet]</span></a>.</p>
<div class="math notranslate nohighlight" id="equation-verlet3">
<span class="eqno">(56)<a class="headerlink" href="#equation-verlet3" title="Permalink to this equation"></a></span>\[a_{s} = \frac{F_{s}}{m} = m^{-1} F_{s} = m^{-1} \left( - \dfrac{d V}{d x_{s}} \right)\]</div>
<p>Finally, by solving equations <a class="reference internal" href="#equation-verlet1">Equation (53)</a>, <a class="reference internal" href="#equation-verlet2">Equation (55)</a> and <a class="reference internal" href="#equation-verlet3">Equation (56)</a> iteratively for all particles, their propagation is described over time <a class="reference internal" href="#cuendet" id="id7"><span>[Cuendet]</span></a>. The latter meaning that the atoms will be moved <span class="math notranslate nohighlight">\(s\)</span> times (<span class="math notranslate nohighlight">\(s\)</span> number of steps), and to do so, each atom’s position <span class="math notranslate nohighlight">\(x\)</span>, velocity <span class="math notranslate nohighlight">\(v\)</span> and force <span class="math notranslate nohighlight">\(F\)</span> will have to be recalculated each time. Each step will represent a given period of time <span class="math notranslate nohighlight">\(\Delta t\)</span> in reality. Therefore, as an example, a <span class="math notranslate nohighlight">\(1ps\)</span> simulation carried out over <span class="math notranslate nohighlight">\(2000\)</span> steps, translates to <span class="math notranslate nohighlight">\(\Delta t\)</span> being equal to <span class="math notranslate nohighlight">\(0.5fs\)</span> and <span class="math notranslate nohighlight">\(s\)</span> going from <span class="math notranslate nohighlight">\(0\)</span> to <span class="math notranslate nohighlight">\(2000\)</span>.</p>
<aside class="footnote brackets" id="id8" role="note">
<span class="label"><span class="fn-bracket">[</span><a role="doc-backlink" href="#id5">1</a><span class="fn-bracket">]</span></span>
<p>In the case of the electronic structure, methods such as Hartree-Fock or Density Functional Theory can be used.</p>
</aside>
</section>
<section id="velocity-verlet-algorithm">
<h2>Velocity Verlet Algorithm<a class="headerlink" href="#velocity-verlet-algorithm" title="Permalink to this heading"></a></h2>
<p>Another way to integrate the equations of motion, based on the same idea, is the Velocity Verlet Algorithm <a class="reference internal" href="#cuendet" id="id9"><span>[Cuendet]</span></a>. The latter begins by defining the position in a similar way as equation <a class="reference internal" href="#equation-verlet1a">Equation (51)</a>, but truncates the Taylor expansion to the third term as shown in equation <a class="reference internal" href="#equation-velocityver1">Equation (57)</a>.</p>
<div class="math notranslate nohighlight" id="equation-velocityver1">
<span class="eqno">(57)<a class="headerlink" href="#equation-velocityver1" title="Permalink to this equation"></a></span>\[x_{s + 1} = x_{s} +     v_{s} \Delta t + \frac{1}{2} a_{s} \Delta t^{2}\]</div>
<p>The important part here, is that the velocity <span class="math notranslate nohighlight">\(v_{s}\)</span> and the acceleration <span class="math notranslate nohighlight">\(a_{s}\)</span> should be known as initial conditions. The latter can be obtained in essentially the same way as with the case of equation <a class="reference internal" href="#equation-verlet3">Equation (56)</a>.</p>
<div class="math notranslate nohighlight" id="equation-velocityver2">
<span class="eqno">(58)<a class="headerlink" href="#equation-velocityver2" title="Permalink to this equation"></a></span>\[a_{s+1} = m^{-1} F \left( x_{s + 1} \right)\]</div>
<p>But the velocity does become different than what was seen in equation <a class="reference internal" href="#equation-verlet2">Equation (55)</a>. The approach is based on the same Taylor expansion used for the position.</p>
<div class="math notranslate nohighlight" id="equation-velocityver3">
<span class="eqno">(59)<a class="headerlink" href="#equation-velocityver3" title="Permalink to this equation"></a></span>\[v_{s + 1}  = v_{s} \Delta t + \frac{1}{2} \left( a_{s} + a_{s + 1} \right) \Delta t^{2}\]</div>
<p>By splitting equation <a class="reference internal" href="#equation-velocityver3">Equation (59)</a> and computing the part with <span class="math notranslate nohighlight">\(a_{s}\)</span> first and the leaving the part with <span class="math notranslate nohighlight">\(a_{s + 1}\)</span> last, the algorithm changes its name and becomes the Leapfrog Verlet Algorithm.</p>
</section>
<section id="leapfrog-verlet-algorithm">
<h2>Leapfrog Verlet Algorithm<a class="headerlink" href="#leapfrog-verlet-algorithm" title="Permalink to this heading"></a></h2>
<figure class="align-center" id="id15">
<span id="velver"></span><img alt="../_images/verlet.drawio.png" src="../_images/verlet.drawio.png" />
<figcaption>
<p><span class="caption-number">Fig. 8 </span><span class="caption-text">Flowchart of the Leapfrog algorithm following <a class="reference internal" href="#equation-leapfrog1">Equation (60)</a>, <a class="reference internal" href="#equation-leapfrog2">Equation (61)</a>, <a class="reference internal" href="#equation-leapfrog3">Equation (62)</a>, and <a class="reference internal" href="#equation-leapfrog4">Equation (63)</a>.</span><a class="headerlink" href="#id15" title="Permalink to this image"></a></p>
</figcaption>
</figure>
<p>In other packages, the Leapfrog algorithm is used to integrate the trajectory. In that case, the steps described in the next equations are followed <a class="reference internal" href="#phillips" id="id10"><span>[Phillips]</span></a>.</p>
<div class="math notranslate nohighlight" id="equation-leapfrog1">
<span class="eqno">(60)<a class="headerlink" href="#equation-leapfrog1" title="Permalink to this equation"></a></span>\[v_{s + \frac{1}{2}} = v_{s} + \frac{1}{2} a_{s} \Delta t\]</div>
<div class="math notranslate nohighlight" id="equation-leapfrog2">
<span class="eqno">(61)<a class="headerlink" href="#equation-leapfrog2" title="Permalink to this equation"></a></span>\[x_{s + 1} = x_{s} + v_{s + \frac{1}{2}} \Delta t\]</div>
<div class="math notranslate nohighlight" id="equation-leapfrog3">
<span class="eqno">(62)<a class="headerlink" href="#equation-leapfrog3" title="Permalink to this equation"></a></span>\[a_{s + 1} = m^{-1} F_{s + 1} = m^{-1} F \left( x_{s + 1} \right)\]</div>
<div class="math notranslate nohighlight" id="equation-leapfrog4">
<span class="eqno">(63)<a class="headerlink" href="#equation-leapfrog4" title="Permalink to this equation"></a></span>\[v_{s + 1} = v_{s + \frac{1}{2}} + \frac{1}{2} a_{s + 1} \Delta t\]</div>
<p>No initial conditions for the system must be established in the input of the MD software package, except for the timestep and the number of steps. The initial conditions for NAMD, Orca and TeraChem are provided as <em>restart files</em> for the coordinates and the velocities. Therefore, the previously calculated atomic positions and velocities, as described in other sections, are used by the MD package, which then propagates them following the previously described equations for each step in a cyclic manner, as shown in Figure <a class="reference internal" href="#velver"><span class="std std-ref">1</span></a>. As soon as all the steps have been done, the propagation will stop, and the last geometry will be the one used, in this case, to generate the CID-MS.</p>
</section>
<section id="obtaining-the-forces">
<h2>Obtaining the Forces<a class="headerlink" href="#obtaining-the-forces" title="Permalink to this heading"></a></h2>
<p>In the case of the MD, it is relevant to extract the gradient for each energy calculation. The forces involved at each step of the propagation are computed based on that gradient, as shown in equation <a class="reference internal" href="#equation-gradient">Equation (64)</a>. The gradient is calculated for the coordinates <span class="math notranslate nohighlight">\(\vec{q}\)</span> of each atomic nucleus in the molecule <a class="reference internal" href="#manathunga" id="id11"><span>[Manathunga]</span></a> <a class="reference internal" href="#cruzeiro" id="id12"><span>[Cruzeiro]</span></a>.</p>
<div class="math notranslate nohighlight" id="equation-gradient">
<span class="eqno">(64)<a class="headerlink" href="#equation-gradient" title="Permalink to this equation"></a></span>\[\begin{split}\nabla E \left( \vec{q} \right) = \vec{u}_{x} \left( \dfrac{\partial E }{\partial x} \right) + \vec{u}_{y} \left( \dfrac{\partial E }{\partial y} \right) + \vec{u}_{z} \left( \dfrac{\partial E }{\partial z} \right) = \begin{bmatrix}
        \left( \dfrac{\partial E }{\partial x} \right)\\
        \left( \dfrac{\partial E }{\partial y} \right)\\
        \left( \dfrac{\partial E }{\partial z} \right)
\end{bmatrix}\end{split}\]</div>
<p>noindent where <span class="math notranslate nohighlight">\(\vec{u}_{i}\)</span> is the unit vector for the <span class="math notranslate nohighlight">\(i\)</span> axis <a class="reference internal" href="#holden" id="id13"><span>[Holden]</span></a>.</p>
<p>After the initial conditions are generated, and the MD package propagates the trajectory following Verlet’s equations, the new forces have to be computed based on the atomic charges and electronic structure. The positions and velocities are easily obtained from the Verlet equations, but the Forces can only be obtained through the gradient. The latter allows the propagation to continue at each step <a class="reference internal" href="#melo" id="id14"><span>[Melo]</span></a>.</p>
</section>
<section id="references">
<h2>References<a class="headerlink" href="#references" title="Permalink to this heading"></a></h2>
<div role="list" class="citation-list">
<div class="citation" id="koopman2" role="doc-biblioentry">
<span class="label"><span class="fn-bracket">[</span><a role="doc-backlink" href="#id1">Koopman2</a><span class="fn-bracket">]</span></span>
<p>Koopman, J.; Grimme, S. Journal of the American Society for Mass Spectrometry 2021, 32, 1735–1751.</p>
</div>
<div class="citation" id="carra" role="doc-biblioentry">
<span class="label"><span class="fn-bracket">[</span>Carra<span class="fn-bracket">]</span></span>
<span class="backrefs">(<a role="doc-backlink" href="#id2">1</a>,<a role="doc-backlink" href="#id3">2</a>)</span>
<p>Carrà, A.; Spezia, R. Chemistry - Methods 2021, 1, 123–130.</p>
</div>
<div class="citation" id="nelson" role="doc-biblioentry">
<span class="label"><span class="fn-bracket">[</span><a role="doc-backlink" href="#id4">Nelson</a><span class="fn-bracket">]</span></span>
<p>Nelson, M. T.; Humphrey, W.; Gursoy, A.; Dalke, A.; Kale, L. V.; Skeel, R. D.; Schulten, K. The International Journal of High Performance Computing Applications 1996, 10, 251–268.</p>
</div>
<div class="citation" id="cuendet" role="doc-biblioentry">
<span class="label"><span class="fn-bracket">[</span>Cuendet<span class="fn-bracket">]</span></span>
<span class="backrefs">(<a role="doc-backlink" href="#id6">1</a>,<a role="doc-backlink" href="#id7">2</a>,<a role="doc-backlink" href="#id9">3</a>)</span>
<p>Cuendet, M. A.; Van Gunsteren, W. F. Journal of Chemical Physics 2007, 127, 184102.</p>
</div>
<div class="citation" id="phillips" role="doc-biblioentry">
<span class="label"><span class="fn-bracket">[</span><a role="doc-backlink" href="#id10">Phillips</a><span class="fn-bracket">]</span></span>
<p>Phillips, J. C. et al. The Journal of chemical physics 2020, 153.</p>
</div>
<div class="citation" id="manathunga" role="doc-biblioentry">
<span class="label"><span class="fn-bracket">[</span><a role="doc-backlink" href="#id11">Manathunga</a><span class="fn-bracket">]</span></span>
<p>Manathunga, M.; Aktulga, H. M.; Götz, A. W.; Merz, K. M. Journal of Chemical Information and Modeling 2023, 63, 711–717.</p>
</div>
<div class="citation" id="cruzeiro" role="doc-biblioentry">
<span class="label"><span class="fn-bracket">[</span><a role="doc-backlink" href="#id12">Cruzeiro</a><span class="fn-bracket">]</span></span>
<p>Cruzeiro, V. W. D.; Manathunga, M.; Merz, K. M.; Götz, A. W. Journal of Chemical Information and Modeling 2021, 61, 2109–2115.</p>
</div>
<div class="citation" id="holden" role="doc-biblioentry">
<span class="label"><span class="fn-bracket">[</span><a role="doc-backlink" href="#id13">Holden</a><span class="fn-bracket">]</span></span>
<p>Holden, Z. C.; Rana, B.; Herbert, J. M. Journal of Chemical Physics 2019, 150, 144115.</p>
</div>
<div class="citation" id="melo" role="doc-biblioentry">
<span class="label"><span class="fn-bracket">[</span><a role="doc-backlink" href="#id14">Melo</a><span class="fn-bracket">]</span></span>
<p>Melo, M. C.; Bernardi, R. C.; Rudack, T.; Scheurer, M.; Riplinger, C.; Phillips, J. C.; Maia, J. D.; Rocha, G. B.; Ribeiro, J. V.; Stone, J. E.; Neese, F.; Schulten, K.; Luthey-Schulten, Z. Nature Methods 2018 15:5 2018, 15, 351–354.</p>
</div>
</div>
</section>
</section>


           </div>
          </div>
          <footer><div class="rst-footer-buttons" role="navigation" aria-label="Footer">
        <a href="initconds.html" class="btn btn-neutral float-left" title="Initial Conditions for Trajectories" accesskey="p" rel="prev"><span class="fa fa-arrow-circle-left" aria-hidden="true"></span> Previous</a>
        <a href="dft.html" class="btn btn-neutral float-right" title="Quantum Chemistry - Density Functional Theory -DFT-" accesskey="n" rel="next">Next <span class="fa fa-arrow-circle-right" aria-hidden="true"></span></a>
    </div>

  <hr/>

  <div role="contentinfo">
    <p>&#169; Copyright 2023, Rony J. Letona.</p>
  </div>

  Built with <a href="https://www.sphinx-doc.org/">Sphinx</a> using a
    <a href="https://github.com/readthedocs/sphinx_rtd_theme">theme</a>
    provided by <a href="https://readthedocs.org">Read the Docs</a>.
   

</footer>
        </div>
      </div>
    </section>
  </div>
  <script>
      jQuery(function () {
          SphinxRtdTheme.Navigation.enable(true);
      });
  </script> 

</body>
</html>