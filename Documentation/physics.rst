physics package
===============

Submodules
----------

physics.sampling module
-----------------------

.. automodule:: physics.sampling
   :members:
   :undoc-members:
   :show-inheritance:

physics.spectrum module
-----------------------

.. automodule:: physics.spectrum
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: physics
   :members:
   :undoc-members:
   :show-inheritance:
