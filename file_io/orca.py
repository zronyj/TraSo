""" Module to make IO files to run in Orca

orca.py contains functions to create inp files or parse data from
output files.
"""

import os                  # A library to manage the file system
import shutil              # Library to manage more of the file system
from subprocess import run # Method to run external commands
from math import ceil      # Method to round up a float
import numpy as np         # Library to do basic scientific computing

# Masses for some elements
periodic = {"H": 1.008,
    "Li": 6.941,
    "Be": 9.012,
    "B": 10.811,
    "C": 12.011,
    "N": 14.007,
    "O": 15.999,
    "F": 18.998,
    "Na": 22.990,
    "Mg": 24.305,
    "Al": 26.982,
    "Si": 28.086,
    "P": 30.974,
    "S": 32.065,
    "Cl": 35.453}

def opt_freqs(xyz_file, orca_path, params):
    r"""Make input and perform optimization and frequencies

    This function is intended to create the input and run an
    optimization and frequencies calculation in Orca.

    Parameters
    ----------
    xyz_file : str
        The full path of the XYZ file, with the extension
    orca_path : str
        The complete path to the orca executable[3] for the
        MPI version to work
    params : dic
        Parameters to run Orca

    Returns
    -------
    None
        Two files: name.inp and name.out with the input
        and output data of the Orca calculation.
    """

    make_run_input(xyz_file, orca_path,
        params["Functional"] + " " + params["Diffusion"], params["Basis set"],
        "Opt Freq", params["CPU"], params["RAM"], params["Charge"],
        params["Multiplicity"])

def make_run_input(xyz_file, orca_path, method="B3LYP", basis="6-31G**",
    modifiers="", cores=8, memory=3072, charge=0, mult=1):
    r"""Make an Orca input file, and perform the calculation

    This function is intended to create the input file for a
    frequencies (thermochemistry) calculation in Orca.

    Parameters
    ----------
    xyz_file : str
        The full path of the XYZ file, with the extension
    orca_path : str
        The complete path to the orca executable `Setting up ORCA
        <https://sites.google.com/site/orcainputlibrary/setting-up-orca>`_
        for the MPI version to work
    method : str
        Either "HF", or the DFT functional `DFT calculations
        <https://sites.google.com/site/orcainputlibrary/dft-calculations>`_
        (e.g. PBE, B3LYP, M06)
    basis : str
        Any basis set available in Orca `Basis Sets
        <https://sites.google.com/site/orcainputlibrary/basis-sets>`_
        (e.g. 6-31G, def2-TZVP, aug-cc-pVDZ)
    modifiers : str
        Can be any one-line command to Orca (e.g. Opt, Freq)
    cores : int
        The number of processing cores to use in the given
        computer/node.
    memory : int
        The amount of memory to be used **per core** in the
        given computer/node.
    charge : int
        Total charge of the molecule
    mult : int
        Spin multiplicity of the molecule

    Returns
    -------
    None
        Two files : name.inp and name.out
        The input and output data of the Orca calculation.
    """

    xyz_file = xyz_file[:-4]

    # Orca input template
    template = f"""! {method} {basis} {modifiers}
%maxcore {memory}
%pal
nprocs {cores}
end
*xyz {charge} {mult}
"""
    # Read the XYZ file and extract the data
    with open(f"{xyz_file}.xyz", "r") as xyz:
        data = xyz.readlines()

    # Add the atoms and coordinates
    for i, line in enumerate(data):
        if i > 1:
            template += line
    # Finish the file
    template += "*\n"

    # Save the file
    with open(f"{xyz_file}.inp", "w") as o_inp:
        o_inp.write(template)

    # Run the calculation
    with open(f"{xyz_file}.out", "w") as out:
        orca_run = run([orca_path, f"{xyz_file}.inp"], stdout=out)

    # Inform if the process finished correctly
    print(f"Orca finished with code {orca_run.returncode}")

    # Clean the place
    for extension in [".densities", ".engrad", ".gbw",
        "_property.txt", "_trj.xyz"]:
        os.remove(f"{xyz_file}{extension}")

def parse_output(sim_name, output_file, periodic=periodic):
    r"""Extract data from an Orca output file

    This function is intended to extract the number of atoms,
    total mass, atom mass, coordinates, normal modes, and
    frequencies of the Orca output.

    Parameters
    ----------
    sim_name : str
        Name of the simulation
    output_file : str
        The name of the output file, with the extension.

    Returns
    -------
    na : int
        The number of atoms in the molecule
    mass : float
        The total mass of the molecule.
    masses : list of float
        A list of the masses of all atoms in the molecule
    coords : list of [str, float, float, float]
        A list of the atom symbol and its X, Y and Z coordinates
    nm : list of list of float
        A list with lists of numbers corresponding to the normal
        modes in the form of XYZXYZ... vectors.
    freqs : list of float
        A list of the frequencies (eigenvalues) of the normal
        modes
    """
    with open(output_file, 'r') as f:
        data = f.readlines()

    na_init = 0             # Line number for number of atoms
    coord_init = 0          # Line number for atom coordinates
    nm_init = 0             # Line number for normal modes
    freqs_init = 0          # Line number for frequencies

    # Check where are the lines of interest
    for ln in range(len(data)):
        # Find the line with the number of atoms in the molecule
        if "Number of atoms" in data[ln] and not na_init:
            na_init = ln
        # Find the line where the cartesian coordinates of the molecule start
        if "CARTESIAN COORDINATES (ANGSTROEM)" in data[ln]:
            coord_init = ln + 2
        # Find the line where the frequencies of the molecule start
        if "VIBRATIONAL FREQUENCIES" in data[ln]:
            freqs_init = ln + 5
        # Find the line where the normal modes of the molecule start
        if "NORMAL MODES" in data[ln] and not nm_init:
            nm_init = ln + 7

    # Extract the number of atoms in the molecule
    na_line = data[na_init].split()
    na = int(na_line[-1])

    # Get the coordinates of all atoms
    coord_lines = data[coord_init: coord_init + na]
    coord_lines = [l.split() for l in coord_lines]
    coords = []
    for l in coord_lines:
        coords.append(
            [float(l[i]) if i > 0 else l[i] for i in range(len(l))]
            )

    # Get the masses for each atom individually
    masses = np.array([ periodic[c[0]] for c in coords ])
    mass = masses.sum()

    # Get the frequencies
    freqs_num = na * 3
    freqs_lines = data[freqs_init: freqs_init + freqs_num]
    freqs_lines = [l.split() for l in freqs_lines]
    freqs = np.array([float(l[1]) for l in freqs_lines if float(l[1]) != 0])
    if all(freqs) < 0:
        raise ValueError("WARNING! Negative frequency detected!")

    # Get the normal modes
    # These come in groups of 6, as column-vectors of XYZXYZXYZ... components
    # https://gaussian.com/vib/
    nm_num = na * 3           # 3 normal modes per coordinate
    nm_chunks = ceil(na / 2)  # It's actually 3 * na / 6 for the 6 columns
    nm_lines = []

    # For each 6-column chunk of data ...
    for c in range(nm_chunks):
        temp_from = nm_init + c * (nm_num + 1)     # Compute the starting coord
        temp_to = nm_init + (c + 1) * (nm_num + 1) # COmpute the end coord
        nm_lines += data[temp_from : temp_to]      # Save the data

    # At this point the normal modes are stored as a list of strings

    nm_lines = [l.split() for l in nm_lines]       # Split the strings

    nm = [[] for r in range(nm_num)]               # Prepare a list for the NMs

    # Iterating over all the lists of broken strings
    for i in range(len(nm_lines)):
        ent = i%(nm_num + 1) - 1                   # Compute the entry number
        if ent != -1:                              # If it's not the first ...
            nm[ent] += [float(n) for n in nm_lines[i][1:]]

    # Create the normal modes matrix using NumPy
    nm = np.array(nm)

    # Check that the normal modes don't include translations and rotations
    # (first 5 or 6 normal modes)
    # https://chem.libretexts.org/Bookshelves/Physical_and_Theoretical_Chemistry_Textbook_Maps/Supplemental_Modules_(Physical_and_Theoretical_Chemistry)/Spectroscopy/Vibrational_Spectroscopy/Vibrational_Modes/Introduction_to_Vibrations
    check = nm.sum(axis=0)
    if check[5] <= 1E-15:
        nm = nm[:,6:].T.reshape(3*na - 6, na, 3)
    else:
        nm = nm[:,5:].T.reshape(3*na - 5, na, 3)

    return na, mass, masses, coords, nm, freqs

def make_md_input(sim_name, traj, na, coords, vels,
                    pwd, cwd, par_md, par_qm, par):
    r"""Make an Orca MD input file

    This function is intended to create the input file for a
    Ab Initio `Molecular Dynamics
    <https://sites.google.com/site/orcainputlibrary/molecular-dynamics>`_
    -AIMD- calculation in Orca.

    Parameters
    ----------
    sim_name : str
        The name of the simulation
    traj : int
        The number of trajectory being created
    na : int
        The number of atoms in the molecule
    coords : list of [str, float, float, float]
        The symbols and coordinates for all atoms
    vels : list of [str, float, float, float]
        The symbols and velocities for all atoms
    pwd : str
        TraSo's executable directory
    cwd : str
        The current working directory
    par_md : dict
        All the parameters for external Molecular Dynamics package
    par_qm : dict
        All the parameters for external Quantum Mechanics package
    par : dict
        All the parameters for the simulation

    Returns
    -------
    None
        Three files: name.inp, name.xyz and name.mdrestart with the input
        data of the Orca calculation, the coordinates and the restart
        conditions.
    """

    # Establish new trajectory's path
    traj_path = os.path.join(cwd, f"Trajectory_{traj:04d}")

    # If the directory exists, delete it
    if os.path.exists(traj_path):
        shutil.rmtree(traj_path)

    # Create the directory
    os.mkdir(traj_path)

    # Go into the new folder
    os.chdir(traj_path)

    # Save the XYZ coordinates
    # https://open-babel.readthedocs.io/en/latest/FileFormats/XYZ_cartesian_coordinates_format.html
    lines = []
    lines.append(f"{na}\n")
    lines.append("Molecule generated with TraSo\n")
    pretty_format = " {}\t{:>18.15f}\t{:>18.15f}\t{:>18.15f}\n"
    lines += [pretty_format.format(*a) for a in coords]

    with open(f"{sim_name}.xyz", "w") as xyz:
        xyz.writelines(lines)

    # Orca input template
    template = f"""! {par["QM"]["Functional"]} {par["QM"]["Diffusion"]} {par["QM"]["Basis set"]} MD
%maxcore {par_qm[par["QM"]["Software"]]["RAM"]}
%pal
nprocs {par_qm[par["QM"]["Software"]]["CPU"]}
end
%scf
    maxiter {par["QM"]["SCF"]}
    TolE 3e-5
    ConvCheckMode 1
end
%md
    Timestep {par["MD"]["Time step"]}_fs
    Thermostat None
    Dump Position Stride 1 Filename "trajectory.xyz"
    Restart
    Run {par["MD"]["Steps"]}
end
*xyzfile {par["QM"]["Charge"]} {par["QM"]["Multiplicity"]} {sim_name}.xyz
"""
    
    # Save the file
    with open(f"{sim_name}.inp", "w") as o_inp:
        o_inp.write(template)

    # Restart file template
    restart_template = f"""# ORCA AIMD Restart File, written by TraSo
&AtomCount
  {na}
&CurrentStep
  1
&SimulationTime
  {par["MD"]["Time step"]:.2f}
"""

    # Add the coordinates
    restart_template += "&Positions\n"
    cord_temp = "  {}\t{:>26.14f}\t{:>26.14f}\t{:>26.14f}\n"
    for a in coords:
        restart_template += cord_temp.format(*a)

    # Add the velocities
    restart_template += "&Velocities\n"
    vel_temp = "  {}\t{:>26.16e}\t{:>26.16e}\t{:>26.16e}\n"
    for v in vels:
        # Transform Angstrom / ps to Angstrom / fs
        scaled_vels = [v[0]] + [c * 1e-3 for c in v[1:]]
        restart_template += vel_temp.format(*scaled_vels)

    # Save the restart file
    with open(f"{sim_name}.mdrestart", "w") as md_res:
        md_res.write(restart_template)

    # Come back
    os.chdir(cwd)

def create_wrapper(cwd, pwd, par, par_qm):
    r"""Make a wrapper script for NAMD to use Orca

    This function creates an NAMD wrapper script for Orca to
    handle the QM part in the AIMD.

    Parameters
    ----------
    cwd : str
        The current working directory
    pwd : str
        TraSo's executable directory
    par : dict
        All the parameters for the simulation
    par_qm : dict
        All the parameters for external Quantum Mechanics package

    Returns
    -------
    None
        The python wrapper script in the "wrappers" folder, for NAMD
        to use Orca for the QM part.
    """

    # Where is the template for this?
    template_path = os.path.join(pwd, "templates",
                                    par_qm["Orca"]["Template"])

    with open(template_path, 'r') as t:
        template = t.read()

    # Where to put the wrapper?
    wrapper_path = os.path.join(pwd, "wrappers",
                                    par_qm["Orca"]["Template"])

    # Build the wrapper so that NAMD can use Orca
    with open(wrapper_path, 'w') as w:
        w.write(
                template.format(
                        path = par_qm["Orca"]["Path"],
                        cpu = par_qm["Orca"]["CPU"],
                        ram = par_qm["Orca"]["RAM"],
                        bset = par["QM"]["Basis set"],
                        theo = par["QM"]["Functional"],
                        diff = par["QM"]["Diffusion"],
                        scf = par["QM"]["SCF"],
                        chrg = par["QM"]["Charge"],
                        mltpl = par["QM"]["Multiplicity"]
                    )
            )

    os.chmod(wrapper_path, 0o755)

def run_md(cwd, inp_path, orca_par, sim_name):
    r"""Perform a propagation using Orca

    This function is intended to run an AIMD calculation in Orca.

    Parameters
    ----------
    cwd : str
        The current working directory
    inp_path : str
        The directory where the input file is located
    orca_par : dict
        The parameters for the orca executable[2] for the
        MPI version to work
    sim_name : str
        The name of the simulation
    

    Returns
    -------
    None
        One file: name.out with the output data of the Orca calculation.
    """

    # Move to the directory where the MD will be run
    os.chdir(inp_path)

    # Run the calculation
    with open(f"{sim_name}.out", "w") as out:
        orca_run = run([orca_par["Path"], f"{sim_name}.inp"],
            stdout=out)

    # Come back
    os.chdir(cwd)

def get_final_trajectory(sim_name, traj_wd):
    r"""Retrieve the last structure after the MD

    This function attempts to read the coordinates file after
    the MD analysis has finished, in order to extract the final
    coordinates.

    Parameters
    ----------
    sim_name : str
        The name of the simulation
    traj_wd : str
        The current working directory

    Returns
    -------
    coords : list of [str, float, float, float]
        A list of the atom symbol and its X, Y and Z coordinates
    
    """

    # Path for the file with the coordinates
    file_name = os.path.join(traj_wd, "trajectory.xyz")

    # Read data from file
    with open(file_name, 'r') as pdb:
        data = pdb.readlines()

    # Loop over all lines to locate the last structure
    last_structure = -1
    for i, l in enumerate(data):
        if "# ORCA AIMD" in l:
            last_structure = i

    # Extract the number of atoms
    atom_num = int(data[last_structure - 1])

    # Create the array with all the coordinates
    coords = []
    for idx in range(last_structure + 1, last_structure + 1 + atom_num):
        temp = data[idx].split()
        coords.append(
            [temp[0], float(temp[1]), float(temp[2]), float(temp[3])]
            )

    return coords