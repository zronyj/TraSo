Getting TraSo
+++++++++++++

.. note::
	**TraSo** has only been **tested on Linux**. If you wish to try it on Windows, please let me know. In principle, it should work having all the required dependencies. Nevertheless, any modifications that lead to better working software are greatly appreciated.

You can find **TraSo** in a GitLab repository: https://gitlab.com/zronyj/TraSo
To get it, you may proceed in either of the next two ways:

Downloading using Git
=====================

To do this, please make sure that you have Git installed in your computer. To do this, you can run the following command in a Terminal:

.. code-block:: bash

	git --version

Doing this should display your current Git version. If that isn't the case, you can install Git following `this guide <https://git-scm.com/book/en/v2/Getting-Started-Installing-Git>`_.

Once you have Git, you may use a terminal to navigate to your folder of choice (where TraSo will be installed), and enter the execute the following instruction:

.. code-block:: bash

	git clone https://gitlab.com/zronyj/TraSo.git

This will lead to you having a new **TraSo** folder, with the source code, inside your folder of choice. To continue with the installation, please proceed to the :doc:`Setting it up<config>` section.

Downloading from HTTP
=====================

To do this, please use your browser to navigate to the `TraSo Repository <https://gitlab.com/zronyj/TraSo>`_. Once you are there, you should see something similar to this:

.. image:: ../imgs/traso_repo.png
	:alt: **TraSo** repository

In this case, please click on the download button (the one before the blue **Code** button), and select one of the options: :code:`zip`, :code:`tar.gz`, :code:`tar.bz2` or :code:`tar`. Your download should begin shortly after.

Once your download finishes (it shouldn't take long), please put the compressed file in your folder of choice. Afterwards, you will need to decompress the file. To do so, from a terminal, you can execute the following commands:

.. code-block:: bash

	# For a ZIP file
	gunzip TraSo-main.zip

	# For tar.gz
	tar -xzvf TraSo-main.tar.gz

	# For tar.bz2
	tar -xvjf TraSo-main.tar.bz2

	# For tar
	tar -xf TraSo-main.tar

This should have extracted all the files in a folder called *TraSo-main*. To continue with the installation, please proceed to the :doc:`Setting it up<config>` section.