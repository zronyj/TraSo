<img src="./Documentation/imgs/traso_red.png" alt="TraSo" width="350px"/>

# TraSo - Trajectory Software

Last revision: July, 2023

---

- **Author:** Rony J. Letona
- **email:** [rony.letona@estudiante.uam.es](mailto:rony.letona@estudiante.uam.es)

---

## Description

This program is part of my master's thesis at the EMTCCM.

The program computes theoretical CID mass spectra. To do so, it takes a molecule in XYZ format, optimizes its geometry, calculates its normal modes and frequencies, samples those normal modes with in a microcanonical approach, creates initial condition files for a given number of trajectories, runs those trajectories using Born-Oppenheimer molecular dynamics, and analyzes the results as a Collision Induced Dissociation mass spectrum.

For details about installation, execution and background theory, please check the **[Official TraSo Documentation](https://traso.readthedocs.io/)**.

---

## License

Copyright (C) 2023  Rony J. Letona

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.