.. TraSo documentation master file, created by
   sphinx-quickstart on Thu Jun 29 13:44:55 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. image:: imgs/traso_red.png
   :width: 300

TraSo - Trajectory Software
===========================

**TraSo** - Trajectory Software is intended for unimolecular decomposition reactions by sampling a molecule's normal modes to a target energy. The main idea is to theoretically generate CID mass spectra (tandem MS/MS) after propagating the randomly sampled normal modes through *Ab Initio* Molecular Dynamics.

**TraSo** was fully written in Python to make it as versatile as possible. Thus, it doesn't require compilation and will run out-of-the-box once it is properly configured. Additionally, the idea is that anyone can add features to it and/or use only parts of it. Please remember citing the original papers related to the theory used by **TraSo** (shown in the last section).

Documentation
=============

.. toctree::
   :maxdepth: 2
   :caption: How to install:

   Install/get
   Install/config

.. toctree::
   :maxdepth: 2
   :caption: Running simulations:

   Running/input
   Running/cli

.. toctree::
   :maxdepth: 2
   :caption: Background theory:

   Background/normodes
   Background/initconds
   Background/verlet
   Background/dft

.. toctree::
   :maxdepth: 2
   :caption: Adding modules:

   Modules/qm
   Modules/sampling

Acknowledgements
================

This software package has come to existence thanks to the groundbreaking work of Hase [CIT1]_, Grimme [CIT2]_, Spezia [CIT3]_, and Martin-Somer [CIT4]_. All acknowledgements are to them for their great contribution to this branch of Computational Chemistry.

.. [CIT1] Hase, W. L.; Buckowski, D. G. *Chemical Physics Letters* 1980, 74, 284–287.

.. [CIT2] Grimme, S. *Angewandte Chemie International Edition* 2013, 52, 6306–6312.

.. [CIT3] Spezia, R.; Salpin, J. Y.; Gaigeot, M. P.; Hase, W. L.; Song, K. *Journal of Physical Chemistry A* 2009, 113, 13853–13862.

.. [CIT4] Martin-Somer, A.; Martens, J.; Grzetic, J.; Hase, W. L.; Oomens, J.; Spezia, R. *Journal of Physical Chemistry A* 2018, 122, 2612–2625.

.. toctree::
   :maxdepth: 2
   :hidden:
   :caption: Code description:

   modules
