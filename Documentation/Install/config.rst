Setting it up
+++++++++++++

**TraSo** is essentially a manager of different pieces of software. Even though it does some calculations, the heavy lifting is done by other packages. To use those packages, please download them and install them separately; you will need them before starting to use **TraSo**.

Additional Software
===================

As an example, two packages will be selected to explain how **TraSo** is configured and works:

* For Quantum Mechanics/Quantum Chemistry: `Orca <https://sites.google.com/site/orcainputlibrary/home>`_
* For Molecular Dynamics: `NAMD <https://www.ks.uiuc.edu/Research/namd/>`_

Both codes are mature, fast and reliable. Besides, the community using them is large enough to get answers to most questions. Please create an account in both sites (it's free), download the codes and install them.

.. note::
	Both packages will likely provide you with the binaries. You will just need to extract them into a location of your choice and *remember the path*. It is a common practice to put such packages in the :code:`/opt` folder in Linux, but you may choose a different location. The important part is for you to **remember the path** to the binaries.

Additional Files (for NAMD)
---------------------------

NAMD requires the use of parameter and topology files, even if they won't be used during the simulation. Because of that, the files have to be downloaded and included in the **TraSo** directory.

* To get those files, pleasae use your browser to navigate to the `MacKerell Lab website <http://mackerell.umaryland.edu/charmm_ff.shtml>`_.

* In there, you will find a table with the **Additive Force Field Files**. Please download the first entry (at the current date, it's :code:`toppar_c36_jul22.tgz`).

* Once you have downloaded the file, extract it like this: :code:`tar -xzvf toppar_c36_jul22.tgz`.

* Immediately afterwards, you will find a :code:`toppar` folder right next to the compressed file. Move that folder into the **TraSo** directory, alongisde :code:`traso.py`.


Getting Python and some Libraries
=================================

The first thing you need to use **TraSo** is Python. The latter may come in different flavors, but only a couple of them will be suggested in here. However, the most important thing to remember is that **TraSo requires Python 3**.

Aptitude
--------

This works primarily for users with a Linux distribution based on Ubuntu. In this case, Python 3 should already be installed in your system. However, you still need a couple of libraries. To install them, you may open a Terminal, type in the following instruction and execute it:

.. code:: bash
	
	sudo apt install python3-scipy python3-numpy python3-matplotlib

.. danger::
	You will need **super-user privileges** (sudo) to run this command!

PIP
---

If you already have a working version of Python 3 in your system, but you don't have sudo privileges, you may use PIP. PIP is a package manager for Python, and it allows you to install packages for your user only. In this case, you may open a Terminal, type in the following instruction and execute it:

.. code:: bash
	
	pip install scipy numpy matplotlib

.. warning::
	This approach, while simple, can lead to trouble, because PIP only installs the packages and doesn't handle package conflicts that nicely.

Anaconda
--------

This is the suggested approach. Even if you have Python in your Linux machine, `Anaconda <https://www.anaconda.com/download>`_ is a much more stable Python distribution with :code:`conda` as its package manager. The latter *can* resolve conflicts in a cleaner way. To install Anaconda, please go to the following website and download the binary: https://www.anaconda.com/download (this download may take a while). After downloading, you may proceed to install it (a good place to install such packages in Linux is the :code:`/opt` folder). Finally, once you have installed it, and you have activated Anaconda in your Terminal (the term :code:`(base)` should appear before your prompt in the Terminal), you may proceed to install the packages. You may now open a Terminal, type in the following instructions and execute them:

.. code:: bash
	
	conda install -c anaconda scipy numpy
	conda install -c conda-forge matplotlib


Configuring paths
=================

If you have set up your Quantum Chemistry and Molecular Dynamics packages, it is time to start configuring. The first step is to go to the folder where **TraSo** is installed and locate the file :code:`binpaths.json`. The latter contains the configuration used to run both the QM and MD packages. You may open the file using your preferred text editor and you should find this:

.. code-block:: json

	{
		"Orca" : {
			"CPU" : 2,
			"RAM" : 1024,
			"MD" : true,
			"QM" : true,
			"Frequencies" : true,
			"Path" : "/path/to/orca",
			"Template" : "o_wrapper_template.py"
		},
		"NAMD" : {
			"CPU" : 1,
			"MD" : true,
			"QM" : false,
			"Path" : "/path/to/namd2",
			"Template" : "template.namd"
		}
	}

As you can see, there are 2 main entries in the file: one for Orca and one for NAMD. The rest is rather self-explanatory, but some details should be remembered.

* Orca
	* :code:`"CPU"` sets the number of cores that Orca should use for a single calculation.
	* :code:`"RAM"` sets the amount of memory that Orca should use for a single calculation.
	* :code:`"MD"` establishes if the current program can perform Molecular Dynamics simulations.
	* :code:`"QM"` establishes if the current program can perform Quantum Chemistry calculations.
	* :code:`"Frequencies"` establishes if the current program can perform a Hessian Analysis, Thermochemistry or Frequencies calculation (they all mean the same).
	* :code:`"Path"` establishes the location of Orca's binary file. Notice that the path includes the name of the executable: **orca**
	* :code:`"Template"` sets the name of the template to create a wrapper, which NAMD can use to run Orca in every step of the propagation.
* NAMD
	* :code:`"CPU"` sets the number of cores that NAMD should use for a single calculation.
	* :code:`"MD"` establishes if the current program can perform Molecular Dynamics simulations.
	* :code:`"QM"` establishes if the current program can perform Quantum Chemistry calculations.
	* :code:`"Path"` establishes the location of NAMD's binary file. Notice that the path includes the name of the executable: **namd2**
	* :code:`"Template"` sets the name of the template to create NAMD's input files for each initial conditions stage.

Please fill the information in each field and do not change anything inside the quotation marks. If you want to include another software package, you may do so by adding an entry to the file. As an example, an entry for `TeraChem <http://www.petachem.com/products.html>`_ will be included now:

.. code-block:: json

	{
		"Orca" : {
			"CPU" : 2,
			"RAM" : 1024,
			"MD" : true,
			"QM" : true,
			"Frequencies" : true,
			"Path" : "/path/to/orca",
			"Template" : "o_wrapper_template.py"
		},
		"TeraChem" : {
			"CPU" : "1 0",
			"MD" : true,
			"QM" : true,
			"Frequencies" : true,
			"Path" : "/path/to/terachem",
			"Template" : "tc_wrapper_template.py"
		},
		"NAMD" : {
			"CPU" : 1,
			"MD" : true,
			"QM" : false,
			"Path" : "/path/to/namd2",
			"Template" : "template.namd"
		}
	}

Finally, please note that the provided paths have to be absolute paths. No relative paths can be handled by **TraSo**.

.. warning::
	You should keep in mind that if you plan on running several propagations in parallel, the number of cores has to be considered. For example: if you choose to use NAMD with a single core, Orca with 4 cores, and to perform 4 propagations at the same time, your CPU consumption will look like this:

	* 1 core for **TraSo**
	* 4 cores for NAMD (1 for each propagation)
	* 16 cores for Orca (4 for each propagation)

	This gives a total of 21 cores. If you have limited resources, remember to leave at least 1 or 2 cores for your Operating System to still be able to perform decently.

.. warning::
	The amount of memory assigned to Orca will be the memory used for each core when processing. Therefore, you need to multiply the assigned memory by the amount of cores and make sure that the number is lower than your total memory. Otherwise, your system may crash. For example, if you are running 2 propagations with 4 cores each, and you assign 2GB memory to Orca, your CPU and RAM consumption will look like this:

	* CPU
		* 1 core for **TraSo**
		* 2 cores for NAMD (1 for each propagation)
		* 8 cores for Orca (4 for each propagation)
	* RAM
		* 16GB for Orca (2GB for each core)

	In this case, the memory consumption by **TraSo** and NAMD is negligible. Neverhteless, the total is of 11 cores for processing and over 16GB of RAM. Using all your memory is not suggested. Remember to leave at least 2 to 5GB of RAM for your Operating System to still be able to perform decently.

Once you have filled the :code:`binpaths.json` file with the required parameters, you may proceed to the :doc:`The Input File<../Running/input>` section.