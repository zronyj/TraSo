Initial Conditions for Trajectories
+++++++++++++++++++++++++++++++++++

Monte Carlo Sampling in Microcanonical Ensemble
===============================================

Considering that the sample molecule is already in its equilibrium geometry, and following the path laid in the previous section, the next step when simulating its CID-MS is to reproduce the molecular excitation [Carra1]_. In order to do this, a target energy [1]_ is given to the molecule in a microcanonical or NVE ensemble [Spezia1]_. The latter is defined as an isolated system with a fixed number of particles (or atoms in this case), fixed volume, and an energy lying within an interval :math:`\left[E, E + \Delta \right]`, where :math:`\Delta` is very small. The Hamiltonian for said system is defined as :math:`\hat{H} \left(p, q\right)`, where :math:`p` and :math:`q` are the momenta and positions of the particles in phase space [Schwabl]_. While in its equilibrium geometry, the molecule's total thermal energy :math:`E` is assumed to be the lowest vibrational state it can have. When this is modeled by a harmonic oscillator, it translates to the sum of the energies of each normal mode at their lowest vibrational state :math:`E_{j}`, being :math:`N` is the number of atoms.

.. math::
	:label: sumenerg

	E = \sum_{j = 1}^{N} E_{j}

To distribute the target energy along all normal modes of the molecule, the latter are computed as described in previous section, using electronic structure methods (e.g. Hartree-Fock, Semi-Empirical, Density Functional Theory) [GaussianVib]_. Once those are known, a random sampling of said modes is performed by assigning different energy to each one, and making sure that the total energy of the molecule does not exceed the target energy (i.e. the energy of the inert gas) [Hase2]_. The sampling follows a uniform distribution, and the new energies are calculated according to :eq:`microcan`.

.. math::
	:label: microcan

	E_{i} = \left[ E - \sum_{j=1}^{i-1} E_{j} \right] \left( 1 - R^{\frac{1}{N - i}}_{*} \right)

Where :math:`E_{i}` is the new energy of the :math:`i^{th}` normal mode, :math:`E` is the target energy (i.e. the energy of the inert gas surrounding the molecule), :math:`R_{*}` is the random number of a continuous uniform distribution between :math:`0` and :math:`1`, and :math:`n` is the total number of atoms in the molecule [Hase1]_ [Hase2]_.  By assuming that each normal mode behaves as a quantum harmonic oscillator, then the energy of each oscillator can be taken as:

.. math::
	:label: qho_ener

	E_{j} = \left. h \nu_{j} \left( l + \frac{1}{2} \right) \right| l = 0, 1, 2, ...

where :math:`\nu_{j}` is the frequency of the :math:`j^{th}` normal mode, :math:`h` is the Planck constant and :math:`l` is an integer standing for the vibrational quantum number. This means that by acknowledging the energy of a quantum harmonic oscillator as displayed in :eq:`qho_ener`, all normal modes should begin at :math:`l = 0`. Therefore, the lowest energy :math:`E_{j}` of each normal mode in :eq:`microcan` is the Zero Point Energy -ZPE-. Having this in mind, the :eq:`microcan` can be re-written as follows:

.. math::
	:label: microcanmod

	E_{i} = \left[ E - \sum_{j=1}^{N} h \nu_{j} \left( l_{j} + \frac{1}{2} \right) \right] \left( 1 - R^{\frac{1}{N - i}}_{*} \right)

The sampling then becomes a matter of finding values of :math:`l \geq 0` such that the sum of the energies of all normal modes :math:`E_{i}` lay close to the target energy.

With the randomly sampled normal modes, all positions and momenta of each atom will be computed for the phase space of the :math:`i^{th}` normal mode. Therefore, understanding the calculation of the normal modes becomes necessary.


.. [1] The energy given to the molecule to excite it thermally.

Position and Velocity in Phase Space
====================================

When dealing with a molecule's normal modes, it should be kept in mind that the latter are 3-dimensional vectors of every atom's individual oscillatory motion. Therefore, to model their collective motion in phase space, they will all be considered as one harmonic oscillator. Whenever the molecule is at its equilibrium geometry, the oscillator will be considered to lie at :math:`x_{0}`. On the other hand, when all the atoms of the molecule move according to a normal mode, the oscillator's position will be given by :math:`x`. For convenience, :math:`x_{0}` will be defined to be at :math:`0` in phase space, meaning that any displacement :math:`x - x_{0}` will be actually given by :math:`x`.

Now, since the normal modes and frequencies are known, and considering the Hamiltonian :math:`\hat{H}` of the harmonic oscillator, the energy of each normal mode can be computed as:

.. math::
	:label: hamiltonian1

	\hat{H} = E = T + V = \frac{1}{2} m v^{2} + \frac{1}{2} k x^{2}

By expressing everything in terms of the oscillator's position :math:`x`, then :eq:`hamiltonian1` becomes:

.. math::
	:label: hamiltonian2

	 2 E = m \left( \dfrac{d x}{d \tau} \right)^{2} + k x^{2}

where :math:`\tau` represents time as a parameter of position :math:`x` and velocity :math:`v` in phase space. When attempting to solve the differential equation the shape of an ellipse arises:

.. math::
	:label: hamiltonian3

	 1 = \frac{m}{2E} x'^{2} + \frac{k}{2E} x^{2}

When plotting :eq:`hamiltonian3` for inconsequential values of :math:`m`, :math:`k` and :math:`E`, the ellipse becomes evident, as seen in Figure :ref:`1 <ellipse>`.

.. _ellipse:
.. figure:: ../imgs/ellipse.png
	:align: center

	Diagram of position :math:`x` and velocity :math:`x'`.

From this point, Figure :ref:`1 <ellipse>` makes it evident that the solution of the differential equation will be a trigonometric function. Additionally, the amplitudes of those functions can be extracted from the plot: they are the maximum and minimum values of the ellipse in each axis. However, for a more rigorous analysis, all the derivations will be carried out analytically.

Therefore, when solving the differential equation, the result should be a linear combination of trigonometric functions, of the parameter :math:`\tau`.

.. math::
	:label: diffeqsol

	x \left( \tau \right) = A \sin \left( \alpha \tau \right) + B \cos \left( \beta \tau \right)

To find out the identity of :math:`A`, :math:`B`, :math:`\alpha` and :math:`\beta`, some initial conditions have to be applied. The first one to be considered is that at :math:`\tau = 0`, where the oscillator is at its minimum, and its displacement :math:`x` should therefore be :math:`0` as well. Next are the energy considerations. However, this is more easily visualized when depicting the potential energy well with respect to :math:`x`:

.. _parabola:
.. figure:: ../imgs/phase_space_V.png
	:align: center

	Diagram of potential energy :math:`V` with respect to displacement :math:`x`.

As seen in Figure :ref:`2 <parabola>`, when :math:`x = 0`, the potential energy of the oscillator is :math:`0`.

.. math::
	:label: initcond1

	x \left( 0 \right)  = 0

When considering :eq:`initcond1`, the second term in :eq:`diffeqsol` is dismissed, making :math:`x` have a sinusodial behavior. Therefore, :eq:`diffeqsol` becomes:

.. math::

	x \left( \tau \right) = A \sin \left( \alpha \tau \right)

However, considering :eq:`hamiltonian1`, this means that the kinetic energy should be equal to the total energy of the system [2]_. On the other hand, when :math:`x = A`, where :math:`A` is the amplitude in :eq:`diffeqsol`, then the potential energy should be equal to the total energy of the system, and the kinetic energy should be :math:`0`. Therefore, remembering :eq:`hookee`, and re-arranging some terms, the amplitude :math:`A` can be found:

.. math::

	E = \frac{1}{2} k \left(x - x_0\right)^2

.. math::

	\left( \frac{2 E}{k} \right)^{\frac{1}{2}} = x - x_0 = A

where :math:`\nu` is the frequency in units :math:`\left[ s^{-1} \right]`, and :math:`A` is the amplitude, as it is the maximum displacement that the oscillator can have at any time.

Finally, the angular velocity :math:`\omega` is defined as the speed at which any particle completes a cycle (as seen in :eq:`omega1`), and is given in terms of radians over second. The latter implies that :math:`\omega` should be the term next to :math:`\tau` in order for the trigonometric function to evaluate to radians. Therefore, by considering that :math:`\alpha = \omega`, and the amplitude :math:`A` then the latter turns into:

.. math::
	:label: protox

	x \left( \tau \right) = \left( \frac{2 E}{k} \right)^{\frac{1}{2}} \sin \left( \omega \tau \right)

Now, by taking the derivative to find the velocity, the following arises:

.. math::
	:label: protov

	\dot{x} \left( \tau \right) = \left( \frac{2 E}{k} \right)^{\frac{1}{2}} \omega \cos \left( \omega \tau \right)

Re-inserting :eq:`protox` and :eq:`protov` into :eq:`hamiltonian2`, the value of :math:`\omega` can be obtained:

.. math::

	2 E = m \left[ \left( \frac{2 E}{k} \right)^{\frac{1}{2}} \omega \cos \left( \omega \tau \right) \right]^{2} + k \left[ \left( \frac{2 E}{k} \right)^{\frac{1}{2}} \sin \left( \omega \tau \right) \right]^{2}

.. math::

	2 E = m \left( \frac{2 E}{k} \right) \omega^{2} \cos^{2} \left( \omega \tau \right) + k \left( \frac{2 E}{k} \right) \sin^{2} \left( \omega \tau \right)

.. math::

	2 E = 2E \left( \frac{m}{k} \right) \omega^{2} \cos^{2} \left( \omega \tau \right) + 2E \sin^{2} \left( \omega \tau \right)

.. math::

	2 E = 2E \left[ \left( \frac{m}{k} \right) \omega^{2} \cos^{2} \left( \omega \tau \right) + \sin^{2} \left( \omega \tau \right) \right]

.. math::

	1 = \left( \frac{m}{k} \right) \omega^{2} \cos^{2} \left( \omega \tau \right) + \sin^{2} \left( \omega \tau \right)

The only way this equation can be true, is when :math:`\omega = \sqrt{ \frac{k}{m} }`, as shown in :eq:`omega2`. That being said, the solution then becomes:

.. math::

	x \left( \tau \right) = \left( \frac{2 E}{k} \right)^{\frac{1}{2}} \sin \left[ \left( \frac{k}{m} \right)^\frac{1}{2} \tau \right]

.. math::
	:label: position

	x \left( \tau \right) = \left( \frac{2 E}{k} \right)^{\frac{1}{2}} \sin \left( 2 \pi \nu \tau \right)

.. math::
	\dot{x} \left( \tau \right) = \left( \frac{2 E}{k} \right)^{\frac{1}{2}} \left( \frac{k}{m} \right)^\frac{1}{2} \cos \left[ \left( \frac{k}{m} \right)^\frac{1}{2} \tau \right]

.. math::
	:label: velocity

	\dot{x} \left( \tau \right) = \left( \frac{2 E}{m} \right)^{\frac{1}{2}} \cos \left( 2 \pi \nu \tau \right)

:eq:`position` and :eq:`velocity` already enable for the sampling of position and velocity in phase space. Nonetheless, several values of :math:`\tau` will lead to the exact same position and velocity because of these functions' periodicity. Therefore, restricting the domain of :math:`\tau` seems reasonable as it simplifies the sampling, and it makes it viable from a computational perspective; choosing random numbers from a small domain without the possibility to repeat a value would allow for a better analysis of the phase space.

To find the maximum and minimum values of the domain of :math:`\tau`, it is worth re-visiting what happens when the position is :math:`0` (potential energy is :math:`0` and kinetic energy becomes maximum) or when the velocity is :math:`0` (potential energy becomes maximum and kinetic energy becomes :math:`0`). The first case will lead to a trivial solution, but the second will actually produce the value of :math:`\tau` where the energy is maximum.

.. math::

	m \left[ \left( \frac{2 E}{m} \right)^{\frac{1}{2}} \cos \left( 2 \pi \nu \tau \right) \right]^{2} = 0

.. math::

	\left( \frac{2 E}{m} \right)^{\frac{1}{2}} \cos \left( 2 \pi \nu \tau \right) = 0

.. math::

	2 \pi \nu \tau = \cos^{-1} \left( 0 \right)

.. math::

	2 \pi \nu \tau = \left. \frac{\left( 2 n + 1 \right) \pi }{2} \right| n = 0, 1, 2, \ldots

.. math::

	\tau = \left. \frac{\left( 2 n + 1 \right) }{4 \nu} \right| n = 0, 1, 2, \ldots

Again, considering that the periodicity will lead to equal values, the maximum can be obtained by evaluating :math:`n = 0`. Therefore, the maximum energy can be obtained at time:

.. math::

	\tau_{E_{max}} = \pm \frac{1}{4 \nu}

This means that any value between :math:`\tau = 0` and :math:`\tau = \tau_{E_{max}}` is a valid time that preserves the energy, which also means that the space of all vibrations of the harmonic oscillator can be randomly sampled in phase space through the following equations:

.. math::
	:label: positionps

	x \left( R \right) = \sqrt{ \frac{2 E}{k} } \sin \left( R \frac{\pi}{2} \right)

.. math::
	:label: velocityps

	\dot{x} \left( R \right) = \sqrt{ \frac{2 E}{m} } \cos \left(R \frac{\pi}{2} \right)

Where :math:`R` is a random number in a uniform distribution from :math:`-1` to :math:`1`. Translating this into the case of any molecule, the position of the atoms and their velocities can be found for a normal mode, by multiplying the normal mode displacements found in :math:`\Xi` by :math:`x` and :math:`x'` [Hase1]_ [Hase2]_.

.. [2] Given that :math:`x'` will be squared according to :eq:`hamiltonian3`, and the latter is proportional to the kinetic energy, the relationship can also be observed in Figure :ref:`1 <ellipse>`; for :math:`x'` will be at one of its extreme amplitudes :math:`\pm \sqrt{\frac{2E}{m}}`, when :math:`x = 0`.

From Phase Space to Real Space
==============================

The final step to get the initial conditions for a given trajectory is to combine the position and velocity obtained in previous section, with the normal modes obtained in :doc:`Normal Modes<normodes>`. The latter is obtained for each normal mode in the following way:

.. math::
	:label: positionNMi

	\vec{r}^{\left( d \right)}_{i} = \vec{\xi}_{i} \cdot x_{i} \left( R_{i}, E_{i} \right)

.. math::
	:label: velocityNMi

	\vec{v}^{\left( d \right)}_{i} = \vec{\xi}_{i} \cdot \dot{x}_{i} \left( R_{i}, E_{i} \right)

Where :math:`\vec{\xi}_{i}` is the :math:`i^{th}` column vector of the normal mode matrix :math:`\Xi` (i.e. the :math:`i^{th}` normal mode of the molecule), and :math:`E_{i}` is the energy given to that normal mode. However, some considerations ought to be made before computing the final positions and velocities.

The first consideration to be made is that, since a different energy is assigned to each normal mode in the microcanonical sampling, each normal mode later requires a different sampling in phase space [Hu]_. This means that the final coordinates and velocities will be the sum of the positions and coordinates found for every normal mode.

.. math::
	:label: sumpos

	\vec{r}^{\left( d \right)} = \sum_{i = 1}^{N} \vec{r}^{\left( d \right)}_{i}

.. math::
	:label: sumvel

	\vec{v}^{\left( d \right)} = \sum_{i = 1}^{N} \vec{v}^{\left( d \right)}_{i}

The second consideration to be made is that the positions obtained from :eq:`positionNMi` are actually displacements (that is why there is a :math:`(d)` over the :math:`\vec{r}_{i}` vector). So, in order to obtain the coordinates of the new positions :math:`\vec{r}_{f}`, the displacements have to be added to the coordinates of the atom at equilibrium geometry :math:`\vec{r}_{0}`.

.. math::
	:label: posdisp

	\vec{r}_{f} = \vec{r}_{0} + \vec{r}^{\left( d \right)}

The third consideration is that the mass :math:`m` in :eq:`velocityps` for the velocity in phase space does not correspond to any atom or the whole molecule. In this case, and taking notice of :eq:`velocityNMi`, the inverse of the square root of the mass can be removed from :eq:`velocityps` and, to maintain consistency, the usage of normal mode :math:`\vec{\xi}_{i}` would be replaced by the mass-weighted normal mode :math:`\vec{o}_{i}`. This would result in :eq:`velocityps` being re-written as follows:

.. math::
	:label: velocityps2

	\breve{\dot{x}} \left( R \right) = \sqrt{ 2 E } \cos \left(R \frac{\pi}{2} \right)

\noindent And :eq:`velocityNMi` using the mass-weighted normal mode:

.. math::
	:label: velocityNMi2

	\vec{v}^{\left( d \right)}_{i} = \vec{o}_{i} \cdot \breve{\dot{x}}_{i} \left( R_{i}, E_{i} \right)

A fourth consideration is that the positions :math:`\vec{x} \left( R_{i}, E_{i} \right)` and velocities :math:`\vec{\dot{x}} \left( R_{i}, E_{i} \right)` in phase space, for all normal modes, can be encoded as vectors [Hase2]_:

.. math::
	:label: posvec

	\vec{x} = \left[ x_{1}, x_{2}, \ldots, x_{n} \right]

.. math::
	:label: velvec

	\vec{\breve{\dot{x}}} = \left[ \breve{\dot{x}}_{1}, \breve{\dot{x}}_{2}, \ldots, \breve{\dot{x}}_{n} \right]

Finally, bringing :eq:`positionNMi`, :eq:`sumpos`, :eq:`posdisp` and :eq:`posvec` for the position, and :eq:`velocityNMi2`, :eq:`sumvel` and :eq:`velvec` for velocities, the final positions :math:`\vec{r}_{f}` and velocities :math:`\vec{v}_{f}` in real space can be obtained through the following:

.. math::
	:label: positionR

	\vec{r}_{f} = \vec{r}_{0} + \Xi \cdot \vec{x}

.. math::
	:label: velocityR

	\vec{v}_{f} = O \cdot \vec{\breve{\dot{x}}}


Spurious Angular Momentum Removal
=================================

Setting the molecule's atomic positions in an arrangement different than the equilibrium geometry, and randomly adding velocity to said atoms, may cause an angular momentum to arise [Hase2]_. Such motion is unwanted when seeking the molecule's fragmentation based on normal mode vibration. That being the case, the molecule should undergo a procedure to remove this new angular momentum [Nagy]_.

The first step in removing said angular momentum is to shift the molecule into its center of mass. This can be done using equations :eq:`com` and :eq:`movecom`. Next, the molecule's angular momentum is to be computed [Nagy]_.

.. math::
	:label: angularmom

	\vec{L} = \sum_{i} \vec{r}_{i} \times \vec{p}_{i}

\noindent Where :math:`\vec{r}_{i}` is the position of atom :math:`i` relative to its center of mass, and :math:`\vec{p}_{i}` is its momentum.

Once this is done for all atoms, the inertia tensor is also computed for the molecule. This can be done using equation :eq:`inertia`. Once :math:`\vec{L}` and :math:`I` are computed, the new angular velocity :math:`\omega_{s}` can be found:

.. math::
	:label: angvel

	\omega_{s} = I^{-1} \vec{L}

\noindent Finally, the velocities computed from phase space can be adjusted by removing this new rotational velocity:

.. math::
	:label: newvel

	\vec{v}_{new} = \vec{v}_{f} - \omega_{s} \times \vec{r}

Where :math:`\vec{r}` is a :math:`3N` vector with the positions in Cartesian coordinates of all atoms in the molecule, and :math:`\vec{v}` is a :math:`3N` vector containing the velocities of all atoms in the molecule.

Having the new velocities :math:`\vec{v}_{new}`, the kinetic and potential energies are recomputed according to equation :eq:`hamiltonian1`. If the energy is not within :math:`0.01\%` of the intended energy, the positions and velocities are re-scaled using the quotient of the energy as factor. Then, the process is repeated from equation :eq:`angularmom` until the condition is fulfilled [Hase2]_.

.. math::
	:label: rescalex

	\vec{r'} = \vec{r} \left( \frac{E_{t}}{E} \right)^{\frac{1}{2}}

.. math::
	:label: rescalev

	\vec{v'} = \vec{v}_{new} \left( \frac{E_{t}}{E} \right)^{\frac{1}{2}}

Putting Everything Together
===========================

To compute the initial conditions, equation :eq:`microcanmod` will find the randomly assigned energy of normal mode :math:`i`, compute which is the closest level :math:`l` for that energy and use the energy of the normal mode at level :math:`l` according to equation :eq:`qho_ener`. With the final energy :math:`E_{i} \left[ l \right]`, the latter is substituted in expressions :eq:`positionps` and :eq:`velocityps` in order to obtain the position and velocity in phase space. These two are then multiplied by each normal mode (equation :eq:`mwnm`) to obtain the atomic positions and velocities (respectively) of the molecule. The energy and momenta of the molecule are re-computed along with its angular momentum. This new angular momentum is removed through equations :eq:`angularmom`, :eq:`angvel` and :eq:`newvel`, and the re-scaled energy is used to adjust the positions and velocities (see equations :eq:`rescalex` and :eq:`rescalev`). These positions and velocities are the initial conditions to be propagated by an MD package. The whole process is visually explained in Figure :ref:`3 <microcandiagram>`.

.. _microcandiagram:
.. figure:: ../imgs/microcan.drawio.png
	:align: center

	Flowchart of the algorithm to compute the intial conditions based on the microcanonical ensemble.

In orther words, each set of initial conditions (positions and velocities based on all normal modes) corresponds to the trajectory of a molecule when propagating. According to what was previously mentioned about CID-MS, a mass spectrum will arise after propagating several trajectories and adding the masses of the fragments in a cumulative manner. Finally, by considering how a set of several random initial conditions leads, through the propagation of trajectories, to a mass spectrum, this method is considered a Monte Carlo simulation [Metropolis]_ [Hastings]_.


References
==========

.. [Carra1] Carrà, A.; Spezia, R. Chemistry - Methods 2021, 1, 123–130.

.. [Spezia1] Spezia, R.; Martin-Somer, A.; Macaluso, V.; Homayoon, Z.; Pratihar, S.; Hase, W. L. Faraday Discussions 2016, 195, 599–618.

.. [Schwabl] Schwabl, F. Statistical mechanics; Springer, 2002; p 573.

.. [Hase1] Hase, W. L.; Buckowski, D. G. Chemical Physics Letters 1980, 74, 284–287.

.. [Hase2] Hase, W. L.; Duchovic, R. J.; Hu, X.; Komornicki, A.; Lim, K. F.; Lu, D. H.; Peslherbe, G. H.; Swamy, K. N.; Linde, S. R. V.; Zhu, L.; Varandas, A.; Wang, H.; Wolf, R. J. QCPE Bull. 1996, 16, 671.

.. [Hu] Hu, X.; Hase, W. L.; Pirraglia, T. Journal of Computational Chemistry 1991, 12, 1014–1024.

.. [Nagy] Nagy, T.; Vikár, A.; Lendvay, G. Journal of Chemical Physics 2016, 144.

.. [Metropolis] Metropolis, N.; Rosenbluth, A. W.; Rosenbluth, M. N.; Teller, A. H.; Teller, E. The Journal of Chemical Physics 1953, 21, 1087–1092.

.. [Hastings] Hastings, W. K. Biometrika 1970, 57, 97–109.