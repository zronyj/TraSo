The Input File
++++++++++++++

As with almost all theoretical chemistry software packages running over the command line, **TraSo** requires an input file. However, the latter has a very particular format which should make it easier for the user to remember all the keywords and parameters.

Coordinates
===========

The coordinates of the molecule to be studied have to be provided to **TraSo** as an :code:`.xyz` file. The latter should follow the file convention as described by `Wikipedia <https://en.wikipedia.org/wiki/XYZ_file_format>`_ and `OpenBabel <https://open-babel.readthedocs.io/en/latest/FileFormats/XYZ_cartesian_coordinates_format.html>`_:

* The file's first line has to contain the number of atoms in the molecule as an integer
* The file's second line has to contain text. Some usual uses for this line are setting the name of the molecule, adding a comment about the molecule, or adding important information about the molecule (e.g. energy, step in an MD propagation, type of isomer, molecular fingerprint).
* The next lines contain a 2-character column with the symbol of the atom's element, and 3 columns with the atom's X, Y and Z coordinates.

An example for the carbonic acid molecule looks like this:

.. code-block:: text

	6
	Molecule generated with TraSo
	 C	-0.521978491889376	 0.059014522293499	-0.183673288336538
	 O	-0.889255034288109	 1.456761093037222	-0.180614040346853
	 H	 0.046490175519849	 1.765793286984556	-0.362658848494013
	 H	-2.415416948909631	-0.093322984667482	 0.008343128447170
	 O	 0.549556534063759	-0.358083214415093	-0.069021629918692
	 O	-1.568796387773648	-0.603123525217626	 0.031520956864601

Please keep in mind the name of this file, as it will be useful in the input file.

Configuration (json)
====================

The input file is actually a configuration file. The idea is to provide **TraSo** with the right parameters to run the desired simulation. The JSON format was chosen as it already provides the user with a structure of the data, and an idea of what each parameter means. That being said, keep in mind that **all fields are mandatory**.

The input file has 4 main parts. Each contains specific information about the simulation or the program being used at some stage of the latter. An example of the contents of such a file is provided here, but a deeper explanation of each parameter will follow afterwards.

.. code-block:: json

	{
		"General" :
		{
			"Molecule name" : "Carbonic Acid",
			"Coordinates"   : "carbonic.xyz"
		},
		"Sampling" :
		{
			"Method"        : "microcanonical",
			"Energy"        : 100,
			"Trajectories"  : 10,
			"Parallel propagations" : 2,
			"Random key"    : 123456
		},
		"QM" :
		{
			"Software"      : "Orca",
			"Functional"    : "B3LYP",
			"Basis set"     : "6-31G*",
			"Diffusion"     : "D3BJ",
			"Charge"        : 0,
			"Multiplicity"  : 1,
			"SCF"           : 100
		},
		"MD" : 
		{
			"Software"      : "NAMD",
			"Steps"         : 1000,
			"Time step"     : 0.5
		}
	}

.. note::
	If you choose to run this simulation using Orca over 4 cores for each calculation, the total time should be roughly over a day, and you will be using a significant amount of computing power.

	Please remember to keep your machine as cold as you can.

.. warning::
	Please pay close attention to the curly brackets and the comas. Failure to put one or use additional ones will lead to an error.

General
-------

This section contains general information about the molecule. In here, you will have to set the name of the molecule and the name of the :code:`.xyz` file with the coordinates. The name of this last file will be used by **TraSo** as a reference for all stages of the simulation, so keep in mind that this name should something you recognize.

* **Molecule name**: Name of the molecule (used when plotting the final spectrum)
* **Coordinates**: Name of the :code:`.xyz` file with the molecule's coordinates. The file extension has to be included!

Sampling
--------

This section describes how the sampling of the normal modes should be carried out.

* **Method**: Choose the sampling method. Right now, only :code:`"microcanonical"` and :code:`"levels"` are available. In the future, more sampling techniques are expected to be included.
* **Energy**: Target energy for the molecule, in :math:`kcal/mol`. Keep in mind that this energy should be higher (maybe 2 or 3 times) than the molecule's Zero Point Energy.
* **Trajectories**: Number of trajectories to be prepared. This is the number of times that the experiment will be repeated to have a statistically significant result.
* **Parallel propagations**: Number of propagations to be run in parallel. Remember that this number will be later multiplied by the amount of cores assigned to your QM package, to yield the total amount of cores used by the simulation.
* **Random key**: The seed to create random numbers. Use :code:`false` if you don't consider this important.

QM
---

In this section, the parameters for all Quantum Chemical calculations have to be included.

* **Software**: Name of the QM software package, as stated in the :code:`binpaths.json` file.
* **Functional**: DFT functional to be used for all QM calculations.
* **Basis set**: Basis set to be used for all QM calculations.
* **Diffusion**: Grimme's diffusion parameter in the functional, used for all QM calculations.
* **Charge**: Molecule's charge.
* **Multiplicity**: Molecule's spin multiplicity.
* **SCF**: Maximum number of iterations for the SCF to converge.

MD
---

Finally, this section describes the parameters to run the propagation made by the Molecular Dynamics Package.

* **Software**: Name of the MD software package, as stated in the :code:`binpaths.json` file.
* **Steps**: Number of steps to propagate a molecule's initial conditions.
* **Time step**: Size of the steps :math:`\Delta t`, in femtoseconds :math:`fs`