Command Line Interface
++++++++++++++++++++++

Until now, **TraSo** has been written to work only through the command line. In the future, the aim is to make it work as a web server to be able to launch a simulation, monitor it and analyze it dynamically using a Graphical User Interface. In the meantime, though, a word should be said about the CLI.

A complete simulation in **TraSo** consists of *4 stages*:

Stages
======

Optimization and Frequencies (-o)
---------------------------------

In the first one, the molecule's geometry is optimized (the molecule's energy is brought to a minimum by adjusting the atomic coordinates), and its frequencies are calculated (each bond is viewed as a harmonic oscillator and their oscillator constants :math:`k_{i,j}` are computed). Both processes are carried out by the QM package.

In order for **TraSo** to only perform this stage, the :code:`-o` flag should be used. This means that the whole command should look something like this:

.. code-block:: bash
	
	/path/to/traso.py -o -i input_file.json > output_file1.log

Doing so, the remaining files will be ready for the next stage of the simulation. It should be pointed out that this process may take a few minutes.

.. warning::
	**TraSo** may delete some of the output files produced by the QM software, since those are irrelevant for the subsequent stages. However, this can be changed by altering the QM software's module.

Normal Mode Sampling and Initial Conditions (-n)
------------------------------------------------

In the second stage, the molecule's normal modes are randomly sampled to produce molecular initial conditions which could happen when adding energy to the molecule (a target energy). In order for **TraSo** to do this, *it requires the results of the previous frequencies calculation*. The idea of separating them is that they can be done in a different time.

To perform a normal mode sampling and initial condition generation, the :code:`-n` flag should be used. Also, make sure that the results from the **Optimization and Frequencies** stage are in the same folder as the input file and make sure to run the following command at that location:

.. code-block:: bash
	
	/path/to/traso.py -n -i input_file.json > output_file2.log

This step should be fast (a few seconds tops) and it will use all of the available cores in the CPU.

.. note::
	**TraSo** will produce a folder for each trajectory. This means that the conditions of each trajectory may be analyzed individually. Additionally, for the next stage, if some folders are removed from the working directory, **TraSo** will work with the remaining ones. This can be used to split a large simulation to be run over several computers.

*Ab Initio*/Born-Oppenheimer Molecular Dynamics (-a)
----------------------------------------------------

In the third stage, the molecule's coordinates and velocities are propagated by the MD package using Verlet's equations and, at each step, the electronic structure of the molecule is re-calculated by the QM package to set up the conditions of the next step. The process is repeated for :math:`n` steps, each one representing the real motion of a molecule in a :math:`\Delta t` time step.

This stage can be performed by using the :code:`-a` flag when running the simulation. Again, make sure that the results from the **Normal Mode Sampling and Initial Conditions** stage are in the same folder as the input file and make sure to run the following command at that location:

.. code-block:: bash
	
	/path/to/traso.py -a -i input_file.json > output_file3.log

.. danger::
	This is the stage that will require most time, as each trajectory requires :math:`n` steps, which translate into :math:`n` QM calculations. Depending on the amount of steps and trajectories, the total time for this stage may be between several hours, to several weeks.

.. note::
	Since this stage can be performed over different computers, the easiest way to do it is by taking some of the :code:`Trajectory_XXXX` folders into the other computer.

	For example, if you chose to produce the initial conditions for 100 trajectories, you may keep folders :code:`Trajectory_0000` to :code:`Trajectory_0049` in **computer 1**, and take folders :code:`Trajectory_0050` to :code:`Trajectory_0099` to **computer 2**. Then, by running this stage on both computers, the results will be ready to **be brought back together** and analyzed by the next stage.

Create CID Mass Spectrum (-s)
-----------------------------

In the fourth stage, the last geometry of the molecule is analyzed: the atomic coordinates are checked to determine if the molecule underwent fragmentation, and if so, the mass of each fragment is measured and plotted as a spectrum. This is the last stage and said spectrum should be the CID mass spectrum of the molecule.

To carry out this stage, the :code:`-s` flag should be used. Please rememeber that **TraSo** will analyze only the folders present in the working directory. Therefore, make sure that the results from the **Born-Oppenheimer Molecular Dynamics** stage are in the same folder as the input file and make sure to run the following command at that location:

.. code-block:: bash
	
	/path/to/traso.py -s -i input_file.json > output_file4.log

Combining Flags and Not Providing Flags
---------------------------------------

All previous flags can be combined in order for **TraSo** to carry out several stages continuously.

Complete Simulation
~~~~~~~~~~~~~~~~~~~

For instance, to run a complete simulation with all 4 stages, you may proceed in one of the next two ways:

.. code-block:: bash

	/path/to/traso.py -onas -i input_file.json > output_file.log
	/path/to/traso.py -i input_file.json > output_file.log

The latter being the preferred one; providing no flags implies a complete simulation.

Partial Simulation
~~~~~~~~~~~~~~~~~~

Nevertheless, the flags should be provided as a non-broken sequence.

For example, to run the first and second stages, the following command can be used:

.. code-block:: bash
	
	/path/to/traso.py -on -i input_file.json > output_file.log

Another example could be to run stages 2, 3 and 4:

.. code-block:: bash

	/path/to/traso.py -nas -i input_file.json > output_file.log

However, a broken sequence command implies that the results from the previous stage won't be there for the next stage to happen. Therefore, commands like the following will lead to error:

.. danger::
	Broken sequence commands that will lead to error:

	.. code-block:: bash
	
		/path/to/traso.py -oas -i input_file.json > output_file.log
		/path/to/traso.py -ons -i input_file.json > output_file.log
		/path/to/traso.py -ns -i input_file.json > output_file.log
		/path/to/traso.py -oa -i input_file.json > output_file.log
		/path/to/traso.py -os -i input_file.json > output_file.log
