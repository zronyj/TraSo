#!/usr/bin/env python3

# ------------------ Importing libraries ------------------
import os                              # A library to manage the file system
import shutil                          # Library to manage more of the file system
from sys import argv                   # Method to take command line arguments
from time import time                  # Method to ask for the specific time in seconds
import numpy as np                     # Library to do basic scientific computing
import argparse as ap                  # To take command line arguments nicely
from importlib import import_module    # Method to import libraries in-line
from multiprocessing import Pool       # Method to perform multithreaded processes
from file_io import io                 # Library to perform TraSo's I/O
from physics import sampling, spectrum # Libraries to do the physics and spectra

# ------------------- Defining functions ------------------

def normal_mode_analysis(sim_name, par, par_qm):
    r"""Optimize geometry, compute frequencies and parse output

    This function is intended to take the molecular geometry
    optimize it and compute its frequencies with a QM software
    of choice, and parse the output of it to return the data
    needed for a normal mode sampling.

    Parameters
    ----------
    sim_name : str
        Name of the simulation
    par : dic
        A dictionary with all the parameters of the calculation
    par_qm : dic
        A dictionary with all the parameters for QM calculations

    Returns
    -------
    na : int
        The number of atoms in the molecule
    mass : float
        The total mass of the molecule.
    masses : list of float
        A list of the masses of all atoms in the molecule
    coords : list of [str, float, float, float]
        A list of the atom symbol and its X, Y and Z coordinates
    nm : list of list of float
        A list with lists of numbers corresponding to the normal
        modes in the form of XYZXYZ... vectors.
    freqs : list of float
        A list of the frequencies (eigenvalues) of the normal
        modes
    """

    # ------------------ Import QM package --------------------
    # ---------- for Optimization and Frequencies -------------

    # Try to use the suggested software for QM frequencies calculation
    if par_qm[par["QM"]["Software"]]["Frequencies"]:
        # Importing the required module to do the QM part
        qm_fq = import_module(f"file_io.{par['QM']['Software'].lower()}")

        print(("\n Module for optimization and frequencies "
            f"selected: {par['QM']['Software']}"))
    # If the suggested software is unavailable, use one of the others
    else:
        print("\n The suggested QM package can't perform a frequencies")
        print("calculation. Looking for an alternative ...")

        for key, value in par_qm:
            if value["Frequencies"]:
                qm_fq = import_module(f"file_io.{key.lower()}")

                print(("\n Module for optimization and frequencies "
                    f"selected: {key}"))
                break

    # ------------------ OptFreq Calculation ------------------

    print("\n Optimization and frequencies calculation started.")

    # Check if there's an output file already and, if not,
    # perform an optimization and frequencies calculation
    if not(os.path.exists(f"{sim_name}.out")):

        qm_fq.opt_freqs(
            par['General']['Coordinates'],
            par_qm[par["QM"]["Software"]]["Path"],
            par["QM"] | par_qm[par["QM"]["Software"]]
            )

    print("\n Optimization and frequencies calculation finished.")

    # ---------------- Parse Output of OptFreq ----------------

    print("\n Parsing output of frequencies calculation ...")

    # Get all the information from the ORCA output file
    na, mass, masses, coords, nm, freqs = qm_fq.parse_output(
        sim_name,
        f"{sim_name}.out"
        )

    print(f"\n - Number of atoms: {na}")
    print(f" - Molecule mass: {mass:8.2f}")

    # Compute the ZPE of the molecule
    zpes = sampling.get_ZPEs(freqs)
    zpe = sum(zpes)

    print(
        (f"\n The Zero Point Energy for this molecule is {zpe:.4E} J, "
         f"or {sampling.j_to_kcal(zpe):.4F} kcal/mol.")
        )

    return na, mass, masses, coords, nm, freqs

def normal_mode_sampling(par, coords, masses, freqs, nm):
    r"""Randomly sample the normal modes

    This function computes a random position and velocity for
    all atoms in the molecule, according to a provided target
    energy, or according to a provided file with the normal
    mode energy levels.

    Parameters
    ----------
    par : dic
        A dictionary with all the parameters of the calculation
    coords : list of [str, float, float, float]
        A list of the atom symbol and its X, Y and Z coordinates
    masses : list of float
        A list of the masses of all atoms in the molecule
    freqs : list of float
        A list of the frequencies (eigenvalues) of the normal
        modes
    nm : list of list of float
        A list with lists of numbers corresponding to the normal
        modes in the form of XYZXYZ... vectors.

    Raises
    ------
    ValueError
        If the number of provided trajectories doesn't match with the
        provided levels.
        If the number of normal modes in the levels file doesn't match
        with the computed normal modes.

    Returns
    -------
    new_coords : list of [str, float, float, float]
        Coordinates of the atom: Symbol and X, Y and Z
    vels : list of [str, float, float, float]
        Velocities of the atom: Symbol and X, Y and Z
    new_Etot : list of float
        Energies of each sampled normal mode
    """

    # ------------- Select random number and seed -------------

    # Setting the random numbers for deterministic results
    if "Random key" in par["Sampling"]:
        np.random.seed(par["Sampling"]["Random key"])
        print(f"\n Random seed for this run is: {par['Sampling']['Random key']}")
    else:
        random_seed = np.random.randint(1E6, 1E7)
        np.random.seed(random_seed)
        print(f"\n Random seed for this run is: {random_seed}")

    # Generate random numbers for the trajectories
    seed_field = np.random.randint(
                    1000000,
                    9999999,
                    size=par["Sampling"]["Trajectories"]
                    )

    # -------------- Establishing initial energy --------------

    # Rescale the target energy to J/mol
    Etot = sampling.kcal_to_j(par["Sampling"]["Energy"])

    print(
        (f"\n According to the provided energy, the new system energy\n should be "
         f"around {Etot:.4E} J, or {par['Sampling']['Energy']:.4F} kcal/mol.")
        )

    # --------------- Create arrays for results ---------------

    # Getting variables ready for all the data
    new_coords = [0]*par["Sampling"]["Trajectories"]
    vels = [0]*par["Sampling"]["Trajectories"]
    new_Etot = [0]*par["Sampling"]["Trajectories"]

    # ------------- Select the sampling technique -------------

    # Microcanonical sampling
    if par["Sampling"]["Method"] == 'microcanonical':
        # Set all levels to 0
        lvls = [[0]*len(nm)]*par["Sampling"]["Trajectories"]

    # Levels sampling
    elif par["Sampling"]["Method"] == 'levels':

        # Read the levels from a file
        with open('levels.dat', 'r') as lvls_file:
            lvls_lines = lvls_file.readlines()

        # Check if there are enough lines to match trajectories
        if len(lvls_lines) != par["Sampling"]["Trajectories"]:
            raise ValueError(
                ("The number of levels trajectories is not the same as "
                "the number of requested trajectories.")
                )

        # Check if there are enough levels to match the normal modes
        lvls_lines = [l.split() for l in lvls_lines]
        if len(lvls_lines[0]) != len(nm):
            raise ValueError(
                ("The number of provided levels is not the same as "
                "the number of normal modes.")
                )

        # Build the levels array
        lvls = [[int(i) for i in l] for l in lvls_lines]

    # ---------------- Sample the normal modes ----------------

    print("\n Sampling of normal modes started. ")

    # Put all arguments in the same item for each set of random numbers
    items = [
    (coords, masses, freqs, nm, Etot, par["Sampling"]["Method"], lvls[i], rn)\
    for i, rn in enumerate(seed_field)
    ]

    # Doing the process over all available cores
    with Pool() as p:
        # Compute the coordinates, velocity vectors and new energies
        everything = p.starmap(sampling.get_initial_conditions, items)

    # Isolating terms ...
    new_coords, vels, new_Enm, new_Etot, n_modes = zip(*everything)

    print("\n Sampling of normal modes finished. ")

    # ------------- Save the energy levels to CSV -------------

    # Preparing data for detailed output
    title = "Traj     Seed "
    title += "  ".join([f"  Mode_{i+1:>03d}" for i in range(len(n_modes[0]))])
    title += "     Est_Energ_(J)  Scaled_Energ_(J)  Scaled_Energ_(kcal/mol)\n"
    exc_data = [title]
    for t in range(par["Sampling"]["Trajectories"]):
        modes = [f"{m:>10d}" for m in n_modes[t]]
        exc_data.append(f"{t:04d}  {seed_field[t]} " +\
        "  ".join(modes) + f"  {new_Enm[t]:16.8e}  {new_Etot[t]:16.8e}" +\
        f"  {sampling.j_to_kcal(new_Etot[t]):>23.4f}\n")

    # Save filed with details on the trajectories
    with open("sampled_modes.csv", "w") as mccm:
        mccm.writelines(exc_data)

    # ------------- Compute new energies averages -------------

    new_avg_energy = sum(new_Etot)/par["Sampling"]["Trajectories"]

    print(
        (f"\n But the average system energy will be around "
         f"{new_avg_energy:.4E} J. or "
         f"{sampling.j_to_kcal(new_avg_energy):.4f}"
         " kcal/mol.")
        )

    return new_coords, vels, new_Etot

def make_initial_conditions(sim_name, na, coords, vels,
                            pwd, cwd, par_md, par_qm, par):
    r"""Make a folders and input files for each trajectory

    This function is intended to create the input files for
    Ab Initio Molecular Dynamics -AIMD- calculations.

    Parameters
    ----------
    sim_name : str
        The name of the simulation
    na : int
        The number of atoms in the molecule
    coords : list of [str, float, float, float]
        The symbols and coordinates for all atoms
    vels : list of [str, float, float, float]
        The symbols and velocities for all atoms
    pwd : str
        TraSo's executable directory
    cwd : str
        The current working directory
    par_md : dict
        All the parameters for external Molecular Dynamics package
    par_qm : dict
        All the parameters for external Quantum Mechanics package
    par : dict
        All the parameters for the simulation

    Returns
    -------
    None
        Folders: Trajectory_traj
    """

    # -------------- Creating Initial Conditions --------------

    picoseconds = par["MD"]["Steps"] * par["MD"]["Time step"] / 1000

    print(f"\n The time of the simulation will be of: {picoseconds} ps\n")

    # Importing the required module to do the QM part
    md = import_module(f"file_io.{par['MD']['Software'].lower()}")

    print(f" Creating trajectory files for package {par['MD']['Software']}.")

    # Create all the input files for all trajectories in separate folders
    for traj in range(par["Sampling"]["Trajectories"]):

        md.make_md_input(sim_name, traj, na, coords[traj],
            vels[traj], pwd, cwd, par_md, par_qm, par)

    print((f"\n {par['Sampling']['Trajectories']} folders created, each for "
        "a unique trajectory."))

def start_bomd(sim_name, cwd, par, par_md):
    r"""Run the simulation

    This function runs the trajectories in parallel

    Parameters
    ----------
    sim_name : str
        The name of the simulation
    cwd : str
        The current working directory
    par : dict
        All the parameters for the simulation
    par_md : dict
        All the parameters for external Molecular Dynamics package

    Returns
    -------
    None
        Output files for all the simulations
    """

    # Compute the name of all the folders
    traj_args = []
    for t in range(par["Sampling"]["Trajectories"]):
        temp_path = os.path.join(cwd, f"Trajectory_{t:04d}")
        traj_args.append((cwd,
                        temp_path,
                        par_md[par["MD"]["Software"]],
                        sim_name))

    # Import the MD software package module
    md = import_module(f"file_io.{par['MD']['Software'].lower()}")

    print(" Starting with the calculation of trajectories ...")

    # Running the QM/MD simulation
    with Pool(par["Sampling"]["Parallel propagations"]) as p:

        # Run QM/MD
        jobs = p.starmap(md.run_md, traj_args)

    print(" Calculation of trajectories finished.")

def compute_spectrum(sim_name, cwd, par, energy, thr=spectrum.cov_rads):
    r"""Compute the mass spectrum of the molecule

    This function extracts the last structure of all the trajectories,
    determines the fragments, computes the masses and produces the
    mass spectrum of the molecule

    Parameters
    ----------
    sim_name : str
        The name of the simulation
    cwd : str
        The current working directory
    par : dict
        All the parameters for the simulation
    energy : float
        Average energy of the simulation
    thr : dict
        The covalent radii of the elements in the molecule

    Returns
    -------
    None
        Output files for all the simulations
    """

    traj_masses = {}
    all_masses = []

    # Get all the folders
    trajectories = os.listdir(cwd)

    # Filter out everything except trajectories
    trajectories = [t for t in trajectories if "Trajectory_" in t]

    # Import the MD software package module
    md = import_module(f"file_io.{par['MD']['Software'].lower()}")

    # Counter to see how many trajectories yielded results
    good_trajs = 0

    # Loop over all trajectories ...
    for traj in trajectories:
        # Where is the trajectory?
        traj_path = os.path.join(cwd, traj)
        try:
            # Get coords of final traj
            coords = md.get_final_trajectory(sim_name, traj_path)
            # Compute adjacency matrix
            adjm = spectrum.adjacency_matrix(coords, thr)
            # Compute masses of fragments
            frgmt, frags, traj_masses[traj] = spectrum.get_fragments(coords, adjm)
            all_masses += traj_masses[traj]
            good_trajs += 1
        except Exception as e:
            print(f" Trajectory {traj} did not produce any final coordinates file!")

    # Get unique masses in spectrum
    unique_masses = set(all_masses)

    # Accumulate equal masses to make actual mass spectrum
    accu_spect = []
    for f in unique_masses:
        accu_spect.append([f, all_masses.count(f)])

    # Sort the masses
    accu_spect.sort(key=lambda x: x[0])

    # Save the fragments of each trajectory in a file
    spectrum.save_fragments(traj_masses, accu_spect)

    # Plot the mass spectrum
    spectrum.make_spectrum(sim_name, par["General"]["Molecule name"],
                            energy, accu_spect, good_trajs)


# ######################################################### #
# ---------------------- Main program --------------------- #
# ######################################################### #

if __name__ == "__main__":

    # ---------------------- Start output --------------------- #

    s22 = " "*22

    # Important program data
    version = 0.2                # Last version was a couple of glued scripts
    author = "Rony J. Letona"    # For the time being, its me
    year = 2023                  # Year of my MSc thesis defense
    month = 7                    # Month of my MSc thesis defense
    email = "rony.letona@estudiante.uam.es" # Univeristy email
    title = f"\n\n{s22}*** TraSo: Trajectory Software ***{s22}\n\nOptions:"

    parser = ap.ArgumentParser(prog=title,
    description=io.__desc__,
    epilog=f"(C) {year} {author} [{email}]")

    # Adding all the command line options to the program
    parser.add_argument('-o', '--optfreq', action='store_true',
        help=" perform the optimization and frequencies stage")
    parser.add_argument('-n', '--normodes', action='store_true',
        help=(" perform normal mode sampling and initial condition "
                "creation"))
    parser.add_argument('-a', '--aimd', action='store_true',
        help=(" perform Ab Initio Molecular Dynamics to "
                "propagate trajectories"))
    parser.add_argument('-s', '--spectrum', action='store_true',
        help=" perform trajectory analysis and create mass spectrum")
    parser.add_argument('-i', '--input', type=str,
        help=" enter the name of the input file (e.g. sample_input.json)",
        required=True)

    # Parsing all the command line arguments
    args = parser.parse_args()

    # Check whether the commands don't break the sequence
    stage = io.switcher(args.optfreq, args.normodes, args.aimd, args.spectrum)

    # Header
    print(io.__head__.format(v=version, y=year, m=month,
        author=author, email=email))

    # ----------------------- Locations -----------------------

    # Get the current location
    here = os.getcwd()
    print(f"\n Working directory: {here}")

    # Get this program's location
    prog = os.path.dirname(os.path.abspath(__file__))
    print(f"\n Executable's location: {prog}")

    # ---------------- Load software packages -----------------

    # Get available QM and MD software packages and their paths
    qm_params, md_params = io.take_paths(prog)
    print(f"\n QM and MD software paths loaded!")

    print("\n Quantum Chemistry Software:")
    for kqm in qm_params.keys():
        print(f" * {kqm}:\t{qm_params[kqm]['Path']}")

    print("\n Molecular Dynamics Software:")
    for kmd in md_params.keys():
        print(f" * {kmd}:\t{md_params[kmd]['Path']}")

    # -------------- Load input and coordinates ---------------

    print("\n Reading input ...")

    # Get the data from the input file
    params = io.load_input(here, args.input)
    print(f" Input file for {params['General']['Molecule name']} read.")

    simulation_name = params['General']['Coordinates'][:-4]

    print("\n " + "-" * 77 + " \n")

    print(f" Calculation for {params['General']['Molecule name']} starting ...")

    # Time when the simulation starts
    i_time = time()

    # =========================================================

    print("\n " + ". " * 38 + ". \n")

    print(" "*24 + "==> Computing Normal Modes <==" + " "*24 + "\n")

    # The user can't ask not to calculate NMs without an existing output
    if not(stage[0]) and not(os.path.exists(f"{sim_name}.out")):
        raise FileNotFoundError((f"There is no output file with a "
                                "frequencies calculation which can be used "
                                "for the rest of the simulation."))

    # Get data from the normal modes
    na, mass, masses, coords, nm, freqs = normal_mode_analysis(simulation_name,
                                                            params, qm_params)

    q_time = time()
    print(f"\n > Time for optimization and frequencies: {q_time - i_time:10.4f} s.")

    # =========================================================

    print("\n " + ". " * 38 + ". \n")

    print(" "*24 + "==> Sampling Normal Modes <==" + " "*24 + "\n")

    if stage[1]:
        # Get the new positions and velocities
        new_coords, vels, new_Etot = normal_mode_sampling(params, coords, 
                                                            masses, freqs, nm)
    else:
        print(" Skipping the sampling of the normal modes stage ...")

    nm_time = time()
    print(f"\n > Time to sample the normal modes: {nm_time - q_time:10.4f} s.")

    # =========================================================

    print("\n " + ". " * 38 + ". \n")

    print(" "*21 + "==> Creating Initial Conditions <==" + " "*21 + "\n")

    if stage[1]:
        # Make all the folders for the trajectories
        make_initial_conditions(simulation_name, na, new_coords, vels,
                                prog, here, md_params, qm_params, params)
    else:
        print(" Skipping the initial condition creation stage ...")

    t_time = time()
    print(f"\n > Time to create trajectory files: {t_time - nm_time:10.4f} s.")

    # =========================================================

    print("\n " + ". " * 38 + ". \n")

    print(" "*23 + "==> Propagating Trajectories <==" + " "*23 + "\n")

    if stage[2]:
        # Propagate all trajectories
        start_bomd(simulation_name, here, params, md_params)
    else:
        print(" Skipping the Born-Oppenheimer/Ab-Initio MD stage ...")

    f_time = time()
    print(f" Time to run all trajectories: {f_time - t_time:10.4f} s.")

    # =========================================================

    print("\n " + ". " * 38 + ". \n")

    print(" "*23 + "==> Producing Mass Spectrum <==" + " "*23 + "\n")

    if stage[3]:
        # Propagate all trajectories
        compute_spectrum(simulation_name, here, params,
                            sampling.j_to_kcal(sum(new_Etot)))
    else:
        print(" Skipping the mass spectrum creation stage ...")

    s_time = time()
    print(f" Time to produce the mass spectrum: {s_time - f_time:10.4f} s.")

    # =========================================================

    print("\n " + "-" * 77 + " \n")

    print(f"\n\n Total elapsed time: {s_time - i_time:10.4f} s.")

    print("\n\n TraSo has finished with the simulation.\n\n Good bye!")

    print("\n " + "-" * 77 + " \n")