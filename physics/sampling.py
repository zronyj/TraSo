""" Module to compute the trajectories based on random sampling

There are several sampling methods in VENUS, this module implements
one of them for the time being, and several other functions to
compute energies and other quantities.
"""

import numpy as np                 # Library to do basic scientific computing
from scipy import constants as cts # Library with several universal constants

def j_to_kcal(energy):
    r"""Translate energy in Joule to kcal/mol

    Parameters
    ----------
    energy : float
        The energy in Joule

    Returns
    -------
    float
        The energy in kcal/mol
    """
    
    return energy * cts.Avogadro / (1000 * cts.calorie)

def kcal_to_j(energy):
    r"""Translate energy in kcal/mol to Joule

    Parameters
    ----------
    energy : float
        The energy in kcal/mol

    Returns
    -------
    float
        The energy in Joule
    """

    return energy * (1000 * cts.calorie) / cts.Avogadro


def center_of_mass(na, coords_v, masses):
    r"""Compute the center of mass and coordinates

    Function to compute the center of mass and compute
    coordinates with respect to it

    Parameters
    ----------
    na : int
        The number of atoms in the molecule
    coords_v : list of float
        A list of the atom X, Y and Z coordinates
    masses : list of float
        A list of the masses of all atoms in the molecule

    Raises
    ------
    ValueError
        If the second dimension of the provided matrix is not equal
        to 1 (i.e. if it's not a list)

    Returns
    -------
    com : list of float
        A list of 3 coordinates for the Center Of Mass -COM-
    new_coords : list of [float, float, float]
        A list of the X, Y and Z coordinates
    """

    if coords_v.shape[1] != 1:
        raise ValueError("The coordinates should be shaped as an 3*N vector!")
    
    # Reshape the coordinates, and multiply each atom by its mass
    coords = coords_v.reshape(na, 3)
    mass_weighting = coords.T * masses

    # Sum all the x, y and z coordinates, and dividing them through the mass
    com = mass_weighting.sum(axis=1)/masses.sum()
    
    # Re-calculating the coordiantes with respect to the COM
    new_coords = np.array([ c - com for c in coords ])
    return com, new_coords

def inertia_tensor(na, coords_v, masses):
    r"""Compute the inertia tensor of a molecule

    The units of this matrix are g * Angstrom^2

    Parameters
    ----------
    na : int
        The number of atoms in the molecule
    coords_v : list of float
        A list of the atom X, Y and Z coordinates
    masses : list of float
        A list of the masses of all atoms in the molecule

    Raises
    ------
    ValueError
        If the second dimension of the provided matrix is not equal
        to 1 (i.e. if it's not a list)

    Returns
    -------
    list of [float, float, float]
        A 3 by 3 symmetrical matrix, which is the inertia tensor
        of the molecule
    """

    if coords_v.shape[1] != 1:
        raise ValueError("The coordinates should be shaped as an 3*N vector!")

    # Reshape the coordinates
    coords = coords_v.reshape(na, 3)

    # Compute the diagonal elements
    Ixx = sum([masses[i]*(coords[i][1]**2+coords[i][2]**2) for i in range(na)])
    Iyy = sum([masses[i]*(coords[i][0]**2+coords[i][2]**2) for i in range(na)])
    Izz = sum([masses[i]*(coords[i][0]**2+coords[i][1]**2) for i in range(na)])

    # Compute the non-diagonal elements
    Ixy = sum([masses[i]*coords[i][0]*coords[i][1] for i in range(na)])
    Iyz = sum([masses[i]*coords[i][1]*coords[i][2] for i in range(na)])
    Ixz = sum([masses[i]*coords[i][0]*coords[i][2] for i in range(na)])

    # The matrix is symmetrical!
    return np.array([[Ixx, -Ixy, -Ixz], [-Ixy, Iyy, -Iyz], [-Ixz, -Iyz, Izz]])

def angular_momentum(na, coords_v, momentum_v):
    r"""Compute the angular momentum of a molecule

    The units of the computed angular momentum are g * Angstrom^2 / ps

    Parameters
    ----------
    na : int
        The number of atoms in the molecule
    coords_v : list of float
        A list of the atoms X, Y and Z coordinates
    momentum_v : list of float
        A list of the atoms X, Y and Z momenta

    Returns
    -------
    J : list of float
        A single 3D vector representing the total angular momentum
        of the molecule.
    """

    # Creating the 0 3D vector to store the total angular mometum
    J = np.zeros(3)

    # Iterate over all atoms and add each atom's angular momentum
    # sqrt(mol) * Angstrom * (g / mol) * sqrt(mol) * Angstrom / ps = ...
    # Units: ... g * Angstrom^2 / ps
    for atom in range(0, 3*na, 3):
        J += np.cross(coords_v[atom:atom+3,:].reshape(3),
                    momentum_v[atom:atom+3,:].reshape(3))

    return J.reshape(3, 1)

def rotational_velocity(na, coords_v, J_momentum, I_tensor):
    r"""Compute the rotational velocity of a molecule

    The units of the computed angular momentum are sqrt(mol) * Anstrom / ps

    Parameters
    ----------
    na : int
        The number of atoms in the molecule
    coords_v : list of float
        A list of the atoms X, Y and Z coordinates
    J_momentum : list of float
        A list of the X, Y and Z momenta of the molecule
    I_tensor : list of [float, float, float]
        A 3x3 matrix with the inertial tensor of the molecule

    Returns
    -------
    new_vels : list of float
        A list of the atoms X, Y and Z velocities
    """

    # Compute the angular velocity
    # (g * Angstrom^2)^(-1) * g * Angstrom^2 / ps = 1 / ps
    omega = np.linalg.inv(I_tensor) @ J_momentum

    # Creating a list for the new velocities
    new_vels = []

    # Computing the cross-products
    # 1 / ps * sqrt(mol) * Anstrom = sqrt(mol) * Anstrom / ps
    for atom in range(0, 3*na, 3):
        new_vels.append(np.cross(omega.reshape(3),
                    coords_v[atom:atom+3,:].reshape(3)))

    new_vels = np.array(new_vels).reshape(3*na,1)
    return new_vels

def kinetic_energy(mass, velocity):
    r"""Compute the kinetic energy of the atoms in the molecule

    The units of the computed energy are Joule (J)

    Parameters
    ----------
    mass : list of [float, ... float]
        A matrix whose main diagonal are the atomic masses of the
        molecule
    velocity : list of float
        A vector containing the XYZ velocity components of each
        atom in the molecule

    Returns
    -------
    float
        The kinetic energy of the molecule
    """

    # T = 1/2 * (m * v) * v = 1/2 * m * v^2
    # 1 A = 10^-10 m
    # 1 ps = 10^-12 s
    # 1 g = 10^-3 kg
    return 0.5 * ((mass @ velocity).T @ velocity) * ( 1E-10 * 1E12 )**2 * 1E-3

def potential_energy(frequencies, mass, displacement):
    r"""Compute the potential energy of the atoms in the molecule

    The units of the computed energy are Joule (J)

    Parameters
    ----------
    frequencies : list of float
        A list with all the frequencies of the pairs of atoms
        (harmonic oscillator approximation).
    mass : list of [float, ... float]
        A matrix whose main diagonal are the atomic masses of the
        molecule
    displacement : list of float
        A vector containing the XYZ components of the relative
        displacement of each atom in the molecule

    Returns
    -------
    float
        The potential energy of the molecule
    """

    # Compute the square root of the Hooke constant k
    # 100 cm^-1 = 1 m^-1
    sqrt_k = 2 * np.pi * 100 * cts.speed_of_light
    sqrt_k *= np.array([0]*6 + [f for f in frequencies])

    # Compute the square root of k times the displacement
    sqrt_kx = np.sqrt(mass) @ np.diag(sqrt_k) @ displacement * 1E-10 * 1E-3
    # V = 1/2 * (sqrt(k) * x)^2 = 1/2 * k * x^2
    # 1 A = 10^-10 m
    # 1 g = 10^-3 kg
    return 0.5 * sqrt_kx.T @ sqrt_kx

def e_qharmonic(frequency, n):
    r"""Compute the energy of the molecule for a given state

    Compute the energy of the molecule according to the Quantum
    Harmonic Oscillator, for a given frequency.
    The units of the computed energy are Joule (J)

    Parameters
    ----------
    frequency : float
        A normal mode eigenvalue of the molecule
    n : int
        Quantum number for the Harmonic Oscillator

    Returns
    -------
    float
        The energy of the molecule
    """

    # Planck constant = 6.62607015e-34 Js^-1
    # E = h * v * (n + 0.5)
    return cts.Planck * frequency * ( n + 0.5 )

def get_ZPEs(vibs, n=0):
    r"""Compute the Zero Point Energy of a molecule

    Function to compute the ZPE of a quantum harmonic oscillator
    for each frequency

    Parameters
    ----------
    vibs : list of float
        The frequencies of the normal modes of the molecule
    n : int
        The quantum number for the Harmonic Oscillator. For a
        ZPE, n should be 0

    Returns
    -------
    zpes : float
        The Zero Point Energy of the molecule
    """

    # Consider that each frequency is given in cm^{-1}. It is, therefore,
    # changed to m^{-1} and multiplied by the speed of light to get it in s^{-1}
    zpes = [e_qharmonic(100 * v * cts.speed_of_light, n) for v in vibs if v != 0]
    return zpes

def remove_spurious_angular_momentum(na, masses, mass_mat, coords_v, vels_v):
    r"""Compute the new momenta without the spurious angular momentum

    Calculates the spurious angular momentum arising from the molecular
    deformation, and adds it to the current momenta looking to remove it.

    Parameters
    ----------
    na : int
        The number of atoms in the molecule
    masses : list of float
        A list of the masses of all atoms in the molecule
    mass_mat : list of [float, float, ..., float]
        A 3Nx3N matrix with the atomic weights in the diagonal
    coords_v : list of float
        A list of the atoms X, Y and Z coordinates
    vels_v : list of float
        A list of the atoms X, Y and Z velocities

    Returns
    -------
    list of float
        A list of the atoms X, Y and Z new (corrected) velocities
    """

    # Compute the center of mass of the molecule
    # Units: sqrt(mol) * Angstrom
    com, coords_com = center_of_mass(na, coords_v, masses)
    coords_com = coords_com.reshape(3*na,1)

    # Compute the total angular momentum of the molecule
    # Units: g * Angstrom^2 / ps
    J_ang_momentum = angular_momentum(na, coords_com, mass_mat @ vels_v)

    # Compute the inertia tensor of the molecule
    # Units: g * Angstrom^2
    I_tensor = inertia_tensor(na, coords_com, masses)

    # Compute the actual angular velocity of each atom in the molecule
    # Units: sqrt(mol) * Angstrom / ps
    new_rota_vel = rotational_velocity(na, coords_com,
        (-1) * J_ang_momentum, I_tensor)

    return vels_v + new_rota_vel

def get_harmonic_state(freq, ener, random):
    r"""Compute a random state in the harmonic oscillator phase space

    Parameters
    ----------
    freq : float
        The frequency of oscillation of the normal mode, according to the
        harmonic oscillator approximation
    ener : float
        The new target energy of that atom, based on a normal mode level
    random : float
        A random number to compute the new position and velocity of the
        atom using the harmonic oscillator approximation

    Returns
    -------
    amplitude : float
    position : float
        The position for a given atom of the molecule, at the desired
        harmonic oscillator energy
    velocities : float
        The velocity for a given atom of the molecule, at the desired
        harmonic oscillator energy
    """

    # Compute the amplitude
    # w = omega = 2 * pi * v
    # E = 1/2 * k * (x - x0)^2
    # x - x0 = (2 * E / k)^0.5              |  k = omega^2 * m = 4 * pi^2 * v^2 * m
    # x - x0 = (2 * E / m)^0.5 / (2 * pi * v) = A
    # { This lacks the one over sqrt of mass component!!! }
    # Unit: sqrt(kg) * m
    A_numerator = np.sqrt(2 * ener)
    omega = 2 * np.pi * freq
    amplitude = A_numerator / omega

    # Compute the new position and momentum
    #            E     = T + V = p^2 / 2m + (k * q^2) / 2
    #                  = 1/2 * m * v^2 + 1/2 * k * q^2
    #                  = 1/2 * m * (q')^2 + 1/2 * k * q^2
    #            2 * E = m * (q')^2 + k * q^2
    #            1     = m/2E * (q')^2 + k/2E * q^2
    # Thinking of an ellipse, q can be found as a trigonometric function (of a variable u)
    # When q = 0, the potential energy should be 0 and kinetic should be maximum
    # When q = amplitude, the potential energy should be maximum and the kinetic 0
    # Therefore,
    #            q (u) = (2E/k)^0.5 * sin((k/m)^0.5 * u)
    #                  = (2E/m)^0.5 / (2 * pi * v) * sin( 2 * pi * v * u )
    #                  = A * sin( 2 * pi * v * u )
    #            q'(u) = (2E/k)^0.5 * (k/m)^0.5 * cos( 2 * pi * v * u )
    #                  = (2E/m)^0.5 * cos( 2 * pi * v * u )
    # Which further leads to
    #            q = A * sin( 2 * pi * v * u )
    #            p = (2 * m * E)^0.5 * cos( 2 * pi * v * u )
    # The maximum value that the energy can achieve is when
    #     a) V = 0 and T = max
    #     b) T = 0 and V = max
    # Proceeding with (b) ...
    #            (2E/m)^0.5 * cos( 2 * pi * v * u ) = 0
    #            2 * pi * v * u = cos^(-1)(0) = (2*n + 1) * pi / 2  | n = 0, 1, 2, ...
    #            u = (2*n + 1) / (4 * v)
    # Considering that the periodicity will lead to equal values, the maximum
    # can be obtained by evaluating n = 0
    #            u_{max} = 1 / (4 * v)
    # Now, considering that any random number between 0 and the maximum u is a valid
    # coordinate, then a uniform distribution random number R can be chosen as:
    #            R * 1 / (4 * v)
    # Substituting into the position and velocity equations ...
    #            q (u) = (2E/m)^0.5 / (2 * pi * v) * sin( pi/2 * R )
    #            q'(u) = (2E/m)^0.5 * cos( pi/2 * R )
    # Units: sqrt(kg) * m
    position = amplitude * np.sin( random * np.pi / 2 )
    # Units: sqrt(kg) * m / s
    velocity = A_numerator * np.cos( random * np.pi / 2 )
    return amplitude, position, velocity

def microcanonical(num_nms, target_energy, new_energies,
    rand, iter_num, frequency):
    r"""Compute a random energy level according to microcanonical sampling

    Parameters
    ----------
    num_nms : int
        Number of normal modes for this molecule
    target_energy : float
        Target energy given in J/mol 
    new_energies : list of float
        Energy of each normal modes at a different level in J/mol
    rand : float
        A random number to select this harmonic oscillator level
    iter_num : int
        Number of normal mode being computed
    frequency : float
        The frequency of oscillation of the normal mode, according to the
        harmonic oscillator approximation

    Returns
    -------
    level : int
        New energy level in the harmonic oscillator approximation
    """

    # Compute the new energies by following the equation
    # E[i] = (E[0][i] - sum(E[:i])) * (1 - R^(1/(n - i)))
    # Unit: J
    temp_energy = (target_energy - sum(new_energies))
    temp_energy *= (1 - rand**(1/(num_nms - iter_num)))

    # Find the harmonic oscillator level that corresponds to this energy
    level = round( temp_energy / ( cts.Planck * frequency ) )

    # If none, return the ZPE level
    if level < 0: level = 0

    # Compute the real energy at the found harmonic oscillator level
    chance = e_qharmonic(frequency, level)

    return level


def get_initial_conditions(coordinates, masses, frequencies, normal_modes,
    new_energy, sampling='microcanonical', levels=[0], seed=42):
    r"""Compute the force vectors using randomly assigned normal modes

    This function implements several sampling techniques. It was built
    using [Hase]_, et al. as a reference.

    Parameters
    ----------
    coordinates : list of [str, float, float, float]
        A list of the atom symbol and its X, Y and Z coordinates
    masses : list of float
        A list of the masses of all atoms in the molecule
    frequencies : list of float
        A list with all the frequencies of the pairs of atoms
        (harmonic oscillator approximation).
    normal_modes : list of list of float
        A list with lists of numbers corresponding to the normal
        modes in the form of XYZXYZ... vectors.
    new_energy : float
        Target energy given in J/mol
    sampling : str
        The sampling technique. Possible values are:
        - microcanonical: microcanonical sampling
        - levels: compute the force vectors using user assigned normal modes
    levels : list of int
        Level for each normal mode
    seed : int
        Integer used as seed of the generated random numbers

    Returns
    -------
    final_coords : list of [str, float, float, float]
        A list of the atom symbol and its new X, Y and Z coordinates
    final_velocs : list of [str, float, float, float]
        A list of the atom symbol and its new X, Y and Z velocities
    new_nm_energy : float
        The new energy of this specific configuration in phase space

    References
    ----------
    .. [Hase] W. L. Hase, R. J. Duchovic, X. Hu, A. Komornicki, K. F. Lim,
        D. H. Lu, G. H. Peslherbe, K. N. Swamy, S. R. Vande Linde, L. Zhu,
        A. Varandas, H. Wang, and R. J. Wolf. VENUS. a general chemical
        dynamics computer program. QCPE Bull., 16:671, 1996.
    """

    num_atoms = len(coordinates)
    num_nms = len(normal_modes)

    # -------- Mass matrix --------
    # Initializa list of diagonal values
    mass_diag = [0]*num_atoms*3

    # Fill list
    for k in range(num_atoms):
        mass_diag[3*k] = masses[k]   # Atom mass for X coordinate
        mass_diag[3*k+1] = masses[k] # Atom mass for Y coordinate
        mass_diag[3*k+2] = masses[k] # Atom mass for Z coordinate

    # From the list, create a diagonal matrix
    M = np.diag(mass_diag)

    # Inverse of the mass matrix
    iM = np.diag([1/masa if masa != 0 else 0 for masa in mass_diag])


    # Initializing arrays
    amplitudes = [0]*num_nms
    q_positns = [0]*num_nms
    q_velocts = [0]*num_nms

    # The levels of the harmonic oscillators used to achieve certain energy
    if sampling == 'microcanonical':
        levels = [0] * num_nms

    # Setting the energies at the ZPE
    new_energies = [e_qharmonic(100 * frequencies[i] * cts.c, levels[i]) \
    for i in range(num_nms)]

    # Set random seed
    np.random.seed(seed)

    for i in range(num_nms):

        # Frequencies
        # Consider that each frequency is given in cm^{-1}
        # s^{-1} = 1 / cm * [ 100 cm / 1 m ] * [ 3E8 m / s ] 
        # Unit: 1 / s
        v = 100 * frequencies[i] * cts.speed_of_light

        if sampling == 'microcanonical':
            # Compute the new level according to microcanonical sampling
            levels[i] = microcanonical(num_nms, new_energy, new_energies,
                np.random.random(), i, v)

        # Compute the real energy at the found harmonic oscillator level
        new_energies[i] = e_qharmonic(v, levels[i])

        # The energy contribution by kinetic (T) or potential (V) will be split by this
        # random number
        R = np.random.random() - 0.5

        # Find a random coordinate in phase space for this atom at the
        # computed energy
        amplitudes[i], q_positns[i], q_velocts[i] = get_harmonic_state(v,
                                                        new_energies[i], R)

    # Compute the new energy according to the normal modes
    new_nm_energy = sum(new_energies)

    # Equilibrium geometry coordinates
    # Unit: Angstrom
    x0 = np.array([ a[1:] for a in coordinates ]).reshape(num_atoms*3,1)

    # Mass-weighted cartesian displacements
    # 1/sqrt(g/mol) = sqrt(mol/g)
    # Unit: sqrt(mol/g)
    L = normal_modes.reshape(num_nms, num_atoms*3).T

    # Cartesian displacements
    # sqrt(g/mol) * sqrt(mol/g) = nothing
    # Unit: none!
    Lx = np.sqrt(M) @ L

    # Normal coordinates
    # sqrt(J)/(1/s) = sqrt( kg m^2 s^-2 ) / (1/s) = sqrt(kg) m
    # 1 m = 10^10 Angstrom
    # 1 g = n kg * [1000 g / 1 kg ]
    # Unit: sqrt(g) Angstrom
    # {The sqrt(g) part comnes from the lack of mass in the amplitude}
    Q = np.array(q_positns).reshape(num_nms,1) * np.sqrt(1000) * 1E10

    # Normal velocities
    # sqrt(J)/(1/s) = sqrt( kg m^2 s^-2 ) = sqrt(kg) m/s
    # 1 m = 10^10 Angstrom
    # 1 g = n kg * [1000 g / 1 kg ]
    # 1 s = 10^12 ps
    # Unit: sqrt(g) Angstrom / ps
    # {The sqrt(g) part comnes from the lack of mass in the amplitude}
    Qp = np.array(q_velocts).reshape(num_nms,1) * np.sqrt(1000) * 1E-2

    # Displacement
    # sqrt(mol/g) * sqrt(g) * Angstrom = Angstrom
    # {Fixing the lack of mass in the amplitude}
    # Unit: Angstrom * sqrt(mol)
    x = (np.sqrt(iM) @ Lx) @ Q

    # Velocity
    # sqrt(mol/g) * sqrt(g) * Angstrom / ps = Angstrom / ps
    # {Fixing the lack of mass in the amplitude}
    # Unit: Angstrom * sqrt(mol) / ps
    v = (np.sqrt(iM) @ Lx) @ Qp

    # Compute the current kinetic energy
    Kinetic = kinetic_energy(M, v)

    # Compute the current potential energy
    Potential = potential_energy(frequencies, M, x)

    # Compute the current total energy of the molecule
    E_total = Kinetic.item() + Potential.item()

    # Check if, by chance, the energy is in range
    for i in range(100):

        # Compute the new velocities by removing the spurious
        # angular momentum
        v = remove_spurious_angular_momentum(num_atoms, masses, M, x, v)

        # Re-calculate the kinetic energy
        Kinetic = kinetic_energy(M, v)

        # Re-compute the potential energy
        Potential = potential_energy(frequencies, M, x)

        # Re-calculate the total energy
        E_total = Kinetic.item() + Potential.item()

        # Check if the energy is within 0.01% of the target energy
        if abs(new_nm_energy - E_total)/new_nm_energy <= 1E-4:
            break

        # Estimate the deviation of the energy from the target energy
        scaling_factor = np.sqrt(new_nm_energy/E_total)

        # Scale the coordinates and velocities
        x *= scaling_factor
        v *= scaling_factor

        # And if the spurious angular momentum could not be removed?
        if i == 99:
            raise Exception(
                ("The spurious angular momentum "
                "could not be removed after 100 iterations!")
            )

    # Re-compute the kinetic energy
    Kinetic = kinetic_energy(M, v)

    # Re-compute the potential energy
    Potential = potential_energy(frequencies, M, x)

    # Re-compute the total energy of the molecule
    E_total = Kinetic.item() + Potential.item()

    # This should be for an atom!
    # sqrt(mol) * Angstrom * sqrt(atoms/mol) = Angstrom
    x *= np.sqrt(cts.Avogadro)
    # sqrt(mol) * Angstrom / ps * sqrt(atoms/mol) = Angstrom / ps
    v *= np.sqrt(cts.Avogadro)

    # Final coordinates and velocities
    new_x = (x + x0).reshape(num_atoms, 3)
    new_v = v.reshape(num_atoms, 3)

    # Building a [symbol, X, Y, Z] shape list of both the coordinates
    # and the velocities
    final_coords = []
    final_velocs = []
    for i in range(num_atoms):
        final_coords.append(
            # Symbol             Coord X      Coord Y      Coord Z
            [ coordinates[i][0], new_x[i][0], new_x[i][1], new_x[i][2] ]
            )
        final_velocs.append(
            # Symbol             Veloc X      Veloc Y      Veloc Z
            [ coordinates[i][0], new_v[i][0], new_v[i][1], new_v[i][2] ]
            )

    return final_coords, final_velocs, new_nm_energy, E_total, levels
