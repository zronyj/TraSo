Molecular Dynamics -MD-
+++++++++++++++++++++++

To simulate the molecular fragmentation after its excitation, a time dependency must be considered: propagation [Koopman2]_. Contrary to conventional MD simulations where pressure or temperature are sought to remain constant, this case requires the energy to remain constant [Carra]_. This means that the propagation of the molecule should happen without any solvent nor any artifact to constrain the molecule’s volume or its atoms velocity. The atomic velocity will be used as an indicator of the system’s energy through its relationship with the temperature :math:`T`. Actually, another sampling method for the simulation of mass spectra involves setting a temperature, not atomic velocities [Carra]_. Said relationship can be observed in :eq:`boltzvel` as the average atomic velocity being proportional to the number of atoms :math:`n` and the temperature, held equal by the constant :math:`k_{B}`; the Boltzmann constant [Nelson]_.

.. math::
	:label: boltzvel

	\frac{1}{n} \left( \sum_{i=1}^{n} \frac{1}{2} m_{i} v_{i}^{2} \right) = \frac{3}{2} n k_{B} T

Each atom’s propagation during the MD simulation follows the Verlet equations in some way. The most basic algorithm for this is the simple Verlet algorithm.

Verlet’s Equations
==================

Beginning with the idea of finding the position of a particle through a Taylor expansion, and truncating it at the third term, the equation of position takes the following shape:

.. math::

	x \left( t + \Delta t \right) =	x \left( t \right) + \Delta t \dfrac{d}{dt} x \left( t \right) +
	\frac{1}{2} \Delta t^{2} \dfrac{d^{2}}{d t^{2}} x \left( t \right)

.. math::
	:label: verlet1a

	x_{s + 1} = x_{s} + v_{s} \Delta t + \frac{1}{2} a_{s} \Delta t^{2}

Considering that the fourth term of the expansion is not usually computed, a better approximation can be obtained by considering the previous step.

.. math::
	:label: verlet1b

	x_{s - 1} = x_{s} -	v_{s} \Delta t + \frac{1}{2} a_{s} \Delta t^{2}

By adding equations :eq:`verlet1a` and :eq:`verlet1b`, and solve for :math:`x_{s + 1}`, the first Verlet equation arises:

.. math::
	:label: verlet1

	x_{s + 1} = 2 x_{s} - x_{s - 1} + a_{s} \Delta t^{2}

Now, to compute the energy of the particle, the velocity is required. To compute the velocity, equation :eq:`verlet1b` is subtracted from :eq:`verlet1a` and solved for :math:`v_{s}`.

.. math::
	:label: verlet2a

	v_{s} = \frac{x_{s + 1} - x_{s - 1}}{ 2 \Delta t }

Given that the denominator considers an increment of two time steps, and the numerator does so as well by advancing from :math:`s - 1` to :math:`s + 1`, a single step forward would require the initial position to be :math:`x_{s}`. In that case, equation :eq:`verlet2a` becomes:

.. math::
	:label: verlet2

	v_{s + 1} = \frac{x_{s + 1} - x_{s}}{ \Delta t }

This is Verlet's second equation of motion. However, equation :eq:`verlet2` requires the result from :eq:`verlet1`. And :eq:`verlet1` needs the acceleration :math:`a_{s}` for each step. The latter is computed as the product of the inverse of the mass :math:`m` of the particle and the particle's force :math:`F_{s}`, which in turn is computed as the positional derivative of the potential energy given by the Force Field, or the electronic structure [1]_. The latter leading to the *Ab-Initio* Molecular Dynamics -AIMD- method [Cuendet]_.

.. math::
	:label: verlet3

	a_{s} = \frac{F_{s}}{m} = m^{-1} F_{s} = m^{-1} \left( - \dfrac{d V}{d x_{s}} \right)

Finally, by solving equations :eq:`verlet1`, :eq:`verlet2` and :eq:`verlet3` iteratively for all particles, their propagation is described over time [Cuendet]_. The latter meaning that the atoms will be moved :math:`s` times (:math:`s` number of steps), and to do so, each atom's position :math:`x`, velocity :math:`v` and force :math:`F` will have to be recalculated each time. Each step will represent a given period of time :math:`\Delta t` in reality. Therefore, as an example, a :math:`1ps` simulation carried out over :math:`2000` steps, translates to :math:`\Delta t` being equal to :math:`0.5fs` and :math:`s` going from :math:`0` to :math:`2000`.

.. [1] In the case of the electronic structure, methods such as Hartree-Fock or Density Functional Theory can be used.

Velocity Verlet Algorithm
=========================

Another way to integrate the equations of motion, based on the same idea, is the Velocity Verlet Algorithm [Cuendet]_. The latter begins by defining the position in a similar way as equation :eq:`verlet1a`, but truncates the Taylor expansion to the third term as shown in equation :eq:`velocityver1`.

.. math::
	:label: velocityver1

	x_{s + 1} = x_{s} +	v_{s} \Delta t + \frac{1}{2} a_{s} \Delta t^{2}

The important part here, is that the velocity :math:`v_{s}` and the acceleration :math:`a_{s}` should be known as initial conditions. The latter can be obtained in essentially the same way as with the case of equation :eq:`verlet3`.

.. math::
	:label: velocityver2

	a_{s+1} = m^{-1} F \left( x_{s + 1} \right)

But the velocity does become different than what was seen in equation :eq:`verlet2`. The approach is based on the same Taylor expansion used for the position.

.. math::
	:label: velocityver3

	v_{s + 1}  = v_{s} \Delta t + \frac{1}{2} \left( a_{s} + a_{s + 1} \right) \Delta t^{2}

By splitting equation :eq:`velocityver3` and computing the part with :math:`a_{s}` first and the leaving the part with :math:`a_{s + 1}` last, the algorithm changes its name and becomes the Leapfrog Verlet Algorithm.

Leapfrog Verlet Algorithm
=========================

.. _velver:
.. figure:: ../imgs/verlet.drawio.png
	:align: center

	Flowchart of the Leapfrog algorithm following :eq:`leapfrog1`, :eq:`leapfrog2`, :eq:`leapfrog3`, and :eq:`leapfrog4`.

In other packages, the Leapfrog algorithm is used to integrate the trajectory. In that case, the steps described in the next equations are followed [Phillips]_.

.. math::
	:label: leapfrog1

	v_{s + \frac{1}{2}} = v_{s} + \frac{1}{2} a_{s} \Delta t

.. math::
	:label: leapfrog2

	x_{s + 1} = x_{s} + v_{s + \frac{1}{2}} \Delta t

.. math::
	:label: leapfrog3

	a_{s + 1} = m^{-1} F_{s + 1} = m^{-1} F \left( x_{s + 1} \right)

.. math::
	:label: leapfrog4

	v_{s + 1} = v_{s + \frac{1}{2}} + \frac{1}{2} a_{s + 1} \Delta t

No initial conditions for the system must be established in the input of the MD software package, except for the timestep and the number of steps. The initial conditions for NAMD, Orca and TeraChem are provided as *restart files* for the coordinates and the velocities. Therefore, the previously calculated atomic positions and velocities, as described in other sections, are used by the MD package, which then propagates them following the previously described equations for each step in a cyclic manner, as shown in Figure :ref:`1 <velver>`. As soon as all the steps have been done, the propagation will stop, and the last geometry will be the one used, in this case, to generate the CID-MS.

Obtaining the Forces
====================

In the case of the MD, it is relevant to extract the gradient for each energy calculation. The forces involved at each step of the propagation are computed based on that gradient, as shown in equation :eq:`gradient`. The gradient is calculated for the coordinates :math:`\vec{q}` of each atomic nucleus in the molecule [Manathunga]_ [Cruzeiro]_.

.. math::
	:label: gradient
	
	\nabla E \left( \vec{q} \right) = \vec{u}_{x} \left( \dfrac{\partial E }{\partial x} \right) + \vec{u}_{y} \left( \dfrac{\partial E }{\partial y} \right) + \vec{u}_{z} \left( \dfrac{\partial E }{\partial z} \right) = \begin{bmatrix}
		\left( \dfrac{\partial E }{\partial x} \right)\\
		\left( \dfrac{\partial E }{\partial y} \right)\\
		\left( \dfrac{\partial E }{\partial z} \right)
	\end{bmatrix}

\noindent where :math:`\vec{u}_{i}` is the unit vector for the :math:`i` axis [Holden]_.

After the initial conditions are generated, and the MD package propagates the trajectory following Verlet's equations, the new forces have to be computed based on the atomic charges and electronic structure. The positions and velocities are easily obtained from the Verlet equations, but the Forces can only be obtained through the gradient. The latter allows the propagation to continue at each step [Melo]_.

References
==========

.. [Koopman2] Koopman, J.; Grimme, S. Journal of the American Society for Mass Spectrometry 2021, 32, 1735–1751.

.. [Carra] Carrà, A.; Spezia, R. Chemistry - Methods 2021, 1, 123–130.

.. [Nelson] Nelson, M. T.; Humphrey, W.; Gursoy, A.; Dalke, A.; Kale, L. V.; Skeel, R. D.; Schulten, K. The International Journal of High Performance Computing Applications 1996, 10, 251–268.

.. [Cuendet] Cuendet, M. A.; Van Gunsteren, W. F. Journal of Chemical Physics 2007, 127, 184102.

.. [Phillips] Phillips, J. C. et al. The Journal of chemical physics 2020, 153.

.. [Manathunga] Manathunga, M.; Aktulga, H. M.; Götz, A. W.; Merz, K. M. Journal of Chemical Information and Modeling 2023, 63, 711–717.

.. [Cruzeiro] Cruzeiro, V. W. D.; Manathunga, M.; Merz, K. M.; Götz, A. W. Journal of Chemical Information and Modeling 2021, 61, 2109–2115.

.. [Holden] Holden, Z. C.; Rana, B.; Herbert, J. M. Journal of Chemical Physics 2019, 150, 144115.

.. [Melo] Melo, M. C.; Bernardi, R. C.; Rudack, T.; Scheurer, M.; Riplinger, C.; Phillips, J. C.; Maia, J. D.; Rocha, G. B.; Ribeiro, J. V.; Stone, J. E.; Neese, F.; Schulten, K.; Luthey-Schulten, Z. Nature Methods 2018 15:5 2018, 15, 351–354.